<a id="main-content"></a>
<div class="region region-content">
	<div id="block-system-main" class="block block-system clearfix">
		<div class="panel-pane pane-jumbotron">
			<div class="pane-content">
				<section class="jumbotron align-mid" id="main-jumbo" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner.jpg);">
					<div class="overlay"></div>
					<div class="jumbotron_bar">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<ol class="breadcrumb">
										<li><a href="/">Poder Judicial Mendoza</a></li>
										<li class="active">Inicio</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
					<div class="jumbotron_body">
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
							<ol class="carousel-indicators">
								<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
								<li data-target="#carousel-example-generic" data-slide-to="1"></li>
								<li data-target="#carousel-example-generic" data-slide-to="2"></li>
							</ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-md-8 text-left bg" data-bg="background-position:top; background-image:url(<?php echo get_template_directory_uri(); ?>/img/article3.jpeg);">
												<h1>Se inauguraron obras de reformas y mejoras en la delegación del Poder Judicial en Tunuyán</h1>
												<p></p>
												<p><b>Leer más</b></p>
											</div>
										</div>
									</div>
								</div>
								<div class="item">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-md-8 text-left bg" data-bg="background-position:top; background-image:url(<?php echo get_template_directory_uri(); ?>/img/article5-min.jpg);">
												<h1>Implementación de sistemas electrónicos en la creación y tramitación de causas</h1>
												<p></p>
												<p><b>Leer más</b></p>
											</div>
										</div>
									</div>
								</div>
								<div class="item">
									<div class="container">
										<div class="row">
											<div class="col-xs-12 col-md-8 text-left bg" data-bg="background-position:top; background-image:url(<?php echo get_template_directory_uri(); ?>/img/article4.jpg);">
												<h1>La Suprema Corte firmó un convenio de colaboración para capacitar a escribanos mediadores</h1>
												<p></p>
												<p><b>Leer más</b></p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#carousel-example-generic" style="width: 3%;" role="button" data-slide="prev">
								<span class="fa fa-chevron-left" aria-hidden="true" style="padding: 100px 0;"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" style="width: 3%;" href="#carousel-example-generic" role="button" data-slide="next">
								<span class="fa fa-chevron-right" aria-hidden="true" style="padding: 100px 0;"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
						
					</section>
				</div>
			</div>
			<section class="bg-gray" style="padding: 10px 0;">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<form class="form-inline">
								<div class="input-group">
								<input class="form-control" placeholder="Ingresá tu consulta">
								<div class="input-group-btn">
								<button type="submit" class="btn btn-md btn-primary" style="padding: 10px 12px 13px !important;"><i class="text-white fa fa-search"></i></button>
							</div>
							</div>
							</form>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-xs">
							<div class="btn-group btn-group-toggle" data-toggle="buttons" style="margin-top:5px;">
								<label class="btn btn-primary active">
									<input type="radio" name="options" id="option1" autocomplete="off" checked> Guía Judicial
								</label>
								<label class="btn btn-primary">
									<input type="radio" name="options" id="option2" autocomplete="off"> Portal Datos abiertos
								</label>
								<label class="btn btn-primary">
									<input type="radio" name="options" id="option3" autocomplete="off"> Noticias Judiciales
								</label>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 hidden-sm hidden-md hidden-lg">
							<a class="btn btn-primary btn-block">Guía Judicial</a>
							<a class="btn btn-primary btn-block">Portal Datos abiertos</a>
							<a class="btn btn-primary btn-block">Noticias Judiciales</a>
						</div>
						</div>
					</div>
				</div>
			</section>
			<section>
				<div class="container">
					<div class="panel-pane pane-texto">
						<div class="pane-content">
							<div class="">
								<h2 class="h3">Accesos Destacados</h2></div>
							</div>
						</div>
						<div class="panel-separator"></div>
						<div class="panel-pane pane-atajos">
							<div class="pane-content">
								<div class="row panels-row">
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="#" class="panel panel-default panel-icon">
											<div class="panel-heading hidden-xs bg-white"><i class="fa icono-arg-justicia big-panel-icon text-primary"></i>
											</div>
											<div class="panel-body text-center">
												<h3><span class="visible-xs-inline"><i class="fa icono-arg-justicia"></i>&nbsp; </span>Suprema Corte de Justicia</h3>
												<hr class="no-m">
												<p class="text-muted">OFICINAS DEPENDIENTES</p>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="#" class="panel panel-default panel-icon panel-primary">
											<div class="panel-heading hidden-xs bg-white"><object class="umsa" type="image/svg+xml" data="<?php echo get_template_directory_uri(); ?>/img/ministeriopublico4-color.svg"></object></i></div>
											<div class="panel-body text-center">
												<h3><span class="visible-xs-inline"><object type="image/svg+xml" class="umsa" data="<?php echo get_template_directory_uri(); ?>/img/ministeriopublico4.svg"></object>&nbsp; </span>Ministerio Público Fiscal</h3>
												<hr class="no-m">
												<p class="text-muted"> DENUNCIÁ HECHOS DELICTIVOS</p>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="#" class="panel panel-default panel-icon panel-primary">
											<div class="panel-heading hidden-xs bg-white"><i class="fa icono-arg-martillo big-panel-icon text-primary"></i></div>
											<div class="panel-body text-center">
												<h3><span class="visible-xs-inline"><i class="fa icono-arg-martillo"></i>&nbsp; </span>Ministerio Público de la Defensa</h3>
												<hr class="no-m">
												<p class="text-muted"> TURNOS DEFENSORIAS</p>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="container">
					

					<section class="jumbotron align-mid mh-400" style="background-position: top; background-image:url(<?php echo get_template_directory_uri(); ?>/img/noticia2.png);">
						<div class="overlay"></div>
						<div class="jumbotron_body">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-md-8 text-left">
										<p>Noticias judiciales</p>
										<h1>La Suprema Corte afianza el fuero de Pequeñas Causas.</h1>
										<p></p>
										<a class="btn text-white">Leer más</a>
									</div>
								</div>
							</div>
						</section>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<h2 class="h3">Mas información</h2>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<?php echo do_shortcode('[filtrogob widget_name="filtrogob" id="4"]');?>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>

		<script type="text/javascript" charset="utf-8">
			jQuery('#carousel-example-generic').bind('slide.bs.carousel', function (e) {
			    var urlImg = jQuery('div.item.active').find('div.bg').data('bg')
				console.log(urlImg)
				jQuery('#main-jumbo').fadeOut(function() {
					jQuery(this).attr('style',urlImg)
				});
			});
			
		</script>