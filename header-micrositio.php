<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<link href="//www.google-analytics.com" rel="dns-prefetch">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>
		<header class="" role="banner">
			<nav class="navbar navbar-top navbar-default">
				<div class="container">
					<div>
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" id="togg" data-target="#main-navbar-collapse" aria-expanded="true">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<!-- Desktop -->
							<a class="navbar-brand hidden-xs" style="padding: 1px 15px;" href="<?php echo home_url() ?>"> <img src="<?php echo get_template_directory_uri(); ?>/img/logo-micrositio.png" class="image-responsive" alt="Inicio" height="70"></a>
							<!-- Mobile -->
							<a class="navbar-brand hidden-sm hidden-md hidden-lg" href="<?php echo home_url() ?>"> <img src="<?php echo get_template_directory_uri(); ?>/img/logo-micrositio.png" class="image-responsive" alt="Inicio" style="height: 40px;"></a>
							
						</div>
						<div class="navbar-collapse collapse" id="main-navbar-collapse" data-seccion="Inicio" aria-expanded="true" style="">
							<ul class="nav navbar-nav pull-right">
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">Guía de tramites <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right" aria-labelledby="download">
										<li class="active"><a href="/" target="_top">Asesorias</a></li>
										<li><a href="" target="_top">Certificaciones</a></li>
										<li><a href="" target="_top">Defensorias</a></li>

										<li><a href="/version/2.3.2/" target="_top">Direcciones y telefonos</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download">Linea 0800 <span class="caret"></span></a>
									<ul class="dropdown-menu" aria-labelledby="download">
										<li><a id="" href="#">¿Que es?</a></li>
										<li><a id="" href="#">¿Que tramitar?</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download">Movil Judicial <span class="caret"></span></a>
									<ul class="dropdown-menu" aria-labelledby="download">
										<li><a id="" href="#">¿Quienes somos?</a></li>
										<li><a id="" href="#">Recorridos</a></li>
										<li><a id="" href="#">Estadísticas</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo home_url() ?>" id="download">Prensa <span class="caret"></span></a>
									<ul class="dropdown-menu" aria-labelledby="download">
										<li><a id="" href="#">Provinciales</a></li>
										<li><a id="" href="#">Nacionales</a></li>
									</ul>
								</li>
								<li class=""><a href="<?php echo home_url() ?>/category/prensa">SIJUM TV</a></li>
								<li><a href="<?php echo home_url() ?>/mi-perfil"><i class="fa fa-user"></i> Mi Perfil</a></li>
							</ul>

						</div>
					</div>
				</div>
			</nav>

		</header>
		<!-- /header -->
