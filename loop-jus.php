<a id="main-content"></a>
		<div class="container"></div>
		<div class="region region-content">
			<div id="block-system-main" class="block block-system clearfix">
				<div class="panel-pane pane-block pane-argentinagobar-search-apachesolrinput">
					<div class="pane-content">
						<section class="jumbotron" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/banner.jpg);">
							<div class="jumbotron_bar">
								<div class="container">
									<div class="row">
										<div class="col-md-12">
											<ol class="breadcrumb">
												<li><a href="/">Poder Judicial Mendoza</a></li>
												<li class="active">Inicio</li>
											</ol>
										</div>
									</div>
								</div>
							</div>
							<div class="jumbotron_body jumbotron_body-lg">
								<div class="container">
									<div class="row">
										<div id="buscador" class="col-md-8">
											<form class="main-form" role="search" action="/" method="post" id="apachesolr-search-custom-page-search-form" accept-charset="UTF-8">
												<div>
													<input type="hidden" name="form_build_id" value="form-fazep4JG7ahY_R47C1Uq0SZ4582IfMQNidCNP2XOGTs">
													<input type="hidden" name="form_id" value="apachesolr_search_custom_page_search_form">
													<div style="display:none;">
														<div class="form-item form-item-tarro-de-miel form-type-textfield form-group">
															<label class="control-label" for="edit-tarro-de-miel">Dejar en blanco </label>
															<input class="form-control form-text" type="text" id="edit-tarro-de-miel" name="tarro_de_miel" value="" size="60" maxlength="128">
														</div>
													</div>
													<div class="input-group">
														<input placeholder="Buscar trámites, servicios o áreas" class="form-control form-text" type="text" id="edit-keys" name="keys" value="" size="20" maxlength="255"><span class="input-group-btn"><button class="btn-primary btn form-submit" style="padding:13px 25px !important;" type="submit" id="edit-submit" name="op" value="<i class=&quot;fa fa-search&quot;></i>"><i class="fa fa-search"></i></button></span></div>
													</div>
												</form>
											</div>
											<div class="col-md-4 m-t-15-m">
												<ul class="list-group">
													<li class="list-group-item"><span class="badge pull-left">1</span>Listas Diarias</li>
													<li class="list-group-item"><span class="badge pull-left">2</span>Formularios de interés</li>
													<li class="list-group-item"><span class="badge pull-left">3</span>Registro de detenidos</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="overlay"></div>
							</section>
							<section class="bg-gray section-sm">
								<div class="">
									<div class="panel-pane pane-atajos">
										<div class="pane-content">
											<ul class="nav nav-icons">
												<li class=""><a href="#"><i class="fa fa-list text-redjus"></i><span class="text-redjus">Listas diarias</span></a></li>
												<li class=""><a href="#"><i class="fa fa-file text-redjus"></i><span class="text-redjus">Guía Judicial</span></a></li>
												<li class=""><a href="#"><i class="fa fa-info text-redjus"></i><span class="text-redjus">Información</span></a></li>
												<li class=""><a href="#"><i class="fa fa-file-text-o text-redjus"></i><span class="text-redjus">Guía de tramites</span></a></li>
												<li class=""><a href="#"><i class="fa fa-bank text-redjus"></i><span class="text-redjus">Ministerio público</span></a></li>
												<li class=""><a href="#"><i class="fa fa-eye text-redjus"></i><span class="text-redjus">Registro público</span></a></li>
											</ul>
										</div>
									</div>
								</div>
							</section>

						</div>
					</div>
					<section>
						<div class="container">
							<div>
								<h2>SIJUM</h2>
								<div class="panel-separator"></div>
								<div class="panel-pane pane-atajos">
									<div class="pane-content">
										<div class="row panels-row">
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
													<div style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/article.jpeg);" class="panel-heading"></div>
													<div class="panel-body">
														<h4>La Corte Firmó un convenio con el Ministerio de Gobierno y el Ministerio de Seguridad</h4>
														<div class="text-muted">
															<p>Se trata de un acuerdo para establecer acciones conjuntas y coordinadas para optimizar los servicios que se prestan a la comunidad</p>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
													<div style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/article1.jpeg);" class="panel-heading"></div>
													<div class="panel-body">
														<h4>Comenzó el Encuentro Nacional sobre Reforma de la Justicia Civil en Argentina</h4>
														<div class="text-muted">
															<p>Los ejes centrales a desarrollar fueron la oralidad, la capacidad de gestión, un juez con mayor protagonismo y procesos más ágiles.</p>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
													<div style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/article2.jpeg);" class="panel-heading"></div>
													<div class="panel-body">
														<h4>En Godoy Cruz funcionará el primer Puntos de Encuentro Familiar de la Justicia</h4>
														<div class="text-muted">
															<p>A través de un convenio firmado entre la municipalidad y la Suprema Corte de Justicia, se llevará...</p>
														</div>
													</div>
												</a>
											</div>

										</div>
									</div>
								</div>
								<div class="panel-separator"></div>
								<div class="panel-pane pane-texto">
									<div class="pane-content">
										<div class="">
											<section class="m-y-0 p-y-1">
												<div class="">
													<h2>Importante</h2>
													<div class="row panels-row m-t-2">
														<div class="col-xs-12 col-sm-6 col-md-6">
															<a class="panel panel-default" href="estudiar">
																<div class="panel-body">
																	<div class="media">
																		<div class="media-left padding-20"> <i class="fa icono-arg-tramite-electronico fa-fw fa-2x text-primary"></i></div>
																		<div class="media-body">
																			<h3>GED</h3>
																			<p>Gestión Electrónica de Documentos</p>
																		</div>
																	</div>
																</div>
															</a>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-6">
															<a class="panel panel-default" href="">
																<div class="panel-body">
																	<div class="media">
																		<div class="media-left padding-20"> <i class="fa icono-arg-notificaciones fa-fw fa-2x text-primary"></i></div>
																		<div class="media-body">
																			<h3>Notificación Electrónica a los Peritos</h3>
																			<p>Acordada N°28.978. Instructivo</p>
																		</div>
																	</div>
																</div>
															</a>
														</div>
													</div>
												</div>
											</section>

										</div>
									</div>
								</div>
								<div class="panel-separator"></div>
								<div class="panel-pane pane-texto">
									<div class="pane-content">
										<div class="">
											<div class="">
												<h2 class="h3 m-b-2">Más consultados</h2>
												<div class="row row-flex">
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Ministerio Público Fiscal</h3> </a>
														<p class="text-muted">Denunciar hechos delictivos.</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Expres</h3></a>
														<p class="text-muted"> Nueva aplicación para préstamos de expedientes.</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="# "><h3 class="h5">Oficina de pequeñas causas y consumo</h3> </a>
														<p class="text-muted">Patricias Mendocinas 529 - 2° Piso - Ciudad</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Abogado/a del niño/a</h3> </a>
														<p class="text-muted">Dirección de Derechos Humanos y Acceso a la Justicia. Descarga Material Digital 2018</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Inscripción</h3></a>
														<p class="text-muted">Notificadores vespertinos para juicio por jurados.</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="# "><h3 class="h5">MGD</h3></a>
														<p class="text-muted">Dirección de la mujer, genero y diversidad. Dra. Carmen María Argibay</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Depositos judiciales</h3> </a>
														<p class="text-muted">Consulta de saldo y emisión de libranzas. Aplicativo + Tutorial</p>
													</div>
													<div class="col-xs-12 col-sm-6 col-lg-3 m-b-2"> <a href="#"><h3 class="h5">Certificado de Antecedentes Penales</h3> </a>
														<p class="text-muted">Es un documento en formato electrónico que certifica que no tenés antecedentes.</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<section class="bg-gray">
						<div class="container">
							<div class="panel-pane pane-area-estructura">
								<div class="pane-content">
									<div class="row">
										<div class="col-md-9"></div>
									</div>
								</div>
							</div>
							<div class="panel-separator"></div>
							<div class="panel-pane pane-titulo">
								<div class="pane-content">
									<h3 class="activities-sidbar">Accesos Rápidos</h3></div>
								</div>
								<div class="panel-separator"></div>
								<div class="panel-pane pane-atajos">
									<div class="pane-content">
										<div class="row panels-row">
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Formularios de Interés</h3>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Autorizaciones de Viajes</h3>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Agenda Audiencias Penal</h3>
																<div class="text-muted">
																	<p>Uso interno</p>
																</div>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>GED (Gestión electrónica de documentos)</h3>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Validar Documentos (Uso interno)</h3>
															</div>
														</div>
													</div>
												</a>
											</div>

											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>DDJJ Acordada 28944 Ley 9001(CONTESTA DEMANDA)</h3>

															</div>
														</div>
													</div>
												</a>
											</div>

											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Sistema de Notificaciones Electrónicas para Peritos y Abogados</h3>
															</div>
														</div>
													</div>
												</a>
											</div>
											<div class="col-xs-12 col-sm-6 col-md-4">
												<a href="#" class="panel panel-default">
													<div class="panel-body">
														<div class="media">
															<div class="media-body">
																<h3>Liquidación de Haberes</h3>
															</div>
														</div>
													</div>
												</a>
											</div>

										</div>
									</div>
								</div>
							</div>
						</section>
						<section>
							<div class="container">
								<div class="panel-pane pane-texto">
									<div class="pane-content">
										<div class="">
											<h2 class="h3">Servicios</h2></div>
										</div>
									</div>
									<div class="panel-separator"></div>
									<div class="panel-pane pane-atajos">
										<div class="pane-content">
											<div class="row panels-row">
												<div class="col-xs-12 col-sm-6 col-md-3">
													<a href="#" class="panel panel-default panel-icon panel-primary">
														<div class="panel-heading hidden-xs"><i class="fa icono-arg-justicia"></i></div>
														<div class="panel-body">
															<h3><span class="visible-xs-inline"><i class="fa icono-arg-apostilla"></i>&nbsp; </span>Fuero Penal</h3></div>
														</a>
													</div>
													<div class="col-xs-12 col-sm-6 col-md-3">
														<a href="#" class="panel panel-default panel-icon panel-primary">
															<div class="panel-heading hidden-xs"><i class="fa icono-arg-apostilla"></i></div>
															<div class="panel-body">
																<h3><span class="visible-xs-inline"><i class="fa icono-arg-apostilla"></i>&nbsp; </span>Información Ciudadana sobre juicios por jurado</h3></div>
															</a>
														</div>
														<div class="col-xs-12 col-sm-6 col-md-3">
															<a href="#" class="panel panel-default panel-icon panel-primary">
																<div class="panel-heading hidden-xs"><i class="fa icono-arg-martillo"></i></div>
																<div class="panel-body">
																	<h3><span class="visible-xs-inline"><i class="fa icono-arg-martillo"></i>&nbsp; </span>Doctrina y Jurisprudencia</h3></div>
																</a>
															</div>
															<div class="col-xs-12 col-sm-6 col-md-3">
																<a href="#" class="panel panel-default panel-icon panel-primary">
																	<div class="panel-heading hidden-xs"><i class="fa icono-arg-familia-2"></i></div>
																	<div class="panel-body">
																		<h3><span class="visible-xs-inline"><i class="fa icono-arg-familia-2"></i>&nbsp; </span>Código Procesal de Familia y Violencia Familiar</h3></div>
																	</a>
																</div>
															</div>
														</div>
													</div>
												</div>
											</section>
											<section class="bg-gray">
												<div class="container">
													<div class="panel-pane pane-area-estructura">
														<div class="pane-content">
															<div class="row">
																<div class="col-md-9"></div>
															</div>
														</div>
													</div>
													<div class="panel-separator"></div>
													<div class="panel-pane pane-titulo">
														<div class="pane-content">
															<h3 class="activities-sidbar">Información de Interés</h3></div>
														</div>
														<div class="panel-separator"></div>
														<div class="panel-pane pane-atajos">
															<div class="pane-content">
																<div class="row panels-row">
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Formulario</h3>
																						<div class="text-muted">
																							<p>Ingreso de Causas CPCCyT ley 9.001 (MECC)</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Deposito</h3>
																						<div class="text-muted">
																							<p>Para recursos extraordinarios</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Formulario</h3>
																						<div class="text-muted">
																							<p>Demanda pequeñas causas</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Formularios</h3>
																						<div class="text-muted">
																							<p>DJ01 y DJ02</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Fallo Ley 7.722</h3>
																						<div class="text-muted">
																							<p>Caso "Mineria"</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>

																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Levantamiento suspensiones</h3>
																						<div class="text-muted">
																							<p>Aplicación Ley N° 26.773</p>
																						</div>
																					</div>
																				</div>
																			</div>
																		</a>
																	</div>

																	<div class="col-xs-12 col-sm-6 col-md-4">
																		<a href="#" class="panel panel-default">
																			<div class="panel-body">
																				<div class="media">
																					<div class="media-body">
																						<h3>Consejo de la magistratura</h3>
																						<div class="text-muted">
																							<p>Cronograma Anual de Aspirantes 2019</p>
																						</div>

																					</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
												<section>
												<div class="container">
													<div class="row">
														<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
															<?php echo do_shortcode('[filtrogob widget_name="filtrogob" id="4"]');?>
														</div>
													</div>
												</div>
												</section>
											</div>
										</div>