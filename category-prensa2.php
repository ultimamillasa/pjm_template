<?php get_header(); ?>
<?php $arrayImg = array(
	'http://190.105.227.247/~jus/wp-content/uploads/2019/06/article.jpeg',
	'http://190.105.227.247/~jus/wp-content/uploads/2019/07/civil-1.jpeg',
	'http://190.105.227.247/~jus/wp-content/uploads/2019/07/article-1.jpeg'
) ?>
<main role="main">
	<!-- section -->
	<div class="region region-content">
		<div id="block-system-main" class="block block-system clearfix">
			<div class="panel-pane pane-block pane-argentinagobar-search-apachesolrinput">
				<div class="pane-content">
					<section class="jumbotron" style="background-image: url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);">
						<div class="jumbotron_bar">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<ol class="breadcrumb">
											<li><a href="/">Inicio</a></li>
											<li class="active">SIJUM</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
						<div class="jumbotron_body">
							<div class="container">
								<div class="row">
									<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
										<h1>En Godoy Cruz funcionará el primer Puntos de Encuentro Familiar de la Justicia</h1>
										<p></p>
										<p>A través de un convenio firmado entre la municipalidad y la Suprema Corte de Justicia, se llevará...</p>
										<p></p>
									</div>
								</div>
							</div>
						</div>
						<div class="overlay"></div>
					</section>

				</div>
			</div>
			<section>
				<div class="container">
					<div>
						<div class="panel-separator"></div>
						<div class="panel-pane pane-atajos">
							<div class="pane-content">
								<div class="row panels-row">
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>La Corte Firmó un convenio con el Ministerio de Gobierno y el Ministerio de Seguridad</h4>
												<div class="text-muted">
													<p>Se trata de un acuerdo para establecer acciones conjuntas y coordinadas para optimizar los servicios que se prestan a la comunidad</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												<h3 class="panel-title">
													<i class="fa fa-calendar"></i>
													Agenda
												</h3>
											</div>
											<div class="panel-body">
												<ul class="media-list">
													<li class="media">
														<div class="media-left">
															<div class="panel panel-primary text-center date">
																<div class="panel-body day text-primary">
																	<?php echo date('d') ?> <?php echo ucfirst(get_the_time( 'F' )); ?>
																</div>
															</div>
														</div>
														<div class="media-body">
															<a href="#">
																Ciclo de capacitación a través del análisis de sentencias con perspectiva de género
															</a>
															<p>
																Vivamus pulvinar mauris eu placerat blandit. In euismod tellus vel ex vestibulum congue.
															</p>
														</div>
													</li>
												</ul>
												<a href="<?php echo home_url() ?>/agenda" class="btn btn-default btn-block">Ver Agenda Completa</a>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<h1>Redes sociales</h1>
									</div>
								</div>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="panel panel-default">
											<div class="embed-container">
												<iframe src="https://www.youtube.com/embed/LywUnoz5NjE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
											</div>
											<div class="panel-body">
												<p style="margin:0px !important;">Ingresa a nuestro canal <a href="https://www.youtube.com/channel/UCuHPY2aM063Y0_-ZMk1kCew/">SIJUM TV</a> para mas videos sobre Juicios por Jurados</p>
											</div>
										</div>


									</div>
									<div class="col-xs-12 col-sm-6 col-md-6">
										<div class="panel panel-default">
											<a data-height="450" class="twitter-timeline" href="https://twitter.com/PrensaJudicial">Tweets de Poder Judicial Mendoza</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

										</div>
									</div>
								</div>
								
								<div class="row panels-row" style="margin-top:15px;">
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>La Corte Firmó un convenio con el Ministerio de Gobierno y el Ministerio de Seguridad</h4>
												<div class="text-muted">
													<p>Se trata de un acuerdo para establecer acciones conjuntas y coordinadas para optimizar los servicios que se prestan a la comunidad</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>Comenzó el Encuentro Nacional sobre Reforma de la Justicia Civil en Argentina</h4>
												<div class="text-muted">
													<p>Los ejes centrales a desarrollar fueron la oralidad, la capacidad de gestión, un juez con mayor protagonismo y procesos más ágiles.</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-4">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>En Godoy Cruz funcionará el primer Puntos de Encuentro Familiar de la Justicia</h4>
												<div class="text-muted">
													<p>A través de un convenio firmado entre la municipalidad y la Suprema Corte de Justicia, se llevará...</p>
												</div>
											</div>
										</a>
									</div>

								</div>
							</div>
						</div>
						<div class="panel-separator"></div>
						<div class="panel-pane pane-atajos">
							<div class="pane-content">
								<div class="row panels-row">
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>La Corte Firmó un convenio con el Ministerio de Gobierno y el Ministerio de Seguridad</h4>
												<div class="text-muted">
													<p>Se trata de un acuerdo para establecer acciones conjuntas y coordinadas para optimizar los servicios que se prestan a la comunidad</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>Comenzó el Encuentro Nacional sobre Reforma de la Justicia Civil en Argentina</h4>
												<div class="text-muted">
													<p>Los ejes centrales a desarrollar fueron la oralidad, la capacidad de gestión, un juez con mayor protagonismo y procesos más ágiles.</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>En Godoy Cruz funcionará el primer Puntos de Encuentro Familiar de la Justicia</h4>
												<div class="text-muted">
													<p>A través de un convenio firmado entre la municipalidad y la Suprema Corte de Justicia, se llevará...</p>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="http://190.105.227.247/~jus/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
											<div style="background-image:url(<?php echo $arrayImg[array_rand($arrayImg, 1)]; ?>);" class="panel-heading"></div>
											<div class="panel-body">
												<h4>En Godoy Cruz funcionará el primer Puntos de Encuentro Familiar de la Justicia</h4>
												<div class="text-muted">
													<p>A través de un convenio firmado entre la municipalidad y la Suprema Corte de Justicia, se llevará...</p>
												</div>
											</div>
										</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</section></div></div>
			<!-- /section -->
		</main>
		<?php get_footer(); ?>
