<?php get_header(); ?>
<?php if ( has_post_thumbnail()) : ?>
	<?php $destacada = get_the_post_thumbnail_url();?>
<?php endif; ?>
<div class="panel-pane pane-imagen-destacada">
	<div class="pane-content">
		<section class="jumbotron mh-400" style="background-image: url(<?php echo $destacada ?>);">
			<div class="jumbotron_bar">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li><a href="<?php bloginfo('url') ?>"><?php bloginfo('name') ?></a></li>
								<li class=""><?php the_category(' > '); ?></li>
								<li class="active"><?php the_title(); ?></li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="jumbotron_body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<div class="container">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 col-md-offset-2">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="panel-pane pane-titulo">
					<div class="pane-content">
						<h1 class="activities-sidbar"><?php the_title(); ?></h1>
					</div>
				</div>
				<hr>
				<!--<span class="date"><?php _e( 'Categorised in: ', 'html5blank' ); the_category(', '); // Separated by commas ?></span>-->
				<?php the_content(); // Dynamic Content ?>

				<?php the_tags( __( 'Tags: ', 'html5blank' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>

				<!--<p><?php _e( 'This post was written by ', 'html5blank' ); the_author(); ?></p>-->

				<?php edit_post_link(); // Always handy to have Edit Post Links available ?>

			</article>
		</div>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	<?php //get_sidebar(); ?>
</div>
<?php get_footer(); ?>
