<footer class="main-footer bg-white">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<section class="block block-block clearfix"> <img class="image-responsive" width="250" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" >
					<p class="small text-dg"> AVDA. ESPAÑA 480<br>CIUDAD DE MENDOZA<br>ARGENTINA</p>
				</section>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<section id="block-menu-menu-footer-2" class="block block-menu clearfix">
						<h4 class="text-dg"><i class="fa fa-headphones"></i> Línea Directa</h4>
						<p><b><a href="tel:08006665878">0800 666 5878</a></b></p>
						<p class="text-dg">lun a vier 7:30 a 21Hs</p>
					</section>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<section id="block-menu-menu-footer-2" class="block block-menu clearfix">
						<h4 class="text-dg"><i class="fa fa-whatsapp"></i> Consultas WhatsApp</h4>
						<p><a href="https://wa.me/2615465866">261 546 5866</a></p>
					</section>
			</div>
		</div>
	</div>
</footer>

<!--<footer class="main-footer bg-primary">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
				<section class="block block-block clearfix"> <img class="image-responsive" width="250" src="<?php echo get_template_directory_uri(); ?>/img/logo_blanco.png" >
					<p class="text-muted small"> AVDA. ESPAÑA 480<br>CIUDAD DE MENDOZA<br>ARGENTINA</p>
				</section>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<section id="block-menu-menu-footer-2" class="block block-menu clearfix">
						<h4><i class="fa fa-headphones"></i> Línea Directa</h4>
						<p><b><a href="tel:08006665878">0800 666 5878</a></b></p>
						<p>lun a vier 7:30 a 21Hs</p>
					</section>
			</div>
			<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
					<section id="block-menu-menu-footer-2" class="block block-menu clearfix">
						<h4><i class="fa fa-whatsapp"></i> Consultas WhatsApp</h4>
						<p><a href="https://wa.me/2615465866">261 546 5866</a></p>
					</section>
			</div>
		</div>
	</div>
</footer>-->
<?php wp_footer(); ?>
</body>
</html>
