<?php /* Template Name: Micrositio 2 */ ?>
<?php get_header() ?>
<div class="panel-pane pane-imagen-destacada">
	<div class="pane-content">
		<section class="jumbotron" style="background-image: url(<?php echo get_template_directory_uri(); ?>/img/jurado.jpg);">
			<div class="jumbotron_bar">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--<ul class="list-inline pull-right">
								<li class="first leaf"><a href="/cultura/transparencia">Transparencia</a></li>
								<li class="last expanded dropdown"><a href="/" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Institucional <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="first leaf"><a href="/cultura/que-hacemos">¿Qué hacemos?</a></li>
										<li class="last leaf"><a href="/cultura/elencos">Elencos estables</a></li>
									</ul>
								</li>
							</ul>-->
							<ol class="breadcrumb">
								<li><a href="/">Juicios por Jurados</a></li>
								<li class="active"><a href="/educacion">Inicio</a></li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="jumbotron_body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
							<h1>Juicios por Jurados</h1>
							<p></p>
							<p>Comisión para la implementación, seguimiento, mejora y capacitación de "Juicios por Jurados populares (Ley N° 9106)</p>
							<p></p>
						</div>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</section>
	</div>
</div>
<section class="custom-section">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h3>Transmision Juicio por Jurado</h3>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
							<div class="video-container"><iframe width="560" height="315" src="https://www.youtube.com/embed/zM9eNXxgvrw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							<div class="list-group">
								<a href="#" class="list-group-item active">
									Eventos
								</a>
								<a class="list-group-item"><span class="label label-primary">01:00</span> Instrucciones del Jurado</a>
								<a class="list-group-item"><span class="label label-primary">02:00</span> Reglas de las deliberaciones</a>
								<a class="list-group-item"><span class="label label-primary">06:14</span> Interrogatorio de datos personales del imputado</a>
								<a class="list-group-item"><span class="label label-primary">10:30</span> Alegatos de Apertura</a>
								<a class="list-group-item"><span class="label label-primary">15:10</span> Presentación del caso, hechos y cargos</a>
								<a class="list-group-item"><span class="label label-primary">33:40</span> Toma la palabra la defensa</a>
							</div>
						</div>
					</div>
				</div>
			</section>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="activities-sidbar">Sorteo Anual de Jurados</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-md-6">
			<a href="/tema/hogar/hijo/embarazo" class="panel panel-default">
				<div class="panel-body">
					<div class="media">
						<div class="media-left padding-20"> <i class="fa fa-fw fa-3x icono-arg-declaracion-jurada text-primary"></i></div>
						<div class="media-body">
							<h3>Padron General de Jurados 2019</h3>
							<div class="text-muted">
								<p>En Mendoza el juicio por jurados es ley (Ley N° 9106) y todos los ciudadanos y ciudadanas tienen derecho a participar de los juicios criminales como jurados</p>
							</div>
						</div>
					</div>
				</div>
			</a>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6">
			<a href="/tema/hogar/hijo/embarazo" class="panel panel-default">
				<div class="panel-body">
					<div class="media">
						<div class="media-left padding-20"> <i class="fa fa-fw fa-3x icono-arg-consulta text-primary"></i></div>
						<div class="media-body">
							<h3>Sorteo Anual</h3>
							<div class="text-muted">
								<p>Números sorteados por Lotería de la Provincia de Mendoza que integrarán el Listado Anual de Jurados en las cuatro circunscripciones judiciales</p>
							</div>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<section class="custom-section">
	<div class="container">
		<div class="panel-pane pane-area-estructura">
			<div class="pane-content">
				<div class="row">
					<div class="col-md-9"></div>
				</div>
			</div>
		</div>
		<div class="panel-separator"></div>
		<div class="panel-pane pane-atajos">
			<div class="pane-content">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<h3 class="activities-sidbar">Información y capacitación</h3>
					</div>
				</div>
				<div class="row panels-row">
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<a href="#" class="panel panel-default panel-icon panel-primary">
							<div class="panel-heading hidden-xs">
								<i class="fa icono-arg-reclamo-consulta"></i>
							</div>
							<div class="panel-body">
								<h3>
									<span class="visible-xs-inline">
										<i class="fa icono-arg-reclamo-consulta"></i>
									</span>
									Manual
								</h3>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<a href="#" class="panel panel-default panel-icon panel-primary">
							<div class="panel-heading hidden-xs">
								<i class="fa icono-arg-chat"></i>
							</div>
							<div class="panel-body">
								<h3>
									<span class="visible-xs-inline">
										<i class="fa icono-arg-chat"></i>
									</span>
									Cursos Virtuales
								</h3>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<a href="#" class="panel panel-default panel-icon panel-primary">
							<div class="panel-heading hidden-xs">
								<i class="fa icono-arg-capacitacion"></i>
							</div>
							<div class="panel-body">
								<h3>
									<span class="visible-xs-inline">
										<i class="fa icono-arg-capacitacion"></i>
									</span>
									Cursos Presenciales
								</h3>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
						<a href="#" class="panel panel-default panel-icon panel-primary">
							<div class="panel-heading hidden-xs">
								<i class="fa icono-arg-comunidad"></i>
							</div>
							<div class="panel-body">
								<h3>
									<span class="visible-xs-inline">
										<i class="fa icono-arg-comunidad"></i>
									</span>
									Relación con la comunidad
								</h3>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
		<!--<section>
			<div class="container">
				<div class="panel-pane pane-texto">
					<div class="pane-content">
						<div class="">
							<h2 class="h3">Destacados</h2></div>
						</div>
					</div>
					<div class="panel-separator"></div>
					<div class="panel-pane pane-atajos">
						<div class="pane-content">
							<div class="row panels-row">
								<div class="col-xs-12 col-sm-6 col-md-4">
									<a href="https://www.justicia2020.gob.ar/" class="panel panel-default">
										<div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-06-49.png);" class="panel-heading"></div>
										<div class="panel-body">
											<h4>Capacitaciones</h4>
											<div class="text-muted">
												<p>Ciclo de capacitación a través del análisis de sentencias con perspectiva de género	</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<a href="/justicia/afianzar/caj" class="panel panel-default">
										<div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-16-59.png);" class="panel-heading"></div>
										<div class="panel-body">
											<h4>Capacitaciones</h4>
											<div class="text-muted">
												<p>Género y diversidad sexual</p>
											</div>
										</div>
									</a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<a href="/justicia/afianzar/politicacriminal" class="panel panel-default">
										<div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-24-21.png);" class="panel-heading"></div>
										<div class="panel-body">
											<h4>Seminario</h4>
											<div class="text-muted">
												<p>Empoderadas - La mujer hoy</p>
											</div>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>-->
			
			<section class="bg-gray custom-section">
				<div class="container">
					<div class="panel-pane pane-titulo">
						<div class="pane-content">
							<h3 class="activities-sidbar">Te puede interesar</h3></div>
						</div>
						<div class="panel-separator"></div>
						<div class="panel-pane pane-atajos">
							<div class="pane-content">
								<div class="row panels-row">
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="/denuncia-sobre-violencia-laboral-x" class="panel panel-default">
											<div class="panel-body">
												<div class="media">
													<div class="media-body">
														<h3>Institucional</h3>
														<div class="text-muted">
															
														</div>
													</div>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="/denunciar-violencia-institucional-x" class="panel panel-default">
											<div class="panel-body">
												<div class="media">
													<div class="media-body">
														<h3>Preguntas Frecuentes</h3>
														<div class="text-muted">
															
														</div>
													</div>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="/atencion-a-las-victimas-de-explotacion-sexual-y-grooming-x" class="panel panel-default">
											<div class="panel-body">
												<div class="media">
													<div class="media-body">
														<h3>Normativa</h3>
														<div class="text-muted">
															
														</div>
													</div>
												</div>
											</div>
										</a>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<a href="/derechoshumanos" class="panel panel-default">
											<div class="panel-body">
												<div class="media">
													<div class="media-body">
														<h3>Novedades</h3>
														<div class="text-muted">
															
														</div>
													</div>
												</div>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="panel-separator"></div>
						<div class="panel-pane pane-area-estructura">
							<div class="pane-content">
								<div class="row">
									<div class="col-md-9"></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php get_footer() ?>