<?php /* Template Name: Perfil */ ?>
<?php get_header() ?>
<?php $current_user = wp_get_current_user() ?>
<section class="jumbotron" style="background-image: url('http://demo.ultimamilla.com.ar/jus/img/perfil.jpg');">

	<div class="jumbotron_bar jumbotron-bar-transparent">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ol class="breadcrumb">
						<li><a href="/">Inicio</a></li>
						<li><a href="/transporte">Perfil</a></li>
						<li class="active"><?php echo $current_user->user_login ?></li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<div class="jumbotron_body p-t-0 p-b-1">
		<div class="container">
			<h1 class="h3 m-b-0"><?php echo $current_user->user_login ?>

			<a href="" data-toggle="tooltip" class="white-tooltip" title="" data-original-title="Identidad Validada">
				<i class="fa fa-check-circle text-primary"></i>
			</a>


		</h1>

		<p style="color:#fff;" class="lead"><small><b>CUIL &nbsp; 20-35091430-8 </b></small></p>
	</div>
</div>
<div class="overlay"></div>

</section>
<section id="section-main">
	<div class="container">
		<div class="row">


			<aside class="col-md-4" id="menu">
				<nav class="page-sidebar m-b-2">
					<ul class="nav nav-pills nav-stacked">
						<div class="hidden-xs hidden-sm p-r-3">
							<li class="menu active" data-id="inicio"><a href="#">Inicio</a></li>
							<li class="menu" data-id=""><a href="#">Expedientes</a></li>
							<li class="menu" data-id=""><a href="#">Tramites</a></li>
							<li class="menu" data-id=""><a href="#">Turnos</a></li>
							

							<li class="menu" data-id="salud"><a href="">Notificaciones</a></li>
							<hr class="m-y-1">
							<li class="btn-title p-l-2"><i class="fa fa-file-text-o"></i> &nbsp; Mis datos</li>
							<li class="menu"><a href="#">Datos básicos</a></li>
							<li class="menu"><a href="#">Mis dispositivos</a></li>
							<li class="menu"><a href="#">Configurar mi cuenta</a></li>
							<hr class="m-y-1">
							<li class="menu"><a href="#" class="logout">Cerrar la sesión</a></li>
						</div>
						<div class="visible-xs visible-sm">
							<li class="btn-title m-y-0"><a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-bars"></i> &nbsp; Menú</a></li>
							<div class="collapse" id="collapseExample">
								<hr class="m-y-1">
								<div class="row">
									<div class="col-sm-6">
										<li class="menu active" data-id="inicio"><a href="#">Inicio</a></li>
										<li class="menu" data-id=""><a href="#">Expedientes</a></li>
										<li class="menu" data-id=""><a href="#">Tramites</a></li>
										<li class="menu" data-id=""><a href="#">Turnos</a></li>
										<hr class="hidden-sm">
									</div>
									<div class="col-sm-6">
										<li class="menu" data-id="salud"><a href="">Notificaciones</a></li>
										<hr class="m-y-1">
										<li class="btn-title p-l-2"><i class="fa fa-file-text-o"></i> &nbsp; Mis datos</li>
										<li class="menu"><a href="#">Datos básicos</a></li>
										<li class="menu"><a href="#">Mis dispositivos</a></li>
										<li class="menu"><a href="#">Configurar mi cuenta</a></li>
										<hr class="m-y-1">
										<hr class="hidden-sm">

										<li class="menu"><a href="#" class="logout">Cerrar la sesión</a></li>
									</div>
								</div>
							</div>
						</div>
					</ul>
				</nav>
			</aside>
			<div class="col-md-8" id="main-content">
				<div class="panel panel-default panel-border-success">
					<div class="panel-body">
						<h2>Mis trámites y turnos</h2>
						<div class="media">
							<hr class="m-y-1">
							<div id="tramites">
							</div>
							<div class="media-left p-x-1 hidden-xs">
								<i class="fa fa-flag fa-fw fa-3x text-muted m-t-1"></i>
							</div>
							<div class="media-body media-middle">
								<h3 class="m-t-0">No tenés trámites ni turnos pendientes</h3>
								<p class="text-muted"></p>
							</div>
							<hr>




							<div class="media-left p-x-1 hidden-xs">
								<i class="fa fa-exclamation-circle fa-fw fa-2x text-muted m-t-1"></i>
							</div>
							<div class="media-body media-middle">
								<h3 class="m-t-0">No tenes expedientes vinculados</h3>
								<p><a href="">Ingresar número de expediente</a></p>
								<p>Esta consulta usa tu
									DNI 35091430
								</p>
							</div>
							<br>


						</div>

					</div>
				</div>

				<div class="panel panel-default panel-border-primary" id="MisNotificaciones">
					<div class="panel-body">
						<h2>Mis notificaciones</h2>

						<div class="media">

							<hr class="m-y-1">




							<div name="beneficiosANSES" class="">
								<div class="media-left p-x-1 hidden-xs">
									<i class="fa fa-calendar fa-fw fa-2x text-muted m-t-1"></i>
								</div>
								<div class="media-body media-middle">

									<h3 class="m-t-0">Notificaciones electronicas</h3>

									<p>No hay Notificaciones</p>


								</div>
							</div>
							<hr class="m-y-1">
						</div>

						<a href="" class="btn btn-link"><i class="fa fa-cog"></i> &nbsp; Configurar notificaciones</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>