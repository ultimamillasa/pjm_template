<?php /* Template Name: Ciudadano */ ?>
<?php get_header() ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1><?php the_title() ?></h1>
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-chat"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-chat"></i>&nbsp; </span>Chatbot Judicial</h3>
					<div class="text-muted"><p>Consultas online</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-familia-2"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-familia-2"></i>&nbsp; </span>Dirección  de derechos humanos</h3>
					<div class="text-muted"><p>Denucia hechos delictivos</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-mujer"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-mujer"></i>&nbsp; </span>Dirección de la mujer, genero y diversidad</h3>
					<div class="text-muted"><p>Consultas e información</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-marcador-ubicacion-2"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-marcador-ubicacion-2"></i>&nbsp; </span>Ubicación de oficinas en Tribunales</h3>
					<div class="text-muted"><p>Consultas e información</p></div>
				</div>
			</a>	
		</div>
	</div>
	<!-- Segunda fila -->
	<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-institucion"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-institucion"></i>&nbsp; </span>Ministerio Público Fiscal</h3>
					<div class="text-muted"><p>Denunciar hechos delictivos</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-comunidad"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-comunidad"></i>&nbsp; </span>Ministerio público de la defensa</h3>
					<div class="text-muted"><p>Consulta defensorías</p></div>
				</div>
			</a>	
		</div>
		
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-notificaciones"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-notificaciones"></i>&nbsp; </span>Oficina de pequeñas causas</h3>
					<div class="text-muted"><p>Consulta e información</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-micro"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-micro"></i>&nbsp; </span>Centro movil de Información Judicial</h3>
					<div class="text-muted"><p>Consulta e información</p></div>
				</div>
			</a>	
		</div>
	</div>
	<!-- Tercer fila -->
	<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-guia-tramites"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-guia-tramites"></i>&nbsp; </span>Guía de trámites</h3>
					<div class="text-muted"><p>Consulta de tramites frecuentes</p></div>
				</div>
			</a>	
		</div>
	</div>
</div>
<?php get_footer() ?>