<?php /* Template Name: Micrositio */ ?>
<?php get_header() ?>
<div class="panel-pane pane-imagen-destacada">
	<div class="pane-content">
		<section class="jumbotron" style="background-image: url(https://www.argentina.gob.ar/sites/default/files/styles/jumbotron/public/violencia-y-abuso.jpg?itok=E-H1WEIr);">
			<div class="jumbotron_bar">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!--<ul class="list-inline pull-right">
								<li class="first leaf"><a href="/cultura/transparencia">Transparencia</a></li>
								<li class="last expanded dropdown"><a href="/" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Institucional <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<li class="first leaf"><a href="/cultura/que-hacemos">¿Qué hacemos?</a></li>
										<li class="last leaf"><a href="/cultura/elencos">Elencos estables</a></li>
									</ul>
								</li>
							</ul>-->
							<ol class="breadcrumb">
								<li><a href="/">Direccion de Derechos Humanos</a></li>
								<li class="active"><a href="/educacion">Inicio</a></li>
							</ol>
						</div>
					</div>
				</div>
			</div>
			<div class="jumbotron_body">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-md-8 col-md-offset-2 text-center">
							<h1>Dirección de la Mujer, Genéro y diversidad</h1>
							<p></p>
							<p>In molestie id mi eu faucibus. Praesent ac aliquet sem. Vestibulum eget mi finibus, pretium ligula et, interdum est. Aliquam erat volutpat. Nunc rhoncus rutrum libero ultricies condimentum. Quisque mauris est, vehicula id pulvinar quis, congue blandit sapien.</p>
							<p></p>
						</div>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</section>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h3 class="activities-sidbar">Accesos rápidos</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs">
					<i class="fa icono-arg-consulta"></i>
				</div>
				<div class="panel-body">
					<h3>
						<span class="visible-xs-inline">
							<i class="fa icono-arg-consulta"></i>
						</span>
						Consultas
					</h3>
				</div>
			</a>
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs">
					<i class="fa icono-arg-documento"></i>
				</div>
				<div class="panel-body">
					<h3>
						<span class="visible-xs-inline">
							<i class="fa icono-arg-documento"></i>
						</span>
						Documentos
					</h3>
				</div>
			</a>
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs">
					<i class="fa icono-arg-notificaciones"></i>
				</div>
				<div class="panel-body">
					<h3>
						<span class="visible-xs-inline">
							<i class="fa icono-arg-notificaciones"></i>
						</span>
						Novedades
					</h3>
				</div>
			</a>
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs">
					<i class="fa icono-arg-exporta-facil"></i>
				</div>
				<div class="panel-body">
					<h3>
						<span class="visible-xs-inline">
							<i class="fa icono-arg-exporta-facil"></i>
						</span>
						Enlaces
					</h3>
				</div>
			</a>
		</div>
	</div>
</div>
<section>
    <div class="container">
        <div class="panel-pane pane-area-estructura">
            <div class="pane-content">
                <div class="row">
                    <div class="col-md-9"></div>
                </div>
            </div>
        </div>
        <div class="panel-separator"></div>
        <div class="panel-pane pane-atajos">
            <div class="pane-content">
            	<div class="row">
            		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            			<h3 class="activities-sidbar">Donde consultar</h3>
            		</div>
            	</div>
                <div class="row panels-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="http://www.spf.gob.ar/www/estadisticas-indicadores-mapa/catcms/91/Mapa-Indicadores" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Primera Circunscripción Judicial</h3>
                                        <div class="text-muted">
                                            <p>Gran Mendoza</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="http://www.spf.gob.ar/www/denuncias" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Tercera Circunscripción Judicial</h3></div>
                                        <div class="text-muted">
                                            <p>San Martín, Rivadavia, Junín, Santa Rosa y La Paz</p>
                                        </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="http://www.spf.gob.ar/www/asociaciones_civiles" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Cuarta Circunscripción Judicial</h3></div>
                                        <div class="text-muted">
                                            <p>Tunuyán, Tupungato, San Carlos</p>
                                        </div>	
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="panel-pane pane-texto">
            <div class="pane-content">
                <div class="">
                    <h2 class="h3">Destacados</h2></div>
            </div>
        </div>
        <div class="panel-separator"></div>
        <div class="panel-pane pane-atajos">
            <div class="pane-content">
                <div class="row panels-row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="https://www.justicia2020.gob.ar/" class="panel panel-default">
                            <div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-06-49.png);" class="panel-heading"></div>
                            <div class="panel-body">
                                <h4>Capacitaciones</h4>
                                <div class="text-muted">
                                    <p>Ciclo de capacitación a través del análisis de sentencias con perspectiva de género	</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="/justicia/afianzar/caj" class="panel panel-default">
                            <div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-16-59.png);" class="panel-heading"></div>
                            <div class="panel-body">
                                <h4>Capacitaciones</h4>
                                <div class="text-muted">
                                    <p>Género y diversidad sexual</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <a href="/justicia/afianzar/politicacriminal" class="panel panel-default">
                            <div style="background-image:url(http://190.105.227.247/~jus/wp-content/uploads/2019/07/Captura-de-pantalla-de-2019-07-11-16-24-21.png);" class="panel-heading"></div>
                            <div class="panel-body">
                                <h4>Seminario</h4>
                                <div class="text-muted">
                                    <p>Empoderadas - La mujer hoy</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-gray">
    <div class="container">
        <div class="panel-pane pane-titulo">
            <div class="pane-content">
                <h3 class="activities-sidbar">Te puede interesar</h3></div>
        </div>
        <div class="panel-separator"></div>
        <div class="panel-pane pane-atajos">
            <div class="pane-content">
                <div class="row panels-row">
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="/denuncia-sobre-violencia-laboral-x" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Denunciar violencia laboral</h3>
                                        <div class="text-muted">
                                            <p>Si sos víctima de abuso de poder con el fin de excluirte o someterte mediante acoso psicológico, sexual o agresión física.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="/denunciar-violencia-institucional-x" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Denunciar violencia institucional</h3>
                                        <div class="text-muted">
                                            <p>Si tus derechos son violados por un funcionario público podés denunciarlo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="/atencion-a-las-victimas-de-explotacion-sexual-y-grooming-x" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Víctimas de explotación sexual y grooming</h3>
                                        <div class="text-muted">
                                            <p>Te asesoramos en la denuncia de los delitos de grooming, pornografía y prostitución infantil.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <a href="/derechoshumanos" class="panel panel-default">
                            <div class="panel-body">
                                <div class="media">
                                    <div class="media-body">
                                        <h3>Derechos Humanos y Pluralismo Cultural</h3>
                                        <div class="text-muted">
                                            <p>Promovemos los valores del encuentro, la diversidad, la convivencia, el diálogo y el pluralismo cultural.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-separator"></div>
        <div class="panel-pane pane-area-estructura">
            <div class="pane-content">
                <div class="row">
                    <div class="col-md-9"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>