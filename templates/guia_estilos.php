<?php /* Template Name: Guia de estilos */ ?>
<?php get_header() ?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h3>Templates</h3>

        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <a href="<?php echo home_url() ?>/category/prensa" class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-body">
                            <h3>Prensa</h3>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <a href="<?php echo home_url() ?>/2019/06/25/comenzo-el-encuentro-nacional-sobre-reforma-de-la-justicia-civil-en-argentina/" class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-body">
                            <h3>Noticia</h3>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <a href="<?php echo home_url() ?>/mi-perfil" class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-body">
                            <h3>Perfil</h3>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <a href="<?php echo home_url() ?>/micrositio" class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-body">
                            <h3>Micrositio</h3>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
            <a href="<?php echo home_url() ?>/agenda" class="panel panel-default">
                <div class="panel-body">
                    <div class="media">
                        <div class="media-body">
                            <h3>Agenda</h3>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 hidden-xs" id="menuLateral" data-seccion="2">
            <h4 class="text-muted m-t-4">Componentes</h4>
            <div class="page-sidebar">
                <ul class="nav nav-pills nav-stacked sticky">
                    <li><a data-seccion="2" href="#paneles">Paneles</a></li>
                    <li><a data-seccion="3" href="#destacados">Destacados con íconos y números</a></li>
                    <li><a data-seccion="4" href="#alertas">Alertas</a></li>
                    <li><a data-seccion="5" href="#desplegables">Desplegables</a></li>
                    <li><a data-seccion="6" href="#grillas">Grillas</a></li>
                    <li><a data-seccion="7" href="#tabs">Pestañas</a></li>
                    <li><a data-seccion="8" href="#formularios">Formularios</a></li>
                    <li><a data-seccion="9" href="#tablas">Tablas</a></li>
                    <li><a data-seccion="10" href="#iconos">Iconos</a></li>

                </ul>
            </div>
        </div>

        <div class="col-sm-9 col-xs-12">

            <section>
                <article class="row" id="paneles">
                    <div class="col-sm-12">
                        <h1>Paneles</h1>

                        <p>Los paneles de los listados de paneles son enteramente cliqueables y por lo general se usan en listados horizontales o grillas.</p>

                        <hr>

                        <fieldset>

                            <h2>Paneles chicos</h2>
                            <p>Ofrecen un nivel de jerarquía bajo o mediano.</p>

                            <h3>Panel simple</h3>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <h3>Guía de tramites</h3>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <h3>Guía judicial</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel panel-default<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
										    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Ministerio de Economía<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
										<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Panel simple con texto</h3>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <h3>GED</h3>
                                            <p class="text-muted">Gestión electronica de documentos</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <h3>Certificado de Antecedentes Penales</h3>
                                            <p class="text-muted">Es un documento en formato electrónico que certifica que no tenés antecedentes.</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel panel-default<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
											  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
											    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Ministerio de Economía<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
											    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text-muted<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>consectetur adipisicing elit. Hic, eaque nostrum molestiae deleniti esse rati<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
											  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
											<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Panel simple con ícono y texto</h3>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <div class="media">
                                                <div class="media-left padding-5">
                                                    <i class="fa fa-phone fa-fw fa-3x text-secondary"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h3>El estado del estado</h3>
                                                    <p class="text-muted">Diagnóstico de la Administración Pública Nacional en diciembre de 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <div class="media">
                                                <div class="media-left padding-5">
                                                    <i class="fa fa-phone fa-fw fa-3x text-primary"></i>
                                                </div>
                                                <div class="media-body">
                                                    <h3>El estado del estado</h3>
                                                    <p class="text-muted">Diagnóstico de la Administración Pública Nacional en diciembre de 2015</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel panel-default<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
												  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
												    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
												      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media-left padding-5<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
												        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>fa fa-phone fa-fw fa-3x text-secondary<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span>
												      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
												      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
												        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>El estado del estado<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
												        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text-muted<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Diagnóstico de la Administración Pública Nacional en diciembre de 2015<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
												      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
												    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
												  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
												<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <br>
                            <h2>Paneles grandes</h2>
                            <p>Ofrecen un nivel de jerarquía alto.</p>

                            <h3>Panel destacado con ícono</h3>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="panel panel-default panel-icon panel-primary" href="#">
                                        <div class="panel-heading hidden-xs"><i class="fa fa-file"></i></div>
                                        <div class="panel-body">
                                            <h3><span class="visible-xs-inline"><i class="fa fa-file text-secondary"></i>&nbsp; </span>Ministerio de Economía</h3>
                                            <p class="text-muted">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="panel panel-default panel-icon panel-secondary" href="#">
                                        <div class="panel-heading hidden-xs"><i class="fa fa-file"></i></div>
                                        <div class="panel-body">
                                            <h3><span class="visible-xs-inline"><i class="fa fa-file text-secondary"></i>&nbsp; </span>Ministerio de Economía</h3>
                                            <p class="text-muted">quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel panel-default panel-icon panel-secondary<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading hidden-xs<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>fa fa-file<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>visible-xs-inline<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>fa fa-file text-secondary<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span>  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>Ministerio de Economía<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text-muted<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
													<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Panel destacado con imagen</h3>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">

                                        <div class="panel-body">
                                            <div class="media-body">
                                                <h3>El estado del estado</h3>
                                                <p>Todos los servicios por si sos empleado, monotributista, querés capacitarte o estás buscando trabajo.</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a class="panel panel-default" href="#">
                                        <div class="panel-body">
                                            <div class="media-body">
                                                <h3>El estado del estado</h3>
                                                <p>Todos los servicios por si sos empleado, monotributista, querés capacitarte o estás buscando trabajo.</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel panel-default<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading<span class="token punctuation">"</span></span><span class="token style-attr language-css"><span class="token attr-name"> <span class="token attr-name">style</span></span><span class="token punctuation">="</span><span class="token attr-value"><span class="token property">background-image</span><span class="token punctuation">:</span> <span class="token url">url(imagen.jpg)</span></span><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
														    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
														      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>El estado del estado<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
														      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Todos los servicios por si sos empleado, monotributista, querés capacitarte o estás buscando trabajo.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
														    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section>
                <article class="row" id="destacados">
                    <div class="col-sm-12">
                        <h1>Destacados con íconos y números</h1>

                        <p>Los íconos y números son listados con información solamente, es decir que no son cliqueables.</p>

                        <hr>

                        <fieldset>

                            <h2>Íconos</h2>
                            <p>Sirven para mostrar conceptos como principios, valores, funcionalidades y otros atributos.</p>

                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="icon-item">
                                        <i class="fa fa-users text-success"></i>
                                        <h3>Cercana</h3>
                                        <p>Conoce tus necesidades y adopta sus servicios a ellas.</p>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="icon-item">
                                        <i class="fa fa-search text-primary"></i>
                                        <h3>Transparente</h3>
                                        <p>Brinda mayor información y certeza sobre sus actos de gobierno.</p>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-4">
                                    <div class="icon-item">
                                        <i class="fa fa-users"></i>
                                        <h3>Participativa</h3>
                                        <p>Trabaja en equipo con los empleados públicos y la gente.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>icon-item<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
															  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>fa fa-users text-success<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span>
															  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Cercana<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
															  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Conoce tus necesidades y adopta sus servicios a ellas.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
															<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h2>Números</h2>
                            <p>Sirven para mostrar cifras estadísticas. </p>

                            <div class="row numbers">
                                <div class="col-md-4">
                                    <div class="h2 text-success">392.493</div>
                                    <p class="lead">Visitas últimos 30 días</p>
                                    <p class="text-muted">+11,06% vs mes anterior</p>
                                </div>
                                <div class="col-md-4">
                                    <div class="h2 text-gray">33 <small>%</small></div>
                                    <p class="lead">Visitas este mes</p>
                                    <p class="text-muted">del 1 al 26 de Febrero</p>
                                </div>
                                <div class="col-md-4">
                                    <div class="h2">5 <small>de 10</small></div>
                                    <p class="lead">Personas conectadas</p>
                                    <p class="text-muted">Duramente los últimos 30 días.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row numbers<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-md-4<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>h2 text-success<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>392.493<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>lead<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Visitas últimos 30 días<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>text-muted<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>+11,06% vs mes anterior<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section>
                <article class="row" id="alertas">
                    <div class="col-sm-12">
                        <h1>Alertas</h1>

                        <hr>

                        <h2>Significado de alertas según color</h2>

                        <fieldset>

                            <h3>Información</h3>
                            <p>Sirve para mostrar información destacada pero adicional, es decir que el usuario puede o no prestarle atención.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																	  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Acción exitosa</h3>
                            <p>Sirve para indicar que el usuario realizó una acción correctamente, como cuando guarda cambios en una página.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-success">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-success<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Información importante</h3>
                            <p>Sirve para llamar la atención del usuario y que preste atención a cierta información o acción que realizó o está por realizar.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-warning">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-warning<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Mensaje de error o acción peligrosa</h3>
                            <p>Sirve para llamar mucho del atención del usuario y que preste atención a cierta información o acción peligrosa o irreversible que está por realizar, o también para mostrar errores sobre algo que realizó.</p>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-danger">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-danger<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																				<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Información complementaria (well)</h3>
                            <p>Sirve para destacar un contenido separándolo del cuerpo de texto pero sin agregarle un significado como lo tienen las alertas con colores.</p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="well m-t-0">
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>well<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																					Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!
																					<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h2>Variaciones de alertas</h2>

                            <h3 class="h4">Simple (sólo texto)</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																						  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																						<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3 class="h4">Con título</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <h5>Sacá turnos en línea para vacunarte contra</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																							  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																							<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3 class="h4">Con botón</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <h5>Sacá turnos en línea para vacunarte contra</h5>
                                        <p class="margin-20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                        <button type="button" class="btn btn-primary">GUARDAR</button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>button<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>GUARDAR<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
																								<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3 class="h4">Con ícono</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <div class="media">
                                            <div class="media-left">
                                                <i class="fa fa-arrow-circle-o-right fa-fw fa-4x"></i>
                                            </div>
                                            <div class="media-body">
                                                <h5>Sacá turnos en línea para vacunarte contra</h5>
                                                <p class="margin-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																									  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																									    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media-left<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																									      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>i</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>fa fa-arrow-circle-o-right fa-fw fa-4x<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>i</span><span class="token punctuation">&gt;</span></span>
																									    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																									    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>media-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																									      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h5</span><span class="token punctuation">&gt;</span></span>Sacá turnos en línea para vacunarte contra<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h5</span><span class="token punctuation">&gt;</span></span>
																									      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>margin-0<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																									    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																									  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																									<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3 class="h4">Cerrable</h3>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        <h5>Sacá turnos en línea para vacunarte contra</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert alert-info alert-dismissible<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>button<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>close<span class="token punctuation">"</span></span> <span class="token attr-name">data-dismiss</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>alert<span class="token punctuation">"</span></span> <span class="token attr-name">aria-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Close<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span class="token attr-name">aria-hidden</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>true<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>×<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
																										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h5</span><span class="token punctuation">&gt;</span></span>Sacá turnos en línea para vacunarte contra<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h5</span><span class="token punctuation">&gt;</span></span>
																										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																										<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section>
                <article class="row" id="desplegables">
                    <div class="col-sm-12">
                        <h1>Desplegables</h1>

                        <hr>
                        <fieldset>

                            <h3>Usando un link</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-12">
                                        <a data-toggle="collapse" href="#footwear" aria-expanded="false" aria-controls="footwear">Desplegar texto oculto</a>
                                        <div class="collapse" id="footwear">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#desplegable<span class="token punctuation">"</span></span> <span class="token attr-name">aria-expanded</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>false<span class="token punctuation">"</span></span> <span class="token attr-name">aria-controls</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>desplegable<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Desplegar texto oculto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span>
																											<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>desplegable<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																											  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Texto oculto.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																											<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Usando un botón</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#footwear2" aria-expanded="false" aria-controls="footwear2">Abrir</button>
                                        <div class="collapse" id="footwear2">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>button</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>btn btn-primary<span class="token punctuation">"</span></span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>button<span class="token punctuation">"</span></span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">data-target</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#desplegable<span class="token punctuation">"</span></span> <span class="token attr-name">aria-expanded</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>false<span class="token punctuation">"</span></span> <span class="token attr-name">aria-controls</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>desplegable<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Abrir<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>button</span><span class="token punctuation">&gt;</span></span>
																												<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>desplegable<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																												  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Texto oculto.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																												<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Panel desplegable</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-4">
                                        <div class="panel-group">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
																																	<a data-toggle="collapse" href="#collapse1">Desplegar panel</a>
																																</h4>
                                                </div>
                                                <div id="collapse1" class="panel-collapse collapse">
                                                    <div class="panel-body">Panel Body</div>
                                                    <div class="panel-footer">Panel Footer</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-group<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h4</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-title<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#collapse1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Desplegar panel
																													      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h4</span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse1<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-collapse collapse<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Panel Body<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-footer<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Panel Footer<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Acordeón</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-12">
                                        <div class="panel-group" id="accordion">
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
																																		<a data-toggle="collapse" data-parent="#accordion" href="#collapse11">Collapsible Group 1</a>
																																	</h4>
                                                </div>
                                                <div id="collapse11" class="panel-collapse collapse in">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
																																		<a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Collapsible Group 2</a>
																																	</h4>
                                                </div>
                                                <div id="collapse2" class="panel-collapse collapse">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                            <div class="panel">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
																																		<a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Collapsible Group 3</a>
																																	</h4>
                                                </div>
                                                <div id="collapse3" class="panel-collapse collapse">
                                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-group<span class="token punctuation">"</span></span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>accordion<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h4</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-title<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">data-parent</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#accordion<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#collapse1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Collapsible Group 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h4</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse1<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-collapse collapse in<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h4</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-title<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">data-parent</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#accordion<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#collapse2<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Collapsible Group 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h4</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse2<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-collapse collapse<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-heading<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h4</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-title<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse<span class="token punctuation">"</span></span> <span class="token attr-name">data-parent</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#accordion<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#collapse3<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Collapsible Group 3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h4</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>collapse3<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-collapse collapse<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>panel-body<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisi onsequat.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													</code>
																												</pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section>
                <article class="row" id="grillas">
                    <div class="col-sm-12">
                        <h1>Grillas</h1>
                        <p>Las grillas de Poncho se basan en las de bootstrap, pero recomendamos usar sólo las siguientes combinaciones para mantener una lógica entre los diferentes sitios del Gobierno.</p>

                        <hr>

                        <fieldset>

                            <h2>Cómo usar las grillas</h2>
                            <p>Las grillas siempre tienen que tener esta estructura.</p>
                            <ul>
                                <li>container
                                    <ul>
                                        <li>row
                                            <ul>
                                                <li>col</li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <p>Si se omiten elementos pueden haber diferencias de márgenes y otros comportamientos inadecuados.</p>

                            <hr>

                            <h2>Grillas recomendadas</h2>

                            <h3>12 columnas</h3>
                            <p>Se usa para tablas con mucha información, y para headers y footers de página.</p>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-12 col-ejemplo">
                                        Ancho de 12 columnas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>container<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-12<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																													      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																													    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																													<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>8 columnas</h3>
                            <p>Se usa para casi todo tipo de contenido (párrafos, imagenes, videos, tablas con poca información). Se puede usar centrado o alineado a la izquierda.</p>

                            <h4>Centrado</h4>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-8 col-sm-offset-2 col-ejemplo">
                                        Ancho de 8 columnas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>container<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-8 col-sm-offset-2<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																														      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																														    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																														<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h4>Alineado a la izquierda</h4>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-8 col-ejemplo">
                                        Ancho de 8 columnas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>container<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																															  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																															    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																															      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit iure quod, quae architecto animi corporis dolorum odio? Assumenda itaque eos laboriosam at? Voluptatem maxime quam quas error possimus tempore ullam!<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																															    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																															  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																															<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>4 columnas</h3>
                            <p>Se usa en conjunto con 8 columnas, para mostrar navegación (a la izquierda) o información relacionada (a la derecha).</p>

                            <h4>A la izquierda</h4>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-4 col-ejemplo">
                                        Ancho de 4 columnas
                                    </div>
                                    <div class="col-sm-8 col-ejemplo">
                                        Ancho de 8 columnas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>container<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-4<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h4>A la derecha</h4>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-8 col-ejemplo">
                                        Ancho de 8 columnas
                                    </div>
                                    <div class="col-sm-4 col-ejemplo">
                                        Ancho de 4 columnas
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>container<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																	  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																	    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																	      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																	    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																	    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-4<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																	      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																	    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																	  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section>
                <article class="row" id="tabs">
                    <div class="col-sm-12">
                        <h1>Pestañas</h1>

                        <hr>
                        <fieldset>
                            <h3>Tabs</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <ul class="nav nav-tabs" style="margin-top: 0;">
                                        <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                                        <li><a data-toggle="tab" href="#seccion">Sección</a></li>
                                        <li><a data-toggle="tab" href="#contacto">Contacto</a></li>
                                    </ul>
                                    <div class="tab-content bg-white" style="padding:20px;border: 1px solid #ddd;border-top:none;">
                                        <div id="home" class="tab-pane fade in active">
                                            <h3>HOME</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                        <div id="seccion" class="tab-pane fade">
                                            <h3>Sección</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                        <div id="contacto" class="tab-pane fade">
                                            <h3>Contacto</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ul</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>nav nav-tabs<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#home<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Home<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#seccion<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#contacto<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>ul</span><span class="token punctuation">&gt;</span></span>
																																		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-content bg-white<span class="token punctuation">"</span></span><span class="token style-attr language-css"><span class="token attr-name"> <span class="token attr-name">style</span></span><span class="token punctuation">="</span><span class="token attr-value"><span class="token property">padding</span><span class="token punctuation">:</span>20px<span class="token punctuation">;</span><span class="token property">border</span><span class="token punctuation">:</span> 1px solid #ddd<span class="token punctuation">;</span><span class="token property">border-top</span><span class="token punctuation">:</span>none<span class="token punctuation">;</span></span><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>home<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade in active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>HOME<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>seccion<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>contacto<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																		    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																		  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Pills</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <ul class="nav nav-pills" style="margin-top: 0;">
                                        <li class="active"><a data-toggle="tab" href="#home1">Home</a></li>
                                        <li><a data-toggle="tab" href="#seccion1">Sección</a></li>
                                        <li><a data-toggle="tab" href="#contacto1">Contacto</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="home1" class="tab-pane fade in active">
                                            <h3>Home</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                        <div id="seccion1" class="tab-pane fade">
                                            <h3>Sección</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                        <div id="contacto1" class="tab-pane fade">
                                            <h3>Contacto</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ul</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>nav nav-pills<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#home<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Home<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#seccion<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#contacto<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>ul</span><span class="token punctuation">&gt;</span></span>
																																			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-content bg-white<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>home<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade in active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>HOME<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>seccion<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>contacto<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																			    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas, facilis, eveniet.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																			  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                            <h3>Pills verticales</h3>

                            <div class="bd-example">
                                <div class="row row-ejemplo">
                                    <div class="col-sm-3">
                                        <h4 class="text-muted">Title</h4>
                                        <ul class="nav nav-pills nav-stacked" style="margin-top: 0;">
                                            <li class="active"><a data-toggle="tab" href="#home2">Home</a></li>
                                            <li><a data-toggle="tab" href="#seccion2">Sección</a></li>
                                            <li><a data-toggle="tab" href="#contacto2">Contacto</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="tab-content">
                                            <div id="home2" class="tab-pane fade in active">
                                                <h3>HOME</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                            </div>
                                            <div id="seccion2" class="tab-pane fade">
                                                <h3>Sección</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                            </div>
                                            <div id="contacto2" class="tab-pane fade">
                                                <h3>Contacto</h3>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus porro officia hic fugit nihil, rem dolorum inventore nulla, dignissimos aut. Beatae omnis asperiores incidunt, reprehenderit suscipit eligendi? Voluptas, facilis, eveniet.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>row<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-3<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h4</span><span class="token punctuation">&gt;</span></span>Title<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h4</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>ul</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>nav nav-pills nav-stacked<span class="token punctuation">"</span></span><span class="token style-attr language-css"><span class="token attr-name"> <span class="token attr-name">style</span></span><span class="token punctuation">="</span><span class="token attr-value"><span class="token property">margin-top</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span></span><span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#home<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Home<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#seccion<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>li</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>a</span> <span class="token attr-name">data-toggle</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab<span class="token punctuation">"</span></span> <span class="token attr-name">href</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>#contacto<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>a</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>li</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>ul</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>col-sm-9<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-content<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>home<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade in active<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>HOME<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>seccion<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Sección<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">id</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>contacto<span class="token punctuation">"</span></span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>tab-pane fade<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>h3</span><span class="token punctuation">&gt;</span></span>Contacto<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>h3</span><span class="token punctuation">&gt;</span></span>
																																				        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>p</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>p</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
																																				<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>
                                </div>
                            </div>

                        </fieldset>

                    </div>

                </article>
            </section>
            <section id="formularios">
                <h1>Formularios</h1>

                <div class="panel panel-default" style="background-color: #f9f9f9;">
                    <div class="panel-body">
                        <h1>Titulo de formulario</h1>

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quam velit, vulputate eu pharetra nec, mattis ac neque. Duis vulputate commodo.</p>

                        <hr>

                        <div class="wizard m-b-3">
                            <p class="text-muted">Paso 2 de 4</p>
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">
                                    <span class="sr-only">Paso 1 de 4</span>
                                </div>
                            </div>
                        </div>

                        <form>

                            <h2 class="m-t-2">Título de paso del formulario</h2>

                            <!-- EJEMPLO DE ERRORES PARA LECTOR DE VOZ -->
                            <div id="errores" aria-live="polite" class="alert alert-danger" style="display:none"></div>

                            <!-- DATOS PERSONALES -->
                            <fieldset>
                                <legend>
                                    <h3>Datos personales</h3></legend>

                                <div class="row">
                                    <div class="col-md-4 form-group item-form">
                                        <label for="cuil">CUIL</label>
                                        <p class="help-block"><a href="https://www.anses.gob.ar/constancia-de-cuil/" aria-label="Ingresá tu CUIL. Si no lo sabés consultá en este enlace de ANSES" target="_blank">Consultá tu CUIL</a></p>
                                        <input type="number" class="form-control" id="cuil" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá tu CUIL
                                            <br>El CUIL tiene un formato no válido</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="nombre">Nombres</label>
                                        <input type="text" name="name" class="form-control" id="nombre" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá tu nombre</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="apellido">Apellidos</label>
                                        <input type="text" name="lastname" class="form-control" id="apellido" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá tu apellido</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4 item-form">
                                        <label for="birthday">Fecha de nacimiento</label>
                                        <input type="date" class="form-control" id="birthday" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá una fecha válida</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <legend>
                                                <label>Sexo</label>
                                                <p class="help-block">Como aparece en tu DNI</p>
                                            </legend>
                                            <div class="form-group item-form">
                                                <label for="f" class="radio-inline">
                                                    <input type="radio" name="sexo" id="f" value="F" required="" aria-required="true"> Femenino
                                                </label>
                                                <label for="m" class="radio-inline">
                                                    <input type="radio" name="sexo" id="m" value="M" required="" aria-required="true"> Masculino
                                                </label>
                                                <p class="help-block error hidden">Ingresá tu sexo</p>
                                            </div>

                                        </fieldset>
                                    </div>
                                </div>

                            </fieldset>

                            <!-- OTROS DATOS PERSONALES -->
                            <fieldset>
                                <legend>
                                    <h3>Otros datos personales</h3></legend>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group item-form">
                                            <label for="password">Contraseña</label>
                                            <p class="help-block" id="help-password">Usá al menos caracteres</p>
                                            <input id="password" name="password" type="password" class="form-control" maxlength="15" aria-describedby="help-password">
                                            <p class="help-block error hidden">Usá al menos 6 caracteres</p>
                                            <p class="help-block error hidden">Ingresá tu contraseña</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="password_confirmacion">Contraseña</label>
                                        <div class="form-group item-form">
                                            <input id="password_confirmacion" name="password_confirmacion" type="password" class="form-control" maxlength="15">
                                            <p class="help-block error hidden">La contraseña ingresada no es correcta</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-4">
                                        <div class="form-group item-form">
                                            <label for="tipoDoc">Tipo de Documento</label>
                                            <select id="tipoDoc" name="tipodoc" class="form-control">
                                                <option value="DNI" selected="">DNI</option>
                                                <option value="Libreta de Enrolamiento">Libreta de Enrolamiento</option>
                                                <option value="Libreta Cívica">Libreta Cívica</option>
                                                <option value="Pasaporte extranjero">Pasaporte extranjero</option>
                                            </select>
                                            <p class="help-block error hidden">Elegí un tipo de documento</p>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group item-form">
                                            <label for="numDoc">Número de documento</label>
                                            <input type="number" min="1" class="form-control" id="numDoc" required="" aria-required="true">
                                            <p class="help-block error hidden">Ingresá tu número de documento</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group item-form">
                                        <div class="col-xs-12">
                                            <label for="nro-tramite">Número de trámite del DNI</label>
                                            <p>
                                                <a onclick="" role="button" data-toggle="popover" data-trigger="focus">¿Dónde encuentro mi número de trámite?</a>
                                            </p>
                                            <div class="content-popover hidden bg-white" id="content-popover">
                                                <div class="arrow-popover"></div>
                                                <div class="col-xs-12 p-x-0">
                                                    <span class="close" onclick="pophidde()">×</span>
                                                </div>
                                                <div>
                                                    <p>El número de trámite de tu DNI se encuentra en el frente en la parte inferior.</p>
                                                </div>
                                                <div class="m-x-1">

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group  item-form">
                                                        <input type="number" class="form-control" id="nro-tramite" required="" min="0" aria-required="true">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="estadoCivil">Estado civil</label>
                                        <select id="estadoCivil" name="estadoCivil" class="form-control">
                                            <option value="" selected="">Seleccioná un estado civil</option>
                                            <option value="">Soltero/a</option>
                                            <option value="">Casado/a</option>
                                            <option value="">Divorciado/a</option>
                                            <option value="">Viudo/a</option>
                                            <option value="">Vínculo de hecho</option>
                                            <option value="">Separado/a</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá tu estado civil</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group item-form">
                                            <label for="country_birthday">País de nacimiento</label>
                                            <select id="country_birthday" class="form-control">
                                                <option value="">Seleccioná un pais</option>
                                                <option value="ARG" selected="">Argentina</option>
                                                <option value="BRA">Brasil</option>
                                                <option value="URY">Uruguay</option>
                                                <option value="PRY">Paraguay</option>
                                                <option value="CHL">Chile</option>
                                                <option value="BOL">Bolivia</option>
                                                <option value="AFG">Afganistán</option>
                                                <option value="ALB">Albania</option>
                                                <option value="DEU">Alemania</option>
                                                <option value="AND">Andorra</option>
                                                <option value="AGO">Angola</option>
                                                <option value="AIA">Anguila</option>
                                                <option value="ATA">Antártida</option>
                                                <option value="ATG">Antigua y Barbuda</option>
                                                <option value="SAU">Arabia Saudita</option>
                                                <option value="DZA">Argelia</option>
                                                <option value="ARM">Armenia</option>
                                                <option value="ABW">Aruba</option>
                                                <option value="AUS">Australia</option>
                                                <option value="AUT">Austria</option>
                                                <option value="AZE">Azerbaiyán</option>
                                                <option value="BHS">Bahamas</option>
                                                <option value="BGD">Bangladés</option>
                                                <option value="BRB">Barbados</option>
                                                <option value="BHR">Baréin</option>
                                                <option value="BEL">Bélgica</option>
                                                <option value="BLZ">Belice</option>
                                                <option value="BEN">Benín</option>
                                                <option value="BMU">Bermudas</option>
                                                <option value="BLR">Bielorrusia</option>
                                                <option value="BES">Bonaire</option>
                                                <option value="BIH">Bosnia y Herzegovina</option>
                                                <option value="BWA">Botsuana</option>
                                                <option value="BRN">Brunéi Darussalam</option>
                                                <option value="BGR">Bulgaria</option>
                                                <option value="BFA">Burkina Faso</option>
                                                <option value="BDI">Burundi</option>
                                                <option value="BTN">Bután</option>
                                                <option value="CPV">Cabo Verde</option>
                                                <option value="KHM">Camboya</option>
                                                <option value="CMR">Camerún</option>
                                                <option value="CAN">Canadá</option>
                                                <option value="QAT">Catar</option>
                                                <option value="TCD">Chad</option>
                                                <option value="CHL">Chile</option>
                                                <option value="CHN">China</option>
                                                <option value="CYP">Chipre</option>
                                                <option value="COL">Colombia</option>
                                                <option value="COM">Comoras</option>
                                                <option value="COG">Congo</option>
                                                <option value="KOR">Corea</option>
                                                <option value="CIV">Costa de Marfil</option>
                                                <option value="CRI">Costa Rica</option>
                                                <option value="HRV">Croacia</option>
                                                <option value="CUB">Cuba</option>
                                                <option value="CUW">Curaçao</option>
                                                <option value="DNK">Dinamarca</option>
                                                <option value="DMA">Dominica</option>
                                                <option value="ECU">Ecuador</option>
                                                <option value="EGY">Egipto</option>
                                                <option value="SLV">El Salvador</option>
                                                <option value="ARE">Emiratos Árabes Unidos</option>
                                                <option value="ERI">Eritrea</option>
                                                <option value="SVK">Eslovaquia</option>
                                                <option value="SVN">Eslovenia</option>
                                                <option value="ESP">España</option>
                                                <option value="USA">Estados Unidos</option>
                                                <option value="EST">Estonia</option>
                                                <option value="ETH">Etiopía</option>
                                                <option value="PHL">Filipinas</option>
                                                <option value="FIN">Finlandia</option>
                                                <option value="FJI">Fiyi</option>
                                                <option value="FRA">Francia</option>
                                                <option value="GAB">Gabón</option>
                                                <option value="GMB">Gambia</option>
                                                <option value="GEO">Georgia</option>
                                                <option value="SGS">Georgia del sur y las islas sandwich del sur</option>
                                                <option value="GHA">Ghana</option>
                                                <option value="GIB">Gibraltar</option>
                                                <option value="GRD">Granada</option>
                                                <option value="GRC">Grecia</option>
                                                <option value="GRL">Groenlandia</option>
                                                <option value="GLP">Guadalupe</option>
                                                <option value="GUM">Guam</option>
                                                <option value="GTM">Guatemala</option>
                                                <option value="GUF">Guayana Francesa</option>
                                                <option value="GGY">Guernsey</option>
                                                <option value="GIN">Guinea</option>
                                                <option value="GNB">Guinea-Bisáu</option>
                                                <option value="GNQ">Guinea Ecuatorial</option>
                                                <option value="GUY">Guyana</option>
                                                <option value="HTI">Haití</option>
                                                <option value="HND">Honduras</option>
                                                <option value="HKG">Hong Kong</option>
                                                <option value="HUN">Hungría</option>
                                                <option value="IND">India</option>
                                                <option value="IDN">Indonesia</option>
                                                <option value="IRQ">Irak</option>
                                                <option value="IRN">Irán</option>
                                                <option value="IRL">Irlanda</option>
                                                <option value="BVT">Isla Bouvet</option>
                                                <option value="IMN">Isla de Man</option>
                                                <option value="CXR">Isla de Navidad</option>
                                                <option value="HMD">Isla Heard e Islas McDonald</option>
                                                <option value="ISL">Islandia</option>
                                                <option value="NFK">Isla Norfolk</option>
                                                <option value="CYM">Islas Caimán</option>
                                                <option value="CCK">Islas Cocos</option>
                                                <option value="COK">Islas Cook</option>
                                                <option value="UMI">Islas de Ultramar Menores de Estados Unidos</option>
                                                <option value="FRO">Islas Feroe</option>
                                                <option value="FLK">Islas Malvinas</option>
                                                <option value="MNP">Islas Marianas del Norte</option>
                                                <option value="MHL">Islas Marshall</option>
                                                <option value="SLB">Islas Salomón</option>
                                                <option value="TCA">Islas Turcas y Caicos</option>
                                                <option value="VIR">Islas Vírgenes</option>
                                                <option value="ISR">Israel</option>
                                                <option value="ITA">Italia</option>
                                                <option value="JAM">Jamaica</option>
                                                <option value="JPN">Japón</option>
                                                <option value="JEY">Jersey</option>
                                                <option value="JOR">Jordania</option>
                                                <option value="KAZ">Kazajistán</option>
                                                <option value="KEN">Kenia</option>
                                                <option value="KGZ">Kirguistán</option>
                                                <option value="KIR">Kiribati</option>
                                                <option value="KWT">Kuwait</option>
                                                <option value="LAO">Lao</option>
                                                <option value="LSO">Lesoto</option>
                                                <option value="LVA">Letonia</option>
                                                <option value="LBN">Líbano</option>
                                                <option value="LBR">Liberia</option>
                                                <option value="LBY">Libia</option>
                                                <option value="LIE">Liechtenstein</option>
                                                <option value="LTU">Lituania</option>
                                                <option value="LUX">Luxemburgo</option>
                                                <option value="MAC">Macao</option>
                                                <option value="MKD">Macedonia</option>
                                                <option value="MDG">Madagascar</option>
                                                <option value="MYS">Malasia</option>
                                                <option value="MWI">Malaui</option>
                                                <option value="MDV">Maldivas</option>
                                                <option value="MLI">Malí</option>
                                                <option value="MLT">Malta</option>
                                                <option value="MAR">Marruecos</option>
                                                <option value="MTQ">Martinica</option>
                                                <option value="MUS">Mauricio</option>
                                                <option value="MRT">Mauritania</option>
                                                <option value="MYT">Mayotte</option>
                                                <option value="MEX">México</option>
                                                <option value="FSM">Micronesia</option>
                                                <option value="MDA">Moldavia</option>
                                                <option value="MCO">Mónaco</option>
                                                <option value="MNG">Mongolia</option>
                                                <option value="MNE">Montenegro</option>
                                                <option value="MSR">Montserrat</option>
                                                <option value="MOZ">Mozambique</option>
                                                <option value="MMR">Myanmar</option>
                                                <option value="NAM">Namibia</option>
                                                <option value="NRU">Nauru</option>
                                                <option value="NPL">Nepal</option>
                                                <option value="NIC">Nicaragua</option>
                                                <option value="NER">Níger</option>
                                                <option value="NGA">Nigeria</option>
                                                <option value="NIU">Niue</option>
                                                <option value="NOR">Noruega</option>
                                                <option value="NCL">Nueva Caledonia</option>
                                                <option value="NZL">Nueva Zelanda</option>
                                                <option value="OMN">Omán</option>
                                                <option value="NLD">Países Bajos</option>
                                                <option value="PAK">Pakistán</option>
                                                <option value="PLW">Palaos</option>
                                                <option value="PSE">Palestina, Estado de</option>
                                                <option value="PAN">Panamá</option>
                                                <option value="PNG">Papúa Nueva Guinea</option>
                                                <option value="PER">Perú</option>
                                                <option value="PCN">Pitcairn</option>
                                                <option value="PYF">Polinesia Francesa</option>
                                                <option value="POL">Polonia</option>
                                                <option value="PRT">Portugal</option>
                                                <option value="PRI">Puerto Rico</option>
                                                <option value="GBR">Reino Unido</option>
                                                <option value="CAF">República Centroafricana</option>
                                                <option value="CZE">República Checa</option>
                                                <option value="DOM">República Dominicana</option>
                                                <option value="REU">Reunión</option>
                                                <option value="RWA">Ruanda</option>
                                                <option value="ROU">Rumania</option>
                                                <option value="RUS">Rusia</option>
                                                <option value="ESH">Sahara Occidental</option>
                                                <option value="WSM">Samoa</option>
                                                <option value="ASM">Samoa Americana</option>
                                                <option value="BLM">San Bartolomé</option>
                                                <option value="KNA">San Cristóbal y Nieves</option>
                                                <option value="SMR">San Marino</option>
                                                <option value="MAF">San Martín</option>
                                                <option value="SPM">San Pedro y Miquelón</option>
                                                <option value="SHN">Santa Helena</option>
                                                <option value="LCA">Santa Lucía</option>
                                                <option value="STP">Santo Tomé y Príncipe</option>
                                                <option value="VCT">San Vicente y las Granadinas</option>
                                                <option value="SEN">Senegal</option>
                                                <option value="SRB">Serbia</option>
                                                <option value="SYC">Seychelles</option>
                                                <option value="SLE">Sierra leona</option>
                                                <option value="SGP">Singapur</option>
                                                <option value="SXM">Sint Maarten</option>
                                                <option value="SYR">Siria</option>
                                                <option value="SOM">Somalia</option>
                                                <option value="LKA">Sri Lanka</option>
                                                <option value="SWZ">Suazilandia</option>
                                                <option value="ZAF">Sudáfrica</option>
                                                <option value="SDN">Sudán</option>
                                                <option value="SSD">Sudán del Sur</option>
                                                <option value="SWE">Suecia</option>
                                                <option value="CHE">Suiza</option>
                                                <option value="SUR">Surinam</option>
                                                <option value="SJM">Svalbard y Jan Mayen</option>
                                                <option value="THA">Tailandia</option>
                                                <option value="TWN">Taiwán</option>
                                                <option value="TZA">Tanzania</option>
                                                <option value="TJK">Tayikistán</option>
                                                <option value="IOT">Territorio Británico del Océano Índico</option>
                                                <option value="ATF">Territorios Australes Franceses</option>
                                                <option value="TLS">Timor-Leste</option>
                                                <option value="TGO">Togo</option>
                                                <option value="TKL">Tokelau</option>
                                                <option value="TON">Tonga</option>
                                                <option value="TTO">Trinidad y Tobago</option>
                                                <option value="TUN">Túnez</option>
                                                <option value="TKM">Turkmenistán</option>
                                                <option value="TUR">Turquía</option>
                                                <option value="TUV">Tuvalu</option>
                                                <option value="UKR">Ucrania</option>
                                                <option value="UGA">Uganda</option>
                                                <option value="UZB">Uzbekistán</option>
                                                <option value="VUT">Vanuatu</option>
                                                <option value="VEN">Venezuela</option>
                                                <option value="VNM">Vietnam</option>
                                                <option value="WLF">Wallis y Futuna</option>
                                                <option value="YEM">Yemen</option>
                                                <option value="DJI">Yibuti</option>
                                                <option value="ZMB">Zambia</option>
                                                <option value="ZWE">Zimbabue</option>
                                            </select>
                                            <p class="help-block error hidden">Ingresá tu país de nacimiento</p>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <!-- DATOS DE CONTACTO -->
                            <fieldset>
                                <legend>
                                    <h3>Datos de contacto</h3>
                                    <p>Ingresá al menos un dato de contacto</p>
                                </legend>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">

                                        <label for="email">Dirección de correo electrónico</label>
                                        <input type="email" name="email" class="form-control" id="email" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un correo electrónico
                                            <br> El correo electrónico tiene un formato no válido</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <legend>Teléfono móvil</legend>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p class="help-block" id="help_telefono_movil">Incluí el <a href="https://www.argentina.gob.ar/codigos-de-area-telefonicos-de-argentina" aria-label="Ingresá código de área. Si no lo sabés consultá en este enlace" target="_blank">código de área</a> de tu localidad</p>
                                                </div>
                                                <div class="form-group col-xs-12 col-sm-4 item-form">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">0</span>
                                                        <input aria-label="Código de area de teléfono móvil" class="form-control" id="cellphone_area_code" required="" aria-required="true" type="text">
                                                    </div>
                                                    <p class="help-block error hidden">Ingresá el código de área</p>
                                                </div>
                                                <div class="form-group col-xs-12 col-sm-8 item-form">
                                                    <div class="input-group">

                                                        <span class="input-group-addon">15</span>
                                                        <input aria-label="número de teléfono móvil" class="form-control" id="cellphone_number" required="" aria-required="true" type="text">
                                                    </div>
                                                    <p class="help-block error hidden">Ingresá tu número de teléfono móvil</p>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <fieldset>
                                            <legend>Teléfono fijo</legend>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p class="help-block" id="help_telefono_fijo">Incluí el <a href="https://www.argentina.gob.ar/codigos-de-area-telefonicos-de-argentina" aria-label="Ingresá código de área. Si no lo sabés consultá en este enlace" target="_blank">código de área</a> de tu localidad</p>
                                                </div>
                                                <div class="form-group col-xs-12 col-sm-4 item-form">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">0</span>
                                                        <input aria-label="Código de area de teléfono fijo" class="form-control" id="phone_area_code" required="" aria-required="true" type="text">
                                                    </div>
                                                    <p class="help-block error hidden">Ingresá el código de área</p>
                                                </div>

                                                <div class="form-group col-xs-12 col-sm-8 item-form">
                                                    <input aria-label="número de teléfono fijo" class="form-control" id="phone_number" required="" aria-required="true" type="text">
                                                    <p class="help-block error hidden">Ingresá tu número de teléfono fijo</p>
                                                </div>

                                            </div>
                                        </fieldset>
                                    </div>
                                </div>

                            </fieldset>

                            <!-- DOMICILIO -->
                            <fieldset>
                                <legend>
                                    <h3>Domicilio</h3></legend>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="country">País de residencia</label>
                                        <select id="country" class="form-control">
                                            <option value="">Seleccioná un pais</option>
                                            <option value="ARG" selected="">Argentina</option>
                                            <option value="BRA">Brasil</option>
                                            <option value="URY">Uruguay</option>
                                            <option value="PRY">Paraguay</option>
                                            <option value="CHL">Chile</option>
                                            <option value="BOL">Bolivia</option>
                                            <option value="AFG">Afganistán</option>
                                            <option value="ALB">Albania</option>
                                            <option value="DEU">Alemania</option>
                                            <option value="AND">Andorra</option>
                                            <option value="AGO">Angola</option>
                                            <option value="AIA">Anguila</option>
                                            <option value="ATA">Antártida</option>
                                            <option value="ATG">Antigua y Barbuda</option>
                                            <option value="SAU">Arabia Saudita</option>
                                            <option value="DZA">Argelia</option>
                                            <option value="ARM">Armenia</option>
                                            <option value="ABW">Aruba</option>
                                            <option value="AUS">Australia</option>
                                            <option value="AUT">Austria</option>
                                            <option value="AZE">Azerbaiyán</option>
                                            <option value="BHS">Bahamas</option>
                                            <option value="BGD">Bangladés</option>
                                            <option value="BRB">Barbados</option>
                                            <option value="BHR">Baréin</option>
                                            <option value="BEL">Bélgica</option>
                                            <option value="BLZ">Belice</option>
                                            <option value="BEN">Benín</option>
                                            <option value="BMU">Bermudas</option>
                                            <option value="BLR">Bielorrusia</option>
                                            <option value="BES">Bonaire</option>
                                            <option value="BIH">Bosnia y Herzegovina</option>
                                            <option value="BWA">Botsuana</option>
                                            <option value="BRN">Brunéi Darussalam</option>
                                            <option value="BGR">Bulgaria</option>
                                            <option value="BFA">Burkina Faso</option>
                                            <option value="BDI">Burundi</option>
                                            <option value="BTN">Bután</option>
                                            <option value="CPV">Cabo Verde</option>
                                            <option value="KHM">Camboya</option>
                                            <option value="CMR">Camerún</option>
                                            <option value="CAN">Canadá</option>
                                            <option value="QAT">Catar</option>
                                            <option value="TCD">Chad</option>
                                            <option value="CHL">Chile</option>
                                            <option value="CHN">China</option>
                                            <option value="CYP">Chipre</option>
                                            <option value="COL">Colombia</option>
                                            <option value="COM">Comoras</option>
                                            <option value="COG">Congo</option>
                                            <option value="KOR">Corea</option>
                                            <option value="CIV">Costa de Marfil</option>
                                            <option value="CRI">Costa Rica</option>
                                            <option value="HRV">Croacia</option>
                                            <option value="CUB">Cuba</option>
                                            <option value="CUW">Curaçao</option>
                                            <option value="DNK">Dinamarca</option>
                                            <option value="DMA">Dominica</option>
                                            <option value="ECU">Ecuador</option>
                                            <option value="EGY">Egipto</option>
                                            <option value="SLV">El Salvador</option>
                                            <option value="ARE">Emiratos Árabes Unidos</option>
                                            <option value="ERI">Eritrea</option>
                                            <option value="SVK">Eslovaquia</option>
                                            <option value="SVN">Eslovenia</option>
                                            <option value="ESP">España</option>
                                            <option value="USA">Estados Unidos</option>
                                            <option value="EST">Estonia</option>
                                            <option value="ETH">Etiopía</option>
                                            <option value="PHL">Filipinas</option>
                                            <option value="FIN">Finlandia</option>
                                            <option value="FJI">Fiyi</option>
                                            <option value="FRA">Francia</option>
                                            <option value="GAB">Gabón</option>
                                            <option value="GMB">Gambia</option>
                                            <option value="GEO">Georgia</option>
                                            <option value="SGS">Georgia del sur y las islas sandwich del sur</option>
                                            <option value="GHA">Ghana</option>
                                            <option value="GIB">Gibraltar</option>
                                            <option value="GRD">Granada</option>
                                            <option value="GRC">Grecia</option>
                                            <option value="GRL">Groenlandia</option>
                                            <option value="GLP">Guadalupe</option>
                                            <option value="GUM">Guam</option>
                                            <option value="GTM">Guatemala</option>
                                            <option value="GUF">Guayana Francesa</option>
                                            <option value="GGY">Guernsey</option>
                                            <option value="GIN">Guinea</option>
                                            <option value="GNB">Guinea-Bisáu</option>
                                            <option value="GNQ">Guinea Ecuatorial</option>
                                            <option value="GUY">Guyana</option>
                                            <option value="HTI">Haití</option>
                                            <option value="HND">Honduras</option>
                                            <option value="HKG">Hong Kong</option>
                                            <option value="HUN">Hungría</option>
                                            <option value="IND">India</option>
                                            <option value="IDN">Indonesia</option>
                                            <option value="IRQ">Irak</option>
                                            <option value="IRN">Irán</option>
                                            <option value="IRL">Irlanda</option>
                                            <option value="BVT">Isla Bouvet</option>
                                            <option value="IMN">Isla de Man</option>
                                            <option value="CXR">Isla de Navidad</option>
                                            <option value="HMD">Isla Heard e Islas McDonald</option>
                                            <option value="ISL">Islandia</option>
                                            <option value="NFK">Isla Norfolk</option>
                                            <option value="CYM">Islas Caimán</option>
                                            <option value="CCK">Islas Cocos</option>
                                            <option value="COK">Islas Cook</option>
                                            <option value="UMI">Islas de Ultramar Menores de Estados Unidos</option>
                                            <option value="FRO">Islas Feroe</option>
                                            <option value="FLK">Islas Malvinas</option>
                                            <option value="MNP">Islas Marianas del Norte</option>
                                            <option value="MHL">Islas Marshall</option>
                                            <option value="SLB">Islas Salomón</option>
                                            <option value="TCA">Islas Turcas y Caicos</option>
                                            <option value="VIR">Islas Vírgenes</option>
                                            <option value="ISR">Israel</option>
                                            <option value="ITA">Italia</option>
                                            <option value="JAM">Jamaica</option>
                                            <option value="JPN">Japón</option>
                                            <option value="JEY">Jersey</option>
                                            <option value="JOR">Jordania</option>
                                            <option value="KAZ">Kazajistán</option>
                                            <option value="KEN">Kenia</option>
                                            <option value="KGZ">Kirguistán</option>
                                            <option value="KIR">Kiribati</option>
                                            <option value="KWT">Kuwait</option>
                                            <option value="LAO">Lao</option>
                                            <option value="LSO">Lesoto</option>
                                            <option value="LVA">Letonia</option>
                                            <option value="LBN">Líbano</option>
                                            <option value="LBR">Liberia</option>
                                            <option value="LBY">Libia</option>
                                            <option value="LIE">Liechtenstein</option>
                                            <option value="LTU">Lituania</option>
                                            <option value="LUX">Luxemburgo</option>
                                            <option value="MAC">Macao</option>
                                            <option value="MKD">Macedonia</option>
                                            <option value="MDG">Madagascar</option>
                                            <option value="MYS">Malasia</option>
                                            <option value="MWI">Malaui</option>
                                            <option value="MDV">Maldivas</option>
                                            <option value="MLI">Malí</option>
                                            <option value="MLT">Malta</option>
                                            <option value="MAR">Marruecos</option>
                                            <option value="MTQ">Martinica</option>
                                            <option value="MUS">Mauricio</option>
                                            <option value="MRT">Mauritania</option>
                                            <option value="MYT">Mayotte</option>
                                            <option value="MEX">México</option>
                                            <option value="FSM">Micronesia</option>
                                            <option value="MDA">Moldavia</option>
                                            <option value="MCO">Mónaco</option>
                                            <option value="MNG">Mongolia</option>
                                            <option value="MNE">Montenegro</option>
                                            <option value="MSR">Montserrat</option>
                                            <option value="MOZ">Mozambique</option>
                                            <option value="MMR">Myanmar</option>
                                            <option value="NAM">Namibia</option>
                                            <option value="NRU">Nauru</option>
                                            <option value="NPL">Nepal</option>
                                            <option value="NIC">Nicaragua</option>
                                            <option value="NER">Níger</option>
                                            <option value="NGA">Nigeria</option>
                                            <option value="NIU">Niue</option>
                                            <option value="NOR">Noruega</option>
                                            <option value="NCL">Nueva Caledonia</option>
                                            <option value="NZL">Nueva Zelanda</option>
                                            <option value="OMN">Omán</option>
                                            <option value="NLD">Países Bajos</option>
                                            <option value="PAK">Pakistán</option>
                                            <option value="PLW">Palaos</option>
                                            <option value="PSE">Palestina, Estado de</option>
                                            <option value="PAN">Panamá</option>
                                            <option value="PNG">Papúa Nueva Guinea</option>
                                            <option value="PER">Perú</option>
                                            <option value="PCN">Pitcairn</option>
                                            <option value="PYF">Polinesia Francesa</option>
                                            <option value="POL">Polonia</option>
                                            <option value="PRT">Portugal</option>
                                            <option value="PRI">Puerto Rico</option>
                                            <option value="GBR">Reino Unido</option>
                                            <option value="CAF">República Centroafricana</option>
                                            <option value="CZE">República Checa</option>
                                            <option value="DOM">República Dominicana</option>
                                            <option value="REU">Reunión</option>
                                            <option value="RWA">Ruanda</option>
                                            <option value="ROU">Rumania</option>
                                            <option value="RUS">Rusia</option>
                                            <option value="ESH">Sahara Occidental</option>
                                            <option value="WSM">Samoa</option>
                                            <option value="ASM">Samoa Americana</option>
                                            <option value="BLM">San Bartolomé</option>
                                            <option value="KNA">San Cristóbal y Nieves</option>
                                            <option value="SMR">San Marino</option>
                                            <option value="MAF">San Martín</option>
                                            <option value="SPM">San Pedro y Miquelón</option>
                                            <option value="SHN">Santa Helena</option>
                                            <option value="LCA">Santa Lucía</option>
                                            <option value="STP">Santo Tomé y Príncipe</option>
                                            <option value="VCT">San Vicente y las Granadinas</option>
                                            <option value="SEN">Senegal</option>
                                            <option value="SRB">Serbia</option>
                                            <option value="SYC">Seychelles</option>
                                            <option value="SLE">Sierra leona</option>
                                            <option value="SGP">Singapur</option>
                                            <option value="SXM">Sint Maarten</option>
                                            <option value="SYR">Siria</option>
                                            <option value="SOM">Somalia</option>
                                            <option value="LKA">Sri Lanka</option>
                                            <option value="SWZ">Suazilandia</option>
                                            <option value="ZAF">Sudáfrica</option>
                                            <option value="SDN">Sudán</option>
                                            <option value="SSD">Sudán del Sur</option>
                                            <option value="SWE">Suecia</option>
                                            <option value="CHE">Suiza</option>
                                            <option value="SUR">Surinam</option>
                                            <option value="SJM">Svalbard y Jan Mayen</option>
                                            <option value="THA">Tailandia</option>
                                            <option value="TWN">Taiwán</option>
                                            <option value="TZA">Tanzania</option>
                                            <option value="TJK">Tayikistán</option>
                                            <option value="IOT">Territorio Británico del Océano Índico</option>
                                            <option value="ATF">Territorios Australes Franceses</option>
                                            <option value="TLS">Timor-Leste</option>
                                            <option value="TGO">Togo</option>
                                            <option value="TKL">Tokelau</option>
                                            <option value="TON">Tonga</option>
                                            <option value="TTO">Trinidad y Tobago</option>
                                            <option value="TUN">Túnez</option>
                                            <option value="TKM">Turkmenistán</option>
                                            <option value="TUR">Turquía</option>
                                            <option value="TUV">Tuvalu</option>
                                            <option value="UKR">Ucrania</option>
                                            <option value="UGA">Uganda</option>
                                            <option value="UZB">Uzbekistán</option>
                                            <option value="VUT">Vanuatu</option>
                                            <option value="VEN">Venezuela</option>
                                            <option value="VNM">Vietnam</option>
                                            <option value="WLF">Wallis y Futuna</option>
                                            <option value="YEM">Yemen</option>
                                            <option value="DJI">Yibuti</option>
                                            <option value="ZMB">Zambia</option>
                                            <option value="ZWE">Zimbabue</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá un país</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="address_state">Provincia</label>
                                        <select class="form-control" id="address_state" name="address_state">
                                            <option value="" selected="">Seleccioná una provincia</option>
                                            <option value="Ciudad Autónoma de buenos Aires">Ciudad Autónoma de Buenos Aires</option>
                                            <option value="Buenos Aires">Buenos Aires</option>
                                            <option value="Catamarca">Catamarca</option>
                                            <option value="Chaco">Chaco</option>
                                            <option value="Chubut">Chubut</option>
                                            <option value="Córdoba">Córdoba</option>
                                            <option value="Corrientes">Corrientes</option>
                                            <option value="Entre Ríos">Entre Ríos</option>
                                            <option value="Formosa">Formosa</option>
                                            <option value="Jujuy">Jujuy</option>
                                            <option value="La Pampa">La Pampa</option>
                                            <option value="La Rioja">La Rioja</option>
                                            <option value="Mendoza">Mendoza</option>
                                            <option value="Misiones">Misiones</option>
                                            <option value="Neuquén">Neuquén</option>
                                            <option value="Río Negro">Río Negro</option>
                                            <option value="Salta">Salta</option>
                                            <option value="San Juan">San Juan</option>
                                            <option value="Santa Cruz">Santa Cruz</option>
                                            <option value="Santa Fe">Santa Fe</option>
                                            <option value="Santiago del Estero">Santiago del Estero</option>
                                            <option value="Tierra del Fuego">Tierra del Fuego</option>
                                            <option value="Tucumán">Tucumán</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá una provincia</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="department">Partido o departamento</label>
                                        <select class="form-control" name="department" id="department">
                                            <option value="" selected="">Seleccioná un partido o departamento</option>
                                            <option value="">Departamento 1</option>
                                            <option value="">Departamento 2</option>
                                            <option value="">Departamento 3</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá un partido o departamento</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="localidad">Localidad</label>
                                        <select class="form-control" name="localidad" id="localidad">
                                            <option value="" selected="">Seleccioná una localidad</option>
                                            <option value="">Localidad 1</option>
                                            <option value="">Localidad 2</option>
                                            <option value="">Localidad 3</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá una localidad</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 form-group item-form">
                                        <label for="address_postalcode">Código postal</label>
                                        <input type="text" name="address_postalcode" class="form-control" id="address_postalcode" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un código postal</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="address_street">Calle</label>
                                        <input type="text" name="address_street" class="form-control" id="address_street" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá una calle</p>
                                    </div>

                                    <div class="col-md-4 form-group item-form">
                                        <label for="address_street_number">Número de calle</label>
                                        <input type="text" class="form-control" id="address_street_number" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un número de calle</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 form-group item-form">
                                        <label for="address_floor_number">Piso</label>
                                        <input type="text" name="address_floor_number" class="form-control" id="address_floor_number" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un piso</p>
                                    </div>

                                    <div class="col-md-4 form-group item-form">
                                        <label for="address_apartment_number">Departamento</label>
                                        <input type="text" class="form-control" id="address_apartment_number" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un un departamento</p>
                                    </div>
                                </div>

                            </fieldset>

                            <!-- OTROS DATOS ESPECÍFICOS -->
                            <fieldset>
                                <legend>
                                    <h3>Otros datos específicos</h3></legend>

                                <div class="row">
                                    <div class="col-md-4 form-group item-form">
                                        <label for="cuit">CUIT</label>
                                        <input type="number" min="1" class="form-control" id="cuit" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá un CUIT
                                            <br> El CUIT tiene un formato inválido</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group item-form">
                                            <label for="vehicule_plaque">Patente de vehículo</label>
                                            <p class="help-block" id="help_vehicule_plaque">Ejemplo: AAA123 o AA123BB</p>
                                            <input type="text" class="form-control" id="vehicule_plaque" required="" aria-required="true" aria-describedby="help_vehicule_plaque">
                                            <p class="help-block error hidden">Ingresá una patente</p>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend>
                                    <h3 class="m-b-1">Campos genéricos</h3>
                                </legend>

                                <div class="row">
                                    <div class="col-md-12 form-group item-form">
                                        <label for="buscar">Buscar</label>

                                        <div class="row">
                                            <div class="col-md-8">
                                                <input type="search" class="form-control" id="buscar" required="" aria-required="true">
                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn btn-primary btn-block"> Buscar </button>
                                            </div>
                                        </div>

                                        <p class="help-block error hidden">Ingresá una palabra para buscar</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <label for="filtro">Filtrar</label>
                                        <input type="text" name="filtro" class="form-control" id="filtro" required="" aria-required="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="comentarioBreve">Dato o comentario breve</label>
                                        <input class="form-control" id="comentarioBreve" type="text">
                                        <p class="help-block error hidden">Ingresá el dato o comentario breve</p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="comentarioLargos">Comentario largos</label>
                                        <textarea class="form-control" id="comentarioLargos" rows="5"></textarea>
                                        <p class="help-block error hidden">Ingresá el comentario largo</p>
                                    </div>
                                </div>

                                <fieldset>
                                    <legend>
                                        <label>Dato con 2 opciones</label>
                                    </legend>

                                    <div class="row">
                                        <div class="col-md-8 item-form">

                                            <div class="form-group item-form">
                                                <label for="opcion1" class="radio-inline">
                                                    <input type="radio" name="opciones" id="opcion1" value="opcion1" required="" aria-required="true"> Opción 1
                                                </label>
                                                <label for="opcion2" class="radio-inline">
                                                    <input type="radio" name="opciones" id="opcion2" value="opcion2" required="" aria-required="true"> Opción 1
                                                </label>
                                                <p class="help-block error hidden">Ingresá el dato</p>
                                            </div>

                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <div class="row">

                                        <div class="col-md-8 item-form">
                                            <label for="opcion1-alt">Dato con 3 a 7 opciones</label>
                                            <div class="form-group item-form">
                                                <div class="radio">
                                                    <label for="opcion1-alt">
                                                        <input type="radio" name="opciones-alt" id="opcion1-alt" value="opcion1-alt" required="" aria-required="true"> Opción 1
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="opcion2-alt">
                                                        <input type="radio" name="opciones-alt" id="opcion2-alt" value="opcion2-alt" required="" aria-required="true"> Opción 2
                                                    </label>
                                                </div>
                                                <div class="radio">
                                                    <label for="opcion3-alt">
                                                        <input type="radio" name="opciones-alt" id="opcion3-alt" value="opcion3-alt" required="" aria-required="true"> Opción 3
                                                    </label>
                                                </div>
                                                <p class="help-block error hidden">Ingresá el dato</p>
                                            </div>

                                        </div>
                                    </div>
                                </fieldset>

                                <div class="row">
                                    <div class="col-md-8 form-group item-form">
                                        <label for="dato_8_20">Dato con más de 8 opciones</label>
                                        <select id="dato_8_20" name="dato_8_20" class="form-control">
                                            <option value="" selected="">Seleccioná un dato</option>
                                            <option value="opciones1">Opción 1</option>
                                            <option value="opciones2">Opción 2</option>
                                            <option value="opciones3">Opción 3</option>
                                            <option value="opciones4">Opción 4</option>
                                            <option value="opciones5">Opción 5</option>
                                            <option value="opciones6">Opción 6</option>
                                            <option value="opciones7">Opción 7</option>
                                            <option value="opciones8">Opción 8</option>
                                            <option value="opciones9">Opción 9</option>
                                            <option value="opciones10">Opción 10</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá el dato</p>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>
                                                <label>Dato con múltiples opciones</label>
                                            </legend>
                                            <div class="form-group item-form">
                                                <div class="checkbox">
                                                    <label for="Option_1">
                                                        <input id="Option_1" name="Option_1" type="checkbox" value="">Option 1
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="Option_2">
                                                        <input id="Option_2" name="Option_2" type="checkbox" value="">Option 2
                                                    </label>
                                                </div>
                                                <div class="checkbox">
                                                    <label for="Option_3">
                                                        <input id="Option_3" name="Option_3" type="checkbox" value="">Option 3
                                                    </label>
                                                </div>
                                                <p class="help-block error hidden">Ingresá el dato</p>
                                            </div>
                                        </fieldset>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4 item-form">
                                        <label for="fecha">Fecha</label>
                                        <input type="date" class="form-control" id="fecha" required="" aria-required="true">
                                        <p class="help-block error hidden">Ingresá una fecha válida</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4 item-form">
                                        <label for="month">Mes</label>
                                        <select name="month" id="month" class="form-control">
                                            <option value="">Seleccioná un mes</option>
                                            <option value="1">Enero</option>
                                            <option value="2">Febrero</option>
                                            <option value="3">Marzo</option>
                                            <option value="4">Abril</option>
                                            <option value="5">Mayo</option>
                                            <option value="6">Junio</option>
                                            <option value="7">Julio</option>
                                            <option value="8">Agosto</option>
                                            <option value="9">Septiembre</option>
                                            <option value="10">Octubre</option>
                                            <option value="11">Noviembre</option>
                                            <option value="12">Diciembre</option>
                                        </select>
                                        <p class="help-block error hidden">Ingresá el mes</p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <a href="" class="btn btn-link">Anterior</a>
                                        <a href="" class="btn btn-success m-l-1">Siguiente</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </section>
            <section>
                <article class="row" id="tablas">
                    <div class="col-sm-12">
                        <h1>Tablas</h1>

                        <p>Las tablas de Poncho usan html, clases de Bootstrap y tienen funcionalidades opcionales para optimizar su vista en dispositivos móviles.</p>

                        <hr>

                        <h2>Tipo de tablas</h2>

                        <fieldset>

                            <!-- TABLA SIMPLE -->
                            <h3>Simple</h3>
                            <p>Para mostrar un dato por columna. Se usa para cuando hay pocas columnas.</p>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Título 1</th>
                                        <th>Título 2</th>
                                        <th>Título 3</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem ipsum dolor sit amet</td>
                                    </tr>
                                    <tr>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem ipsum dolor sit amet</td>
                                    </tr>
                                </tbody>
                            </table>

                            <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>table</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>thead</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>thead</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																				    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																				  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																				<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>table</span><span class="token punctuation">&gt;</span></span></code></pre>

                            <!-- TABLA COMBINADA -->
                            <h3>Con columnas combinadas</h3>
                            <p>Para mostrar varios datos por columna. Se usa para cuando hay muchas columnas.</p>

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Título 1</th>
                                        <th>Título 2</th>
                                        <th>Título 3</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem ipsum dolor sit amet</td>
                                    </tr>
                                    <tr>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem iipsum dolor</td>
                                        <td>Lorem ipsum dolor sit amet</td>
                                    </tr>
                                </tbody>
                            </table>

                            <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>table</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																						  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>thead</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																						  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>thead</span><span class="token punctuation">&gt;</span></span>
																																						  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																						    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																						  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																						<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>table</span><span class="token punctuation">&gt;</span></span></code></pre>

                            <!-- TABLA completa -->

                            <h2>Comportamiento de tablas</h2>

                            <!-- TABLA RESPONSIVE SIMPLE -->
                            <h3>Responsive</h3>
                            <p>Para mostrar uno o varios datos por columna. Se usa para cuando hay muchas columnas y se necesita priorizar la vista en dispositivos móviles.</p>

                            <table class="table table-responsive-poncho">
                                <thead>
                                    <tr>
                                        <th>Título 1</th>
                                        <th>Título 2</th>
                                        <th>Título 3</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td data-label="Título 1">Lorem iipsum dolor</td>
                                        <td data-label="Título 2">Lorem iipsum dolor</td>
                                        <td data-label="Título 3">Lorem ipsum dolor sit amet</td>
                                    </tr>
                                    <tr>
                                        <td data-label="Título 1">Lorem iipsum dolor</td>
                                        <td data-label="Título 2">Lorem iipsum dolor</td>
                                        <td data-label="Título 3">Lorem ipsum dolor sit amet</td>
                                    </tr>
                                </tbody>
                            </table>

                            <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>table</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table table-responsive-poncho<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>thead</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>thead</span><span class="token punctuation">&gt;</span></span>
																																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 2<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 3<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 2<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem iipsum dolor<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span> <span class="token attr-name">data-label</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>Título 3<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																								    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																								  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																								<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>table</span><span class="token punctuation">&gt;</span></span></code></pre>

                            <!-- TABLA CON SCROLL HORIZONTAL -->
                            <h3>Con scroll horizontal</h3>
                            <p>Sólo ser usado para casos con muchas columnas que no puedan combinarse (grilla de horarios o tarifas). Este comportamiento debe evitarse ya que no es cómodo para dispositivos móviles.</p>

                            <div class="table-scroll">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Título 1</th>
                                            <th>Título 2</th>
                                            <th>Título 3</th>
                                            <th>Título 4</th>
                                            <th>Título 5</th>
                                            <th>Título 6</th>
                                            <th>Título 7</th>
                                            <th>Título 8</th>
                                            <th>Título 9</th>
                                            <th>Título 10</th>
                                            <th>Título 11</th>
                                            <th>Título 12</th>
                                            <th>Título 13</th>
                                            <th>Título 14</th>
                                            <th>Título 15</th>
                                            <th>Título 16</th>
                                            <th>Título 17</th>
                                            <th>Título 18</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Lorem ipsum dolor sit amet</td>
                                            <td>Voluptas expedita hic consequuntur doloremque totam</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>40000</td>
                                            <td>50000</td>
                                            <td>60000</td>
                                            <td>70000</td>
                                        </tr>
                                        <tr>
                                            <td>Lorem ipsum dolor sit amet</td>
                                            <td>Voluptas expedita hic consequuntur doloremque totam</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>30000</td>
                                            <td>40000</td>
                                            <td>50000</td>
                                            <td>60000</td>
                                            <td>70000</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <pre class=" language-html"><code class=" language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table-scroll<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>table</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation">=</span><span class="token punctuation">"</span>table<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
																																										    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>thead</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 1<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 2<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 3<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 4<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 5<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 6<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 7<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 8<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 9<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 10<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 11<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 12<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 13<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 14<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 15<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 16<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 17<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>th</span><span class="token punctuation">&gt;</span></span>Título 18<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>th</span><span class="token punctuation">&gt;</span></span>
																																										    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>thead</span><span class="token punctuation">&gt;</span></span>
																																										    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Voluptas expedita hic consequuntur doloremque totam<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>40000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>50000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>60000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>70000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>tr</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Lorem ipsum dolor sit amet<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>Voluptas expedita hic consequuntur doloremque totam<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>30000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>40000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>50000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>60000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>td</span><span class="token punctuation">&gt;</span></span>70000<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>td</span><span class="token punctuation">&gt;</span></span>
																																										      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tr</span><span class="token punctuation">&gt;</span></span>
																																										    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>tbody</span><span class="token punctuation">&gt;</span></span>
																																										  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>table</span><span class="token punctuation">&gt;</span></span>
																																										<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span></code></pre>

                            <!-- Elementos de tablas -->
                            <h3>Elementos de tablas</h3>
                            <h4>Paginado</h4>
                            <p>Cuando las tablas tienen muchas filas deben tener paginado:</p>
                            <ul>
                                <li>Cuando las tablas simples superan las 30 filas</li>
                                <li>Cuando las tablas con columnas combinadas superan las 10 filas</li>
                            </ul>

                            <h4>Datos de tablas complejas</h4>
                            <p>Al usar celdas combinadas o cuando hay necesidad de enlaces o botones debe tenerse en cuenta:</p>
                            <ul>

                                <li>El primer item de la primer columna debe tener negrita</li>
                                <li>Ese mismo item puede tener un enlace que envíe a una página de detalle por ejemplo</li>
                                <li>Se pueden agregar botones (ej: ver mapa) y deben ir en la última columna</li>
                            </ul>

                    </div>
                </article>
            </section>
            <section>
                <h1>Iconos</h1>
                <p>El prefijo para la utilizacion de los iconos es <b>icono-arg-</b>nombre. </p>
                <div class="list row row-flex padding-40" id="iconos">
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-abeja" class="icono-arg-abeja icono-4x text-gray" data-clipboard-text="icono-arg-abeja"></i>
                        <br>
                        <p class="name m-b-0">abeja</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-abrigo" class="icono-arg-abrigo icono-4x text-gray" data-clipboard-text="icono-arg-abrigo"></i>
                        <br>
                        <p class="name m-b-0">abrigo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-acrobat" class="icono-arg-acrobat icono-4x text-gray" data-clipboard-text="icono-arg-acrobat"></i>
                        <br>
                        <p class="name m-b-0">acrobat</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-aeropuerto" class="icono-arg-aeropuerto icono-4x text-gray" data-clipboard-text="icono-arg-aeropuerto"></i>
                        <br>
                        <p class="name m-b-0">aeropuerto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-agricultura" class="icono-arg-agricultura icono-4x text-gray" data-clipboard-text="icono-arg-agricultura"></i>
                        <br>
                        <p class="name m-b-0">agricultura</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-agua" class="icono-arg-agua icono-4x text-gray" data-clipboard-text="icono-arg-agua"></i>
                        <br>
                        <p class="name m-b-0">agua</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-agua-contaminada" class="icono-arg-agua-contaminada icono-4x text-gray" data-clipboard-text="icono-arg-agua-contaminada"></i>
                        <br>
                        <p class="name m-b-0">agua-contaminada</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ahorro" class="icono-arg-ahorro icono-4x text-gray" data-clipboard-text="icono-arg-ahorro"></i>
                        <br>
                        <p class="name m-b-0">ahorro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-aire" class="icono-arg-aire icono-4x text-gray" data-clipboard-text="icono-arg-aire"></i>
                        <br>
                        <p class="name m-b-0">aire</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-aire-acondicionado" class="icono-arg-aire-acondicionado icono-4x text-gray" data-clipboard-text="icono-arg-aire-acondicionado"></i>
                        <br>
                        <p class="name m-b-0">aire-acondicionado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-alarma" class="icono-arg-alarma icono-4x text-gray" data-clipboard-text="icono-arg-alarma"></i>
                        <br>
                        <p class="name m-b-0">alarma</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-alarma-negativo" class="icono-arg-alarma-negativo icono-4x text-gray" data-clipboard-text="icono-arg-alarma-negativo"></i>
                        <br>
                        <p class="name m-b-0">alarma-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-alimentacion-saludable" class="icono-arg-alimentacion-saludable icono-4x text-gray" data-clipboard-text="icono-arg-alimentacion-saludable"></i>
                        <br>
                        <p class="name m-b-0">alimentacion-saludable</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-alimentos-y-establecimientos-ok" class="icono-arg-alimentos-y-establecimientos-ok icono-4x text-gray" data-clipboard-text="icono-arg-alimentos-y-establecimientos-ok"></i>
                        <br>
                        <p class="name m-b-0">alimentos-y-establecimientos-ok</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ambiente" class="icono-arg-ambiente icono-4x text-gray" data-clipboard-text="icono-arg-ambiente"></i>
                        <br>
                        <p class="name m-b-0">ambiente</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ambiente-cert" class="icono-arg-ambiente-cert icono-4x text-gray" data-clipboard-text="icono-arg-ambiente-cert"></i>
                        <br>
                        <p class="name m-b-0">ambiente-cert</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-animales-exp-imp" class="icono-arg-animales-exp-imp icono-4x text-gray" data-clipboard-text="icono-arg-animales-exp-imp"></i>
                        <br>
                        <p class="name m-b-0">animales-exp-imp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-anses" class="icono-arg-anses icono-4x text-gray" data-clipboard-text="icono-arg-anses"></i>
                        <br>
                        <p class="name m-b-0">anses</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-anses-negativo" class="icono-arg-anses-negativo icono-4x text-gray" data-clipboard-text="icono-arg-anses-negativo"></i>
                        <br>
                        <p class="name m-b-0">anses-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-antibalas" class="icono-arg-antibalas icono-4x text-gray" data-clipboard-text="icono-arg-antibalas"></i>
                        <br>
                        <p class="name m-b-0">antibalas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-anticorrupcion" class="icono-arg-anticorrupcion icono-4x text-gray" data-clipboard-text="icono-arg-anticorrupcion"></i>
                        <br>
                        <p class="name m-b-0">anticorrupcion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-apagado" class="icono-arg-apagado icono-4x text-gray" data-clipboard-text="icono-arg-apagado"></i>
                        <br>
                        <p class="name m-b-0">apagado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-apostilla" class="icono-arg-apostilla icono-4x text-gray" data-clipboard-text="icono-arg-apostilla"></i>
                        <br>
                        <p class="name m-b-0">apostilla</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-araucaria" class="icono-arg-araucaria icono-4x text-gray" data-clipboard-text="icono-arg-araucaria"></i>
                        <br>
                        <p class="name m-b-0">araucaria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arbitraje" class="icono-arg-arbitraje icono-4x text-gray" data-clipboard-text="icono-arg-arbitraje"></i>
                        <br>
                        <p class="name m-b-0">arbitraje</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arbol" class="icono-arg-arbol icono-4x text-gray" data-clipboard-text="icono-arg-arbol"></i>
                        <br>
                        <p class="name m-b-0">arbol</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-archivo-consulta" class="icono-arg-archivo-consulta icono-4x text-gray" data-clipboard-text="icono-arg-archivo-consulta"></i>
                        <br>
                        <p class="name m-b-0">archivo-consulta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-areas-vacantes" class="icono-arg-areas-vacantes icono-4x text-gray" data-clipboard-text="icono-arg-areas-vacantes"></i>
                        <br>
                        <p class="name m-b-0">areas-vacantes</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arma-antigua" class="icono-arg-arma-antigua icono-4x text-gray" data-clipboard-text="icono-arg-arma-antigua"></i>
                        <br>
                        <p class="name m-b-0">arma-antigua</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arma-imp" class="icono-arg-arma-imp icono-4x text-gray" data-clipboard-text="icono-arg-arma-imp"></i>
                        <br>
                        <p class="name m-b-0">arma-imp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arma-portacion" class="icono-arg-arma-portacion icono-4x text-gray" data-clipboard-text="icono-arg-arma-portacion"></i>
                        <br>
                        <p class="name m-b-0">arma-portacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-arma-tenencia" class="icono-arg-arma-tenencia icono-4x text-gray" data-clipboard-text="icono-arg-arma-tenencia"></i>
                        <br>
                        <p class="name m-b-0">arma-tenencia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-armeria" class="icono-arg-armeria icono-4x text-gray" data-clipboard-text="icono-arg-armeria"></i>
                        <br>
                        <p class="name m-b-0">armeria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-autentificacion" class="icono-arg-autentificacion icono-4x text-gray" data-clipboard-text="icono-arg-autentificacion"></i>
                        <br>
                        <p class="name m-b-0">autentificacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-auto" class="icono-arg-auto icono-4x text-gray" data-clipboard-text="icono-arg-auto"></i>
                        <br>
                        <p class="name m-b-0">auto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-auto-informe" class="icono-arg-auto-informe icono-4x text-gray" data-clipboard-text="icono-arg-auto-informe"></i>
                        <br>
                        <p class="name m-b-0">auto-informe</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-auto-licencia" class="icono-arg-auto-licencia icono-4x text-gray" data-clipboard-text="icono-arg-auto-licencia"></i>
                        <br>
                        <p class="name m-b-0">auto-licencia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-auto-licencia-ok" class="icono-arg-auto-licencia-ok icono-4x text-gray" data-clipboard-text="icono-arg-auto-licencia-ok"></i>
                        <br>
                        <p class="name m-b-0">auto-licencia-ok</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-automotor" class="icono-arg-automotor icono-4x text-gray" data-clipboard-text="icono-arg-automotor"></i>
                        <br>
                        <p class="name m-b-0">automotor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-automotor-negativo" class="icono-arg-automotor-negativo icono-4x text-gray" data-clipboard-text="icono-arg-automotor-negativo"></i>
                        <br>
                        <p class="name m-b-0">automotor-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-avion" class="icono-arg-avion icono-4x text-gray" data-clipboard-text="icono-arg-avion"></i>
                        <br>
                        <p class="name m-b-0">avion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-avion-negativo" class="icono-arg-avion-negativo icono-4x text-gray" data-clipboard-text="icono-arg-avion-negativo"></i>
                        <br>
                        <p class="name m-b-0">avion-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-azafata" class="icono-arg-azafata icono-4x text-gray" data-clipboard-text="icono-arg-azafata"></i>
                        <br>
                        <p class="name m-b-0">azafata</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bacteria" class="icono-arg-bacteria icono-4x text-gray" data-clipboard-text="icono-arg-bacteria"></i>
                        <br>
                        <p class="name m-b-0">bacteria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bailar" class="icono-arg-bailar icono-4x text-gray" data-clipboard-text="icono-arg-bailar"></i>
                        <br>
                        <p class="name m-b-0">bailar</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bananas" class="icono-arg-bananas icono-4x text-gray" data-clipboard-text="icono-arg-bananas"></i>
                        <br>
                        <p class="name m-b-0">bananas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bandera" class="icono-arg-bandera icono-4x text-gray" data-clipboard-text="icono-arg-bandera"></i>
                        <br>
                        <p class="name m-b-0">bandera</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-banio-quimico" class="icono-arg-banio-quimico icono-4x text-gray" data-clipboard-text="icono-arg-banio-quimico"></i>
                        <br>
                        <p class="name m-b-0">banio-quimico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-barbijo" class="icono-arg-barbijo icono-4x text-gray" data-clipboard-text="icono-arg-barbijo"></i>
                        <br>
                        <p class="name m-b-0">barbijo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-barrilete" class="icono-arg-barrilete icono-4x text-gray" data-clipboard-text="icono-arg-barrilete"></i>
                        <br>
                        <p class="name m-b-0">barrilete</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-barrio" class="icono-arg-barrio icono-4x text-gray" data-clipboard-text="icono-arg-barrio"></i>
                        <br>
                        <p class="name m-b-0">barrio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-billetera-lineal" class="icono-arg-billetera-lineal icono-4x text-gray" data-clipboard-text="icono-arg-billetera-lineal"></i>
                        <br>
                        <p class="name m-b-0">billetera-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-billetera-negativo" class="icono-arg-billetera-negativo icono-4x text-gray" data-clipboard-text="icono-arg-billetera-negativo"></i>
                        <br>
                        <p class="name m-b-0">billetera-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bolsa-compras" class="icono-arg-bolsa-compras icono-4x text-gray" data-clipboard-text="icono-arg-bolsa-compras"></i>
                        <br>
                        <p class="name m-b-0">bolsa-compras</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bolsa-dinero" class="icono-arg-bolsa-dinero icono-4x text-gray" data-clipboard-text="icono-arg-bolsa-dinero"></i>
                        <br>
                        <p class="name m-b-0">bolsa-dinero</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-bosques" class="icono-arg-bosques icono-4x text-gray" data-clipboard-text="icono-arg-bosques"></i>
                        <br>
                        <p class="name m-b-0">bosques</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-buceo" class="icono-arg-buceo icono-4x text-gray" data-clipboard-text="icono-arg-buceo"></i>
                        <br>
                        <p class="name m-b-0">buceo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caja-1" class="icono-arg-caja-1 icono-4x text-gray" data-clipboard-text="icono-arg-caja-1"></i>
                        <br>
                        <p class="name m-b-0">caja-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caja-2" class="icono-arg-caja-2 icono-4x text-gray" data-clipboard-text="icono-arg-caja-2"></i>
                        <br>
                        <p class="name m-b-0">caja-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caja-exp" class="icono-arg-caja-exp icono-4x text-gray" data-clipboard-text="icono-arg-caja-exp"></i>
                        <br>
                        <p class="name m-b-0">caja-exp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caja-exp-2" class="icono-arg-caja-exp-2 icono-4x text-gray" data-clipboard-text="icono-arg-caja-exp-2"></i>
                        <br>
                        <p class="name m-b-0">caja-exp-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cajero" class="icono-arg-cajero icono-4x text-gray" data-clipboard-text="icono-arg-cajero"></i>
                        <br>
                        <p class="name m-b-0">cajero</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calefactor" class="icono-arg-calefactor icono-4x text-gray" data-clipboard-text="icono-arg-calefactor"></i>
                        <br>
                        <p class="name m-b-0">calefactor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calefactor-negativo" class="icono-arg-calefactor-negativo icono-4x text-gray" data-clipboard-text="icono-arg-calefactor-negativo"></i>
                        <br>
                        <p class="name m-b-0">calefactor-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calendario" class="icono-arg-calendario icono-4x text-gray" data-clipboard-text="icono-arg-calendario"></i>
                        <br>
                        <p class="name m-b-0">calendario</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calendario-2" class="icono-arg-calendario-2 icono-4x text-gray" data-clipboard-text="icono-arg-calendario-2"></i>
                        <br>
                        <p class="name m-b-0">calendario-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calendario-2-negativo" class="icono-arg-calendario-2-negativo icono-4x text-gray" data-clipboard-text="icono-arg-calendario-2-negativo"></i>
                        <br>
                        <p class="name m-b-0">calendario-2-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calibracion-dosimetros" class="icono-arg-calibracion-dosimetros icono-4x text-gray" data-clipboard-text="icono-arg-calibracion-dosimetros"></i>
                        <br>
                        <p class="name m-b-0">calibracion-dosimetros</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-calidad-cert" class="icono-arg-calidad-cert icono-4x text-gray" data-clipboard-text="icono-arg-calidad-cert"></i>
                        <br>
                        <p class="name m-b-0">calidad-cert</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-camara-vigilancia" class="icono-arg-camara-vigilancia icono-4x text-gray" data-clipboard-text="icono-arg-camara-vigilancia"></i>
                        <br>
                        <p class="name m-b-0">camara-vigilancia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cambio" class="icono-arg-cambio icono-4x text-gray" data-clipboard-text="icono-arg-cambio"></i>
                        <br>
                        <p class="name m-b-0">cambio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cambio-moneda" class="icono-arg-cambio-moneda icono-4x text-gray" data-clipboard-text="icono-arg-cambio-moneda"></i>
                        <br>
                        <p class="name m-b-0">cambio-moneda</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cambio-tecnologico" class="icono-arg-cambio-tecnologico icono-4x text-gray" data-clipboard-text="icono-arg-cambio-tecnologico"></i>
                        <br>
                        <p class="name m-b-0">cambio-tecnologico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caminar" class="icono-arg-caminar icono-4x text-gray" data-clipboard-text="icono-arg-caminar"></i>
                        <br>
                        <p class="name m-b-0">caminar</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-camion-construccion" class="icono-arg-camion-construccion icono-4x text-gray" data-clipboard-text="icono-arg-camion-construccion"></i>
                        <br>
                        <p class="name m-b-0">camion-construccion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-candado-abierto" class="icono-arg-candado-abierto icono-4x text-gray" data-clipboard-text="icono-arg-candado-abierto"></i>
                        <br>
                        <p class="name m-b-0">candado-abierto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-candado-cerrado" class="icono-arg-candado-cerrado icono-4x text-gray" data-clipboard-text="icono-arg-candado-cerrado"></i>
                        <br>
                        <p class="name m-b-0">candado-cerrado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-canilla" class="icono-arg-canilla icono-4x text-gray" data-clipboard-text="icono-arg-canilla"></i>
                        <br>
                        <p class="name m-b-0">canilla</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-canilla-2" class="icono-arg-canilla-2 icono-4x text-gray" data-clipboard-text="icono-arg-canilla-2"></i>
                        <br>
                        <p class="name m-b-0">canilla-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cannabis-medicinal-1" class="icono-arg-cannabis-medicinal-1 icono-4x text-gray" data-clipboard-text="icono-arg-cannabis-medicinal-1"></i>
                        <br>
                        <p class="name m-b-0">cannabis-medicinal-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cannabis-medicinal-2" class="icono-arg-cannabis-medicinal-2 icono-4x text-gray" data-clipboard-text="icono-arg-cannabis-medicinal-2"></i>
                        <br>
                        <p class="name m-b-0">cannabis-medicinal-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-capacitacion" class="icono-arg-capacitacion icono-4x text-gray" data-clipboard-text="icono-arg-capacitacion"></i>
                        <br>
                        <p class="name m-b-0">capacitacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cardon" class="icono-arg-cardon icono-4x text-gray" data-clipboard-text="icono-arg-cardon"></i>
                        <br>
                        <p class="name m-b-0">cardon</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-carrito" class="icono-arg-carrito icono-4x text-gray" data-clipboard-text="icono-arg-carrito"></i>
                        <br>
                        <p class="name m-b-0">carrito</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-carrito-mas" class="icono-arg-carrito-mas icono-4x text-gray" data-clipboard-text="icono-arg-carrito-mas"></i>
                        <br>
                        <p class="name m-b-0">carrito-mas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-casa" class="icono-arg-casa icono-4x text-gray" data-clipboard-text="icono-arg-casa"></i>
                        <br>
                        <p class="name m-b-0">casa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-casa-negativo" class="icono-arg-casa-negativo icono-4x text-gray" data-clipboard-text="icono-arg-casa-negativo"></i>
                        <br>
                        <p class="name m-b-0">casa-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-casco" class="icono-arg-casco icono-4x text-gray" data-clipboard-text="icono-arg-casco"></i>
                        <br>
                        <p class="name m-b-0">casco</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-casco-negativo" class="icono-arg-casco-negativo icono-4x text-gray" data-clipboard-text="icono-arg-casco-negativo"></i>
                        <br>
                        <p class="name m-b-0">casco-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-casco-urbano" class="icono-arg-casco-urbano icono-4x text-gray" data-clipboard-text="icono-arg-casco-urbano"></i>
                        <br>
                        <p class="name m-b-0">casco-urbano</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cataratas" class="icono-arg-cataratas icono-4x text-gray" data-clipboard-text="icono-arg-cataratas"></i>
                        <br>
                        <p class="name m-b-0">cataratas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-caza" class="icono-arg-caza icono-4x text-gray" data-clipboard-text="icono-arg-caza"></i>
                        <br>
                        <p class="name m-b-0">caza</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cedula-azul" class="icono-arg-cedula-azul icono-4x text-gray" data-clipboard-text="icono-arg-cedula-azul"></i>
                        <br>
                        <p class="name m-b-0">cedula-azul</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cedula-verde" class="icono-arg-cedula-verde icono-4x text-gray" data-clipboard-text="icono-arg-cedula-verde"></i>
                        <br>
                        <p class="name m-b-0">cedula-verde</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-celular" class="icono-arg-celular icono-4x text-gray" data-clipboard-text="icono-arg-celular"></i>
                        <br>
                        <p class="name m-b-0">celular</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-central-nuclear" class="icono-arg-central-nuclear icono-4x text-gray" data-clipboard-text="icono-arg-central-nuclear"></i>
                        <br>
                        <p class="name m-b-0">central-nuclear</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-central-nuclear-2" class="icono-arg-central-nuclear-2 icono-4x text-gray" data-clipboard-text="icono-arg-central-nuclear-2"></i>
                        <br>
                        <p class="name m-b-0">central-nuclear-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-central-y-profesional-nuclear" class="icono-arg-central-y-profesional-nuclear icono-4x text-gray" data-clipboard-text="icono-arg-central-y-profesional-nuclear"></i>
                        <br>
                        <p class="name m-b-0">central-y-profesional-nuclear</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-centro-atomico-lineal" class="icono-arg-centro-atomico-lineal icono-4x text-gray" data-clipboard-text="icono-arg-centro-atomico-lineal"></i>
                        <br>
                        <p class="name m-b-0">centro-atomico-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-centro-atomico-neg" class="icono-arg-centro-atomico-neg icono-4x text-gray" data-clipboard-text="icono-arg-centro-atomico-neg"></i>
                        <br>
                        <p class="name m-b-0">centro-atomico-neg</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cerdo" class="icono-arg-cerdo icono-4x text-gray" data-clipboard-text="icono-arg-cerdo"></i>
                        <br>
                        <p class="name m-b-0">cerdo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-chat" class="icono-arg-chat icono-4x text-gray" data-clipboard-text="icono-arg-chat"></i>
                        <br>
                        <p class="name m-b-0">chat</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-chequeado" class="icono-arg-chequeado icono-4x text-gray" data-clipboard-text="icono-arg-chequeado"></i>
                        <br>
                        <p class="name m-b-0">chequeado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-chupete" class="icono-arg-chupete icono-4x text-gray" data-clipboard-text="icono-arg-chupete"></i>
                        <br>
                        <p class="name m-b-0">chupete</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ciencia-apoyo-economico" class="icono-arg-ciencia-apoyo-economico icono-4x text-gray" data-clipboard-text="icono-arg-ciencia-apoyo-economico"></i>
                        <br>
                        <p class="name m-b-0">ciencia-apoyo-economico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ciencia-publicacion" class="icono-arg-ciencia-publicacion icono-4x text-gray" data-clipboard-text="icono-arg-ciencia-publicacion"></i>
                        <br>
                        <p class="name m-b-0">ciencia-publicacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cine" class="icono-arg-cine icono-4x text-gray" data-clipboard-text="icono-arg-cine"></i>
                        <br>
                        <p class="name m-b-0">cine</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cinturon-seguridad" class="icono-arg-cinturon-seguridad icono-4x text-gray" data-clipboard-text="icono-arg-cinturon-seguridad"></i>
                        <br>
                        <p class="name m-b-0">cinturon-seguridad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cinturon-seguridad-negativo" class="icono-arg-cinturon-seguridad-negativo icono-4x text-gray" data-clipboard-text="icono-arg-cinturon-seguridad-negativo"></i>
                        <br>
                        <p class="name m-b-0">cinturon-seguridad-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cluse" class="icono-arg-cluse icono-4x text-gray" data-clipboard-text="icono-arg-cluse"></i>
                        <br>
                        <p class="name m-b-0">cluse</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cobro-pago" class="icono-arg-cobro-pago icono-4x text-gray" data-clipboard-text="icono-arg-cobro-pago"></i>
                        <br>
                        <p class="name m-b-0">cobro-pago</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cobros" class="icono-arg-cobros icono-4x text-gray" data-clipboard-text="icono-arg-cobros"></i>
                        <br>
                        <p class="name m-b-0">cobros</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cobros-negativo" class="icono-arg-cobros-negativo icono-4x text-gray" data-clipboard-text="icono-arg-cobros-negativo"></i>
                        <br>
                        <p class="name m-b-0">cobros-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cochecito" class="icono-arg-cochecito icono-4x text-gray" data-clipboard-text="icono-arg-cochecito"></i>
                        <br>
                        <p class="name m-b-0">cochecito</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cocina" class="icono-arg-cocina icono-4x text-gray" data-clipboard-text="icono-arg-cocina"></i>
                        <br>
                        <p class="name m-b-0">cocina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cocina-negativo" class="icono-arg-cocina-negativo icono-4x text-gray" data-clipboard-text="icono-arg-cocina-negativo"></i>
                        <br>
                        <p class="name m-b-0">cocina-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-codigo-alimentario-argentino" class="icono-arg-codigo-alimentario-argentino icono-4x text-gray" data-clipboard-text="icono-arg-codigo-alimentario-argentino"></i>
                        <br>
                        <p class="name m-b-0">codigo-alimentario-argentino</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-codigo-genetico" class="icono-arg-codigo-genetico icono-4x text-gray" data-clipboard-text="icono-arg-codigo-genetico"></i>
                        <br>
                        <p class="name m-b-0">codigo-genetico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-codigo-qr" class="icono-arg-codigo-qr icono-4x text-gray" data-clipboard-text="icono-arg-codigo-qr"></i>
                        <br>
                        <p class="name m-b-0">codigo-qr</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-computadora" class="icono-arg-computadora icono-4x text-gray" data-clipboard-text="icono-arg-computadora"></i>
                        <br>
                        <p class="name m-b-0">computadora</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-computadora-celular-tablet" class="icono-arg-computadora-celular-tablet icono-4x text-gray" data-clipboard-text="icono-arg-computadora-celular-tablet"></i>
                        <br>
                        <p class="name m-b-0">computadora-celular-tablet</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-comunicacion" class="icono-arg-comunicacion icono-4x text-gray" data-clipboard-text="icono-arg-comunicacion"></i>
                        <br>
                        <p class="name m-b-0">comunicacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-comunidad" class="icono-arg-comunidad icono-4x text-gray" data-clipboard-text="icono-arg-comunidad"></i>
                        <br>
                        <p class="name m-b-0">comunidad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-conexion" class="icono-arg-conexion icono-4x text-gray" data-clipboard-text="icono-arg-conexion"></i>
                        <br>
                        <p class="name m-b-0">conexion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-construccion" class="icono-arg-construccion icono-4x text-gray" data-clipboard-text="icono-arg-construccion"></i>
                        <br>
                        <p class="name m-b-0">construccion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-consulta" class="icono-arg-consulta icono-4x text-gray" data-clipboard-text="icono-arg-consulta"></i>
                        <br>
                        <p class="name m-b-0">consulta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-corazon-cruz" class="icono-arg-corazon-cruz icono-4x text-gray" data-clipboard-text="icono-arg-corazon-cruz"></i>
                        <br>
                        <p class="name m-b-0">corazon-cruz</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cosmetovigilancia" class="icono-arg-cosmetovigilancia icono-4x text-gray" data-clipboard-text="icono-arg-cosmetovigilancia"></i>
                        <br>
                        <p class="name m-b-0">cosmetovigilancia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cotizacion-envio" class="icono-arg-cotizacion-envio icono-4x text-gray" data-clipboard-text="icono-arg-cotizacion-envio"></i>
                        <br>
                        <p class="name m-b-0">cotizacion-envio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-credenciales" class="icono-arg-credenciales icono-4x text-gray" data-clipboard-text="icono-arg-credenciales"></i>
                        <br>
                        <p class="name m-b-0">credenciales</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-credenciales-negativo" class="icono-arg-credenciales-negativo icono-4x text-gray" data-clipboard-text="icono-arg-credenciales-negativo"></i>
                        <br>
                        <p class="name m-b-0">credenciales-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cruz-medicina" class="icono-arg-cruz-medicina icono-4x text-gray" data-clipboard-text="icono-arg-cruz-medicina"></i>
                        <br>
                        <p class="name m-b-0">cruz-medicina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cruz-medicina-negativo" class="icono-arg-cruz-medicina-negativo icono-4x text-gray" data-clipboard-text="icono-arg-cruz-medicina-negativo"></i>
                        <br>
                        <p class="name m-b-0">cruz-medicina-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cud" class="icono-arg-cud icono-4x text-gray" data-clipboard-text="icono-arg-cud"></i>
                        <br>
                        <p class="name m-b-0">cud</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cultura" class="icono-arg-cultura icono-4x text-gray" data-clipboard-text="icono-arg-cultura"></i>
                        <br>
                        <p class="name m-b-0">cultura</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cultura-exp" class="icono-arg-cultura-exp icono-4x text-gray" data-clipboard-text="icono-arg-cultura-exp"></i>
                        <br>
                        <p class="name m-b-0">cultura-exp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cultura-imp" class="icono-arg-cultura-imp icono-4x text-gray" data-clipboard-text="icono-arg-cultura-imp"></i>
                        <br>
                        <p class="name m-b-0">cultura-imp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-curita" class="icono-arg-curita icono-4x text-gray" data-clipboard-text="icono-arg-curita"></i>
                        <br>
                        <p class="name m-b-0">curita</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-curriculum" class="icono-arg-curriculum icono-4x text-gray" data-clipboard-text="icono-arg-curriculum"></i>
                        <br>
                        <p class="name m-b-0">curriculum</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-cus" class="icono-arg-cus icono-4x text-gray" data-clipboard-text="icono-arg-cus"></i>
                        <br>
                        <p class="name m-b-0">cus</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-database" class="icono-arg-database icono-4x text-gray" data-clipboard-text="icono-arg-database"></i>
                        <br>
                        <p class="name m-b-0">database</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-datos-abiertos" class="icono-arg-datos-abiertos icono-4x text-gray" data-clipboard-text="icono-arg-datos-abiertos"></i>
                        <br>
                        <p class="name m-b-0">datos-abiertos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-datos-abiertos-2" class="icono-arg-datos-abiertos-2 icono-4x text-gray" data-clipboard-text="icono-arg-datos-abiertos-2"></i>
                        <br>
                        <p class="name m-b-0">datos-abiertos-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-datos-envio" class="icono-arg-datos-envio icono-4x text-gray" data-clipboard-text="icono-arg-datos-envio"></i>
                        <br>
                        <p class="name m-b-0">datos-envio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-declaracion-jurada" class="icono-arg-declaracion-jurada icono-4x text-gray" data-clipboard-text="icono-arg-declaracion-jurada"></i>
                        <br>
                        <p class="name m-b-0">declaracion-jurada</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-declaracion-jurada-rectificativa" class="icono-arg-declaracion-jurada-rectificativa icono-4x text-gray" data-clipboard-text="icono-arg-declaracion-jurada-rectificativa"></i>
                        <br>
                        <p class="name m-b-0">declaracion-jurada-rectificativa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-deficit-habitacional" class="icono-arg-deficit-habitacional icono-4x text-gray" data-clipboard-text="icono-arg-deficit-habitacional"></i>
                        <br>
                        <p class="name m-b-0">deficit-habitacional</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-deporte" class="icono-arg-deporte icono-4x text-gray" data-clipboard-text="icono-arg-deporte"></i>
                        <br>
                        <p class="name m-b-0">deporte</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-derechos-de-autor" class="icono-arg-derechos-de-autor icono-4x text-gray" data-clipboard-text="icono-arg-derechos-de-autor"></i>
                        <br>
                        <p class="name m-b-0">derechos-de-autor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-derrame-toxico" class="icono-arg-derrame-toxico icono-4x text-gray" data-clipboard-text="icono-arg-derrame-toxico"></i>
                        <br>
                        <p class="name m-b-0">derrame-toxico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-derrumbes" class="icono-arg-derrumbes icono-4x text-gray" data-clipboard-text="icono-arg-derrumbes"></i>
                        <br>
                        <p class="name m-b-0">derrumbes</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-desarrollo" class="icono-arg-desarrollo icono-4x text-gray" data-clipboard-text="icono-arg-desarrollo"></i>
                        <br>
                        <p class="name m-b-0">desarrollo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-desierto" class="icono-arg-desierto icono-4x text-gray" data-clipboard-text="icono-arg-desierto"></i>
                        <br>
                        <p class="name m-b-0">desierto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-dialogo" class="icono-arg-dialogo icono-4x text-gray" data-clipboard-text="icono-arg-dialogo"></i>
                        <br>
                        <p class="name m-b-0">dialogo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-dialogo-2" class="icono-arg-dialogo-2 icono-4x text-gray" data-clipboard-text="icono-arg-dialogo-2"></i>
                        <br>
                        <p class="name m-b-0">dialogo-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-digital" class="icono-arg-digital icono-4x text-gray" data-clipboard-text="icono-arg-digital"></i>
                        <br>
                        <p class="name m-b-0">digital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-diploma" class="icono-arg-diploma icono-4x text-gray" data-clipboard-text="icono-arg-diploma"></i>
                        <br>
                        <p class="name m-b-0">diploma</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-diploma-negativo" class="icono-arg-diploma-negativo icono-4x text-gray" data-clipboard-text="icono-arg-diploma-negativo"></i>
                        <br>
                        <p class="name m-b-0">diploma-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-discapacidad" class="icono-arg-discapacidad icono-4x text-gray" data-clipboard-text="icono-arg-discapacidad"></i>
                        <br>
                        <p class="name m-b-0">discapacidad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-discapacidad-2" class="icono-arg-discapacidad-2 icono-4x text-gray" data-clipboard-text="icono-arg-discapacidad-2"></i>
                        <br>
                        <p class="name m-b-0">discapacidad-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-discapacidad-2-negativo" class="icono-arg-discapacidad-2-negativo icono-4x text-gray" data-clipboard-text="icono-arg-discapacidad-2-negativo"></i>
                        <br>
                        <p class="name m-b-0">discapacidad-2-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-discapacidad-3" class="icono-arg-discapacidad-3 icono-4x text-gray" data-clipboard-text="icono-arg-discapacidad-3"></i>
                        <br>
                        <p class="name m-b-0">discapacidad-3</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-discapacidad-3-negativo" class="icono-arg-discapacidad-3-negativo icono-4x text-gray" data-clipboard-text="icono-arg-discapacidad-3-negativo"></i>
                        <br>
                        <p class="name m-b-0">discapacidad-3-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-diversidad-sexual" class="icono-arg-diversidad-sexual icono-4x text-gray" data-clipboard-text="icono-arg-diversidad-sexual"></i>
                        <br>
                        <p class="name m-b-0">diversidad-sexual</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-dni" class="icono-arg-dni icono-4x text-gray" data-clipboard-text="icono-arg-dni"></i>
                        <br>
                        <p class="name m-b-0">dni</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-documento" class="icono-arg-documento icono-4x text-gray" data-clipboard-text="icono-arg-documento"></i>
                        <br>
                        <p class="name m-b-0">documento</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ducha" class="icono-arg-ducha icono-4x text-gray" data-clipboard-text="icono-arg-ducha"></i>
                        <br>
                        <p class="name m-b-0">ducha</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ducha-negativo" class="icono-arg-ducha-negativo icono-4x text-gray" data-clipboard-text="icono-arg-ducha-negativo"></i>
                        <br>
                        <p class="name m-b-0">ducha-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-educacion" class="icono-arg-educacion icono-4x text-gray" data-clipboard-text="icono-arg-educacion"></i>
                        <br>
                        <p class="name m-b-0">educacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-eleccion" class="icono-arg-eleccion icono-4x text-gray" data-clipboard-text="icono-arg-eleccion"></i>
                        <br>
                        <p class="name m-b-0">eleccion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-electricidad" class="icono-arg-electricidad icono-4x text-gray" data-clipboard-text="icono-arg-electricidad"></i>
                        <br>
                        <p class="name m-b-0">electricidad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-electrodependiente-2" class="icono-arg-electrodependiente-2 icono-4x text-gray" data-clipboard-text="icono-arg-electrodependiente-2"></i>
                        <br>
                        <p class="name m-b-0">electrodependiente-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-electrodependientes-1" class="icono-arg-electrodependientes-1 icono-4x text-gray" data-clipboard-text="icono-arg-electrodependientes-1"></i>
                        <br>
                        <p class="name m-b-0">electrodependientes-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-embarazo" class="icono-arg-embarazo icono-4x text-gray" data-clipboard-text="icono-arg-embarazo"></i>
                        <br>
                        <p class="name m-b-0">embarazo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-embarcacion" class="icono-arg-embarcacion icono-4x text-gray" data-clipboard-text="icono-arg-embarcacion"></i>
                        <br>
                        <p class="name m-b-0">embarcacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-embarcacion-pago" class="icono-arg-embarcacion-pago icono-4x text-gray" data-clipboard-text="icono-arg-embarcacion-pago"></i>
                        <br>
                        <p class="name m-b-0">embarcacion-pago</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-empleo-2" class="icono-arg-empleo-2 icono-4x text-gray" data-clipboard-text="icono-arg-empleo-2"></i>
                        <br>
                        <p class="name m-b-0">empleo-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-empresas-e-industria" class="icono-arg-empresas-e-industria icono-4x text-gray" data-clipboard-text="icono-arg-empresas-e-industria"></i>
                        <br>
                        <p class="name m-b-0">empresas-e-industria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-eolica" class="icono-arg-energia-eolica icono-4x text-gray" data-clipboard-text="icono-arg-energia-eolica"></i>
                        <br>
                        <p class="name m-b-0">energia-eolica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-hidroelectrica" class="icono-arg-energia-hidroelectrica icono-4x text-gray" data-clipboard-text="icono-arg-energia-hidroelectrica"></i>
                        <br>
                        <p class="name m-b-0">energia-hidroelectrica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-nuclear" class="icono-arg-energia-nuclear icono-4x text-gray" data-clipboard-text="icono-arg-energia-nuclear"></i>
                        <br>
                        <p class="name m-b-0">energia-nuclear</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-solar" class="icono-arg-energia-solar icono-4x text-gray" data-clipboard-text="icono-arg-energia-solar"></i>
                        <br>
                        <p class="name m-b-0">energia-solar</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-solar-casa" class="icono-arg-energia-solar-casa icono-4x text-gray" data-clipboard-text="icono-arg-energia-solar-casa"></i>
                        <br>
                        <p class="name m-b-0">energia-solar-casa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-solar-industria" class="icono-arg-energia-solar-industria icono-4x text-gray" data-clipboard-text="icono-arg-energia-solar-industria"></i>
                        <br>
                        <p class="name m-b-0">energia-solar-industria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-energia-sustentable-2" class="icono-arg-energia-sustentable-2 icono-4x text-gray" data-clipboard-text="icono-arg-energia-sustentable-2"></i>
                        <br>
                        <p class="name m-b-0">energia-sustentable-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-envio-aereo" class="icono-arg-envio-aereo icono-4x text-gray" data-clipboard-text="icono-arg-envio-aereo"></i>
                        <br>
                        <p class="name m-b-0">envio-aereo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-envio-maritimo" class="icono-arg-envio-maritimo icono-4x text-gray" data-clipboard-text="icono-arg-envio-maritimo"></i>
                        <br>
                        <p class="name m-b-0">envio-maritimo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-envio-terrestre" class="icono-arg-envio-terrestre icono-4x text-gray" data-clipboard-text="icono-arg-envio-terrestre"></i>
                        <br>
                        <p class="name m-b-0">envio-terrestre</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-envio-terrestre-1" class="icono-arg-envio-terrestre-1 icono-4x text-gray" data-clipboard-text="icono-arg-envio-terrestre-1"></i>
                        <br>
                        <p class="name m-b-0">envio-terrestre-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-erupciones-volcanicas" class="icono-arg-erupciones-volcanicas icono-4x text-gray" data-clipboard-text="icono-arg-erupciones-volcanicas"></i>
                        <br>
                        <p class="name m-b-0">erupciones-volcanicas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-escarapela" class="icono-arg-escarapela icono-4x text-gray" data-clipboard-text="icono-arg-escarapela"></i>
                        <br>
                        <p class="name m-b-0">escarapela</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-escritura" class="icono-arg-escritura icono-4x text-gray" data-clipboard-text="icono-arg-escritura"></i>
                        <br>
                        <p class="name m-b-0">escritura</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-escudo-nacional" class="icono-arg-escudo-nacional icono-4x text-gray" data-clipboard-text="icono-arg-escudo-nacional"></i>
                        <br>
                        <p class="name m-b-0">escudo-nacional</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-espacio-publico" class="icono-arg-espacio-publico icono-4x text-gray" data-clipboard-text="icono-arg-espacio-publico"></i>
                        <br>
                        <p class="name m-b-0">espacio-publico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-espacios" class="icono-arg-espacios icono-4x text-gray" data-clipboard-text="icono-arg-espacios"></i>
                        <br>
                        <p class="name m-b-0">espacios</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-estacion-trenes" class="icono-arg-estacion-trenes icono-4x text-gray" data-clipboard-text="icono-arg-estacion-trenes"></i>
                        <br>
                        <p class="name m-b-0">estacion-trenes</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-estereo" class="icono-arg-estereo icono-4x text-gray" data-clipboard-text="icono-arg-estereo"></i>
                        <br>
                        <p class="name m-b-0">estereo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-estetoscopio" class="icono-arg-estetoscopio icono-4x text-gray" data-clipboard-text="icono-arg-estetoscopio"></i>
                        <br>
                        <p class="name m-b-0">estetoscopio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-etiqueta-eficiencia" class="icono-arg-etiqueta-eficiencia icono-4x text-gray" data-clipboard-text="icono-arg-etiqueta-eficiencia"></i>
                        <br>
                        <p class="name m-b-0">etiqueta-eficiencia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-exporta-facil" class="icono-arg-exporta-facil icono-4x text-gray" data-clipboard-text="icono-arg-exporta-facil"></i>
                        <br>
                        <p class="name m-b-0">exporta-facil</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-exterior" class="icono-arg-exterior icono-4x text-gray" data-clipboard-text="icono-arg-exterior"></i>
                        <br>
                        <p class="name m-b-0">exterior</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-factor-pago" class="icono-arg-factor-pago icono-4x text-gray" data-clipboard-text="icono-arg-factor-pago"></i>
                        <br>
                        <p class="name m-b-0">factor-pago</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-factura" class="icono-arg-factura icono-4x text-gray" data-clipboard-text="icono-arg-factura"></i>
                        <br>
                        <p class="name m-b-0">factura</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-fallecimiento" class="icono-arg-fallecimiento icono-4x text-gray" data-clipboard-text="icono-arg-fallecimiento"></i>
                        <br>
                        <p class="name m-b-0">fallecimiento</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-famila" class="icono-arg-famila icono-4x text-gray" data-clipboard-text="icono-arg-famila"></i>
                        <br>
                        <p class="name m-b-0">famila</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-familia-2" class="icono-arg-familia-2 icono-4x text-gray" data-clipboard-text="icono-arg-familia-2"></i>
                        <br>
                        <p class="name m-b-0">familia-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-farmacovigilancia" class="icono-arg-farmacovigilancia icono-4x text-gray" data-clipboard-text="icono-arg-farmacovigilancia"></i>
                        <br>
                        <p class="name m-b-0">farmacovigilancia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-fertilizacion-asistida" class="icono-arg-fertilizacion-asistida icono-4x text-gray" data-clipboard-text="icono-arg-fertilizacion-asistida"></i>
                        <br>
                        <p class="name m-b-0">fertilizacion-asistida</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-financiamiento" class="icono-arg-financiamiento icono-4x text-gray" data-clipboard-text="icono-arg-financiamiento"></i>
                        <br>
                        <p class="name m-b-0">financiamiento</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-finanzas" class="icono-arg-finanzas icono-4x text-gray" data-clipboard-text="icono-arg-finanzas"></i>
                        <br>
                        <p class="name m-b-0">finanzas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-firma-contrato" class="icono-arg-firma-contrato icono-4x text-gray" data-clipboard-text="icono-arg-firma-contrato"></i>
                        <br>
                        <p class="name m-b-0">firma-contrato</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-firma-digital" class="icono-arg-firma-digital icono-4x text-gray" data-clipboard-text="icono-arg-firma-digital"></i>
                        <br>
                        <p class="name m-b-0">firma-digital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-firma-digital-con-token" class="icono-arg-firma-digital-con-token icono-4x text-gray" data-clipboard-text="icono-arg-firma-digital-con-token"></i>
                        <br>
                        <p class="name m-b-0">firma-digital-con-token</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-firma-digital-remota" class="icono-arg-firma-digital-remota icono-4x text-gray" data-clipboard-text="icono-arg-firma-digital-remota"></i>
                        <br>
                        <p class="name m-b-0">firma-digital-remota</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-frio" class="icono-arg-frio icono-4x text-gray" data-clipboard-text="icono-arg-frio"></i>
                        <br>
                        <p class="name m-b-0">frio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-frio-2" class="icono-arg-frio-2 icono-4x text-gray" data-clipboard-text="icono-arg-frio-2"></i>
                        <br>
                        <p class="name m-b-0">frio-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-frio-calor" class="icono-arg-frio-calor icono-4x text-gray" data-clipboard-text="icono-arg-frio-calor"></i>
                        <br>
                        <p class="name m-b-0">frio-calor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-frutas-y-verduras" class="icono-arg-frutas-y-verduras icono-4x text-gray" data-clipboard-text="icono-arg-frutas-y-verduras"></i>
                        <br>
                        <p class="name m-b-0">frutas-y-verduras</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-frutilla" class="icono-arg-frutilla icono-4x text-gray" data-clipboard-text="icono-arg-frutilla"></i>
                        <br>
                        <p class="name m-b-0">frutilla</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-fuego" class="icono-arg-fuego icono-4x text-gray" data-clipboard-text="icono-arg-fuego"></i>
                        <br>
                        <p class="name m-b-0">fuego</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-fuga-material-radioactivo" class="icono-arg-fuga-material-radioactivo icono-4x text-gray" data-clipboard-text="icono-arg-fuga-material-radioactivo"></i>
                        <br>
                        <p class="name m-b-0">fuga-material-radioactivo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gallina" class="icono-arg-gallina icono-4x text-gray" data-clipboard-text="icono-arg-gallina"></i>
                        <br>
                        <p class="name m-b-0">gallina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ganaderia" class="icono-arg-ganaderia icono-4x text-gray" data-clipboard-text="icono-arg-ganaderia"></i>
                        <br>
                        <p class="name m-b-0">ganaderia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-garrafa" class="icono-arg-garrafa icono-4x text-gray" data-clipboard-text="icono-arg-garrafa"></i>
                        <br>
                        <p class="name m-b-0">garrafa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gas" class="icono-arg-gas icono-4x text-gray" data-clipboard-text="icono-arg-gas"></i>
                        <br>
                        <p class="name m-b-0">gas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gaseosa" class="icono-arg-gaseosa icono-4x text-gray" data-clipboard-text="icono-arg-gaseosa"></i>
                        <br>
                        <p class="name m-b-0">gaseosa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gaseosa-prohibida" class="icono-arg-gaseosa-prohibida icono-4x text-gray" data-clipboard-text="icono-arg-gaseosa-prohibida"></i>
                        <br>
                        <p class="name m-b-0">gaseosa-prohibida</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gastos" class="icono-arg-gastos icono-4x text-gray" data-clipboard-text="icono-arg-gastos"></i>
                        <br>
                        <p class="name m-b-0">gastos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gastos-recursos" class="icono-arg-gastos-recursos icono-4x text-gray" data-clipboard-text="icono-arg-gastos-recursos"></i>
                        <br>
                        <p class="name m-b-0">gastos-recursos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-genero" class="icono-arg-genero icono-4x text-gray" data-clipboard-text="icono-arg-genero"></i>
                        <br>
                        <p class="name m-b-0">genero</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-genetica" class="icono-arg-genetica icono-4x text-gray" data-clipboard-text="icono-arg-genetica"></i>
                        <br>
                        <p class="name m-b-0">genetica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-genetica-exp-imp" class="icono-arg-genetica-exp-imp icono-4x text-gray" data-clipboard-text="icono-arg-genetica-exp-imp"></i>
                        <br>
                        <p class="name m-b-0">genetica-exp-imp</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gente-de-campo" class="icono-arg-gente-de-campo icono-4x text-gray" data-clipboard-text="icono-arg-gente-de-campo"></i>
                        <br>
                        <p class="name m-b-0">gente-de-campo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-geolocalizacion" class="icono-arg-geolocalizacion icono-4x text-gray" data-clipboard-text="icono-arg-geolocalizacion"></i>
                        <br>
                        <p class="name m-b-0">geolocalizacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-geolocalizacion-automotores" class="icono-arg-geolocalizacion-automotores icono-4x text-gray" data-clipboard-text="icono-arg-geolocalizacion-automotores"></i>
                        <br>
                        <p class="name m-b-0">geolocalizacion-automotores</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-gestion-de-salud" class="icono-arg-gestion-de-salud icono-4x text-gray" data-clipboard-text="icono-arg-gestion-de-salud"></i>
                        <br>
                        <p class="name m-b-0">gestion-de-salud</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-glaciar" class="icono-arg-glaciar icono-4x text-gray" data-clipboard-text="icono-arg-glaciar"></i>
                        <br>
                        <p class="name m-b-0">glaciar</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-glaciar-2" class="icono-arg-glaciar-2 icono-4x text-gray" data-clipboard-text="icono-arg-glaciar-2"></i>
                        <br>
                        <p class="name m-b-0">glaciar-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-globo-terraqueo" class="icono-arg-globo-terraqueo icono-4x text-gray" data-clipboard-text="icono-arg-globo-terraqueo"></i>
                        <br>
                        <p class="name m-b-0">globo-terraqueo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-graduada" class="icono-arg-graduada icono-4x text-gray" data-clipboard-text="icono-arg-graduada"></i>
                        <br>
                        <p class="name m-b-0">graduada</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-grafico-circular" class="icono-arg-grafico-circular icono-4x text-gray" data-clipboard-text="icono-arg-grafico-circular"></i>
                        <br>
                        <p class="name m-b-0">grafico-circular</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-granizo" class="icono-arg-granizo icono-4x text-gray" data-clipboard-text="icono-arg-granizo"></i>
                        <br>
                        <p class="name m-b-0">granizo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-guia-tramites" class="icono-arg-guia-tramites icono-4x text-gray" data-clipboard-text="icono-arg-guia-tramites"></i>
                        <br>
                        <p class="name m-b-0">guia-tramites</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-habitat" class="icono-arg-habitat icono-4x text-gray" data-clipboard-text="icono-arg-habitat"></i>
                        <br>
                        <p class="name m-b-0">habitat</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-heladera" class="icono-arg-heladera icono-4x text-gray" data-clipboard-text="icono-arg-heladera"></i>
                        <br>
                        <p class="name m-b-0">heladera</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-herramientas" class="icono-arg-herramientas icono-4x text-gray" data-clipboard-text="icono-arg-herramientas"></i>
                        <br>
                        <p class="name m-b-0">herramientas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-herramientas-colaborativas" class="icono-arg-herramientas-colaborativas icono-4x text-gray" data-clipboard-text="icono-arg-herramientas-colaborativas"></i>
                        <br>
                        <p class="name m-b-0">herramientas-colaborativas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hogar-y-familia" class="icono-arg-hogar-y-familia icono-4x text-gray" data-clipboard-text="icono-arg-hogar-y-familia"></i>
                        <br>
                        <p class="name m-b-0">hogar-y-familia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hogar-y-familia-negativo" class="icono-arg-hogar-y-familia-negativo icono-4x text-gray" data-clipboard-text="icono-arg-hogar-y-familia-negativo"></i>
                        <br>
                        <p class="name m-b-0">hogar-y-familia-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hojas" class="icono-arg-hojas icono-4x text-gray" data-clipboard-text="icono-arg-hojas"></i>
                        <br>
                        <p class="name m-b-0">hojas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hombre" class="icono-arg-hombre icono-4x text-gray" data-clipboard-text="icono-arg-hombre"></i>
                        <br>
                        <p class="name m-b-0">hombre</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-horas-extra" class="icono-arg-horas-extra icono-4x text-gray" data-clipboard-text="icono-arg-horas-extra"></i>
                        <br>
                        <p class="name m-b-0">horas-extra</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hospital" class="icono-arg-hospital icono-4x text-gray" data-clipboard-text="icono-arg-hospital"></i>
                        <br>
                        <p class="name m-b-0">hospital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hosting" class="icono-arg-hosting icono-4x text-gray" data-clipboard-text="icono-arg-hosting"></i>
                        <br>
                        <p class="name m-b-0">hosting</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hotel-lineal" class="icono-arg-hotel-lineal icono-4x text-gray" data-clipboard-text="icono-arg-hotel-lineal"></i>
                        <br>
                        <p class="name m-b-0">hotel-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-hotel-neg" class="icono-arg-hotel-neg icono-4x text-gray" data-clipboard-text="icono-arg-hotel-neg"></i>
                        <br>
                        <p class="name m-b-0">hotel-neg</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-huerta" class="icono-arg-huerta icono-4x text-gray" data-clipboard-text="icono-arg-huerta"></i>
                        <br>
                        <p class="name m-b-0">huerta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-huevos" class="icono-arg-huevos icono-4x text-gray" data-clipboard-text="icono-arg-huevos"></i>
                        <br>
                        <p class="name m-b-0">huevos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-identidad" class="icono-arg-identidad icono-4x text-gray" data-clipboard-text="icono-arg-identidad"></i>
                        <br>
                        <p class="name m-b-0">identidad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-identidad-federada" class="icono-arg-identidad-federada icono-4x text-gray" data-clipboard-text="icono-arg-identidad-federada"></i>
                        <br>
                        <p class="name m-b-0">identidad-federada</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-incendios-forestales" class="icono-arg-incendios-forestales icono-4x text-gray" data-clipboard-text="icono-arg-incendios-forestales"></i>
                        <br>
                        <p class="name m-b-0">incendios-forestales</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-incendios-urbanos" class="icono-arg-incendios-urbanos icono-4x text-gray" data-clipboard-text="icono-arg-incendios-urbanos"></i>
                        <br>
                        <p class="name m-b-0">incendios-urbanos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-inclusion-digital" class="icono-arg-inclusion-digital icono-4x text-gray" data-clipboard-text="icono-arg-inclusion-digital"></i>
                        <br>
                        <p class="name m-b-0">inclusion-digital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-incucai" class="icono-arg-incucai icono-4x text-gray" data-clipboard-text="icono-arg-incucai"></i>
                        <br>
                        <p class="name m-b-0">incucai</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-industria" class="icono-arg-industria icono-4x text-gray" data-clipboard-text="icono-arg-industria"></i>
                        <br>
                        <p class="name m-b-0">industria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-industria-y-medioambiente" class="icono-arg-industria-y-medioambiente icono-4x text-gray" data-clipboard-text="icono-arg-industria-y-medioambiente"></i>
                        <br>
                        <p class="name m-b-0">industria-y-medioambiente</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-informacion-envio" class="icono-arg-informacion-envio icono-4x text-gray" data-clipboard-text="icono-arg-informacion-envio"></i>
                        <br>
                        <p class="name m-b-0">informacion-envio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-informes-y-estadisticas" class="icono-arg-informes-y-estadisticas icono-4x text-gray" data-clipboard-text="icono-arg-informes-y-estadisticas"></i>
                        <br>
                        <p class="name m-b-0">informes-y-estadisticas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-institucion" class="icono-arg-institucion icono-4x text-gray" data-clipboard-text="icono-arg-institucion"></i>
                        <br>
                        <p class="name m-b-0">institucion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-instructor-de-tiro" class="icono-arg-instructor-de-tiro icono-4x text-gray" data-clipboard-text="icono-arg-instructor-de-tiro"></i>
                        <br>
                        <p class="name m-b-0">instructor-de-tiro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-interes-cultural" class="icono-arg-interes-cultural icono-4x text-gray" data-clipboard-text="icono-arg-interes-cultural"></i>
                        <br>
                        <p class="name m-b-0">interes-cultural</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-inundaciones" class="icono-arg-inundaciones icono-4x text-gray" data-clipboard-text="icono-arg-inundaciones"></i>
                        <br>
                        <p class="name m-b-0">inundaciones</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-investigaciones-famacologicas" class="icono-arg-investigaciones-famacologicas icono-4x text-gray" data-clipboard-text="icono-arg-investigaciones-famacologicas"></i>
                        <br>
                        <p class="name m-b-0">investigaciones-famacologicas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-jaguarete" class="icono-arg-jaguarete icono-4x text-gray" data-clipboard-text="icono-arg-jaguarete"></i>
                        <br>
                        <p class="name m-b-0">jaguarete</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-jubilado" class="icono-arg-jubilado icono-4x text-gray" data-clipboard-text="icono-arg-jubilado"></i>
                        <br>
                        <p class="name m-b-0">jubilado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-juegos" class="icono-arg-juegos icono-4x text-gray" data-clipboard-text="icono-arg-juegos"></i>
                        <br>
                        <p class="name m-b-0">juegos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-justicia" class="icono-arg-justicia icono-4x text-gray" data-clipboard-text="icono-arg-justicia"></i>
                        <br>
                        <p class="name m-b-0">justicia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-justicia-negativo" class="icono-arg-justicia-negativo icono-4x text-gray" data-clipboard-text="icono-arg-justicia-negativo"></i>
                        <br>
                        <p class="name m-b-0">justicia-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lactancia" class="icono-arg-lactancia icono-4x text-gray" data-clipboard-text="icono-arg-lactancia"></i>
                        <br>
                        <p class="name m-b-0">lactancia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lampara" class="icono-arg-lampara icono-4x text-gray" data-clipboard-text="icono-arg-lampara"></i>
                        <br>
                        <p class="name m-b-0">lampara</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lampara-bajo-consumo" class="icono-arg-lampara-bajo-consumo icono-4x text-gray" data-clipboard-text="icono-arg-lampara-bajo-consumo"></i>
                        <br>
                        <p class="name m-b-0">lampara-bajo-consumo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lampara-hoja" class="icono-arg-lampara-hoja icono-4x text-gray" data-clipboard-text="icono-arg-lampara-hoja"></i>
                        <br>
                        <p class="name m-b-0">lampara-hoja</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lavar-manos" class="icono-arg-lavar-manos icono-4x text-gray" data-clipboard-text="icono-arg-lavar-manos"></i>
                        <br>
                        <p class="name m-b-0">lavar-manos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lavarropas" class="icono-arg-lavarropas icono-4x text-gray" data-clipboard-text="icono-arg-lavarropas"></i>
                        <br>
                        <p class="name m-b-0">lavarropas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lazo-lineal" class="icono-arg-lazo-lineal icono-4x text-gray" data-clipboard-text="icono-arg-lazo-lineal"></i>
                        <br>
                        <p class="name m-b-0">lazo-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lazo-negativo" class="icono-arg-lazo-negativo icono-4x text-gray" data-clipboard-text="icono-arg-lazo-negativo"></i>
                        <br>
                        <p class="name m-b-0">lazo-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lechuga" class="icono-arg-lechuga icono-4x text-gray" data-clipboard-text="icono-arg-lechuga"></i>
                        <br>
                        <p class="name m-b-0">lechuga</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-libre-de-gluten" class="icono-arg-libre-de-gluten icono-4x text-gray" data-clipboard-text="icono-arg-libre-de-gluten"></i>
                        <br>
                        <p class="name m-b-0">libre-de-gluten</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-licencia-valida" class="icono-arg-licencia-valida icono-4x text-gray" data-clipboard-text="icono-arg-licencia-valida"></i>
                        <br>
                        <p class="name m-b-0">licencia-valida</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-licencia-valida-negativo" class="icono-arg-licencia-valida-negativo icono-4x text-gray" data-clipboard-text="icono-arg-licencia-valida-negativo"></i>
                        <br>
                        <p class="name m-b-0">licencia-valida-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lluvias" class="icono-arg-lluvias icono-4x text-gray" data-clipboard-text="icono-arg-lluvias"></i>
                        <br>
                        <p class="name m-b-0">lluvias</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lluvias-intensas" class="icono-arg-lluvias-intensas icono-4x text-gray" data-clipboard-text="icono-arg-lluvias-intensas"></i>
                        <br>
                        <p class="name m-b-0">lluvias-intensas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-login" class="icono-arg-login icono-4x text-gray" data-clipboard-text="icono-arg-login"></i>
                        <br>
                        <p class="name m-b-0">login</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-login-social" class="icono-arg-login-social icono-4x text-gray" data-clipboard-text="icono-arg-login-social"></i>
                        <br>
                        <p class="name m-b-0">login-social</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-lupa" class="icono-arg-lupa icono-4x text-gray" data-clipboard-text="icono-arg-lupa"></i>
                        <br>
                        <p class="name m-b-0">lupa</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-magnetometria" class="icono-arg-magnetometria icono-4x text-gray" data-clipboard-text="icono-arg-magnetometria"></i>
                        <br>
                        <p class="name m-b-0">magnetometria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mail-1" class="icono-arg-mail-1 icono-4x text-gray" data-clipboard-text="icono-arg-mail-1"></i>
                        <br>
                        <p class="name m-b-0">mail-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mail-2" class="icono-arg-mail-2 icono-4x text-gray" data-clipboard-text="icono-arg-mail-2"></i>
                        <br>
                        <p class="name m-b-0">mail-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maiz" class="icono-arg-maiz icono-4x text-gray" data-clipboard-text="icono-arg-maiz"></i>
                        <br>
                        <p class="name m-b-0">maiz</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maletin-check" class="icono-arg-maletin-check icono-4x text-gray" data-clipboard-text="icono-arg-maletin-check"></i>
                        <br>
                        <p class="name m-b-0">maletin-check</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maletin-check-negativo" class="icono-arg-maletin-check-negativo icono-4x text-gray" data-clipboard-text="icono-arg-maletin-check-negativo"></i>
                        <br>
                        <p class="name m-b-0">maletin-check-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maletin-medico-1" class="icono-arg-maletin-medico-1 icono-4x text-gray" data-clipboard-text="icono-arg-maletin-medico-1"></i>
                        <br>
                        <p class="name m-b-0">maletin-medico-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maletin-medico-2" class="icono-arg-maletin-medico-2 icono-4x text-gray" data-clipboard-text="icono-arg-maletin-medico-2"></i>
                        <br>
                        <p class="name m-b-0">maletin-medico-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mano" class="icono-arg-mano icono-4x text-gray" data-clipboard-text="icono-arg-mano"></i>
                        <br>
                        <p class="name m-b-0">mano</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mano-alt" class="icono-arg-mano-alt icono-4x text-gray" data-clipboard-text="icono-arg-mano-alt"></i>
                        <br>
                        <p class="name m-b-0">mano-alt</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-manzana" class="icono-arg-manzana icono-4x text-gray" data-clipboard-text="icono-arg-manzana"></i>
                        <br>
                        <p class="name m-b-0">manzana</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mapa-2" class="icono-arg-mapa-2 icono-4x text-gray" data-clipboard-text="icono-arg-mapa-2"></i>
                        <br>
                        <p class="name m-b-0">mapa-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mapa-2-negativo" class="icono-arg-mapa-2-negativo icono-4x text-gray" data-clipboard-text="icono-arg-mapa-2-negativo"></i>
                        <br>
                        <p class="name m-b-0">mapa-2-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mapa-argentina" class="icono-arg-mapa-argentina icono-4x text-gray" data-clipboard-text="icono-arg-mapa-argentina"></i>
                        <br>
                        <p class="name m-b-0">mapa-argentina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-maquina-agricola" class="icono-arg-maquina-agricola icono-4x text-gray" data-clipboard-text="icono-arg-maquina-agricola"></i>
                        <br>
                        <p class="name m-b-0">maquina-agricola</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-marcador-centro-de-salud-1" class="icono-arg-marcador-centro-de-salud-1 icono-4x text-gray" data-clipboard-text="icono-arg-marcador-centro-de-salud-1"></i>
                        <br>
                        <p class="name m-b-0">marcador-centro-de-salud-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-marcador-centro-de-salud-2" class="icono-arg-marcador-centro-de-salud-2 icono-4x text-gray" data-clipboard-text="icono-arg-marcador-centro-de-salud-2"></i>
                        <br>
                        <p class="name m-b-0">marcador-centro-de-salud-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-marcador-temporal" class="icono-arg-marcador-temporal icono-4x text-gray" data-clipboard-text="icono-arg-marcador-temporal"></i>
                        <br>
                        <p class="name m-b-0">marcador-temporal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-marcador-ubicacion-1" class="icono-arg-marcador-ubicacion-1 icono-4x text-gray" data-clipboard-text="icono-arg-marcador-ubicacion-1"></i>
                        <br>
                        <p class="name m-b-0">marcador-ubicacion-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-marcador-ubicacion-2" class="icono-arg-marcador-ubicacion-2 icono-4x text-gray" data-clipboard-text="icono-arg-marcador-ubicacion-2"></i>
                        <br>
                        <p class="name m-b-0">marcador-ubicacion-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-martillo" class="icono-arg-martillo icono-4x text-gray" data-clipboard-text="icono-arg-martillo"></i>
                        <br>
                        <p class="name m-b-0">martillo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-martillo-negativo" class="icono-arg-martillo-negativo icono-4x text-gray" data-clipboard-text="icono-arg-martillo-negativo"></i>
                        <br>
                        <p class="name m-b-0">martillo-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mecedora" class="icono-arg-mecedora icono-4x text-gray" data-clipboard-text="icono-arg-mecedora"></i>
                        <br>
                        <p class="name m-b-0">mecedora</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medicamentos" class="icono-arg-medicamentos icono-4x text-gray" data-clipboard-text="icono-arg-medicamentos"></i>
                        <br>
                        <p class="name m-b-0">medicamentos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medicamentos-2" class="icono-arg-medicamentos-2 icono-4x text-gray" data-clipboard-text="icono-arg-medicamentos-2"></i>
                        <br>
                        <p class="name m-b-0">medicamentos-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medicamentos-negativo" class="icono-arg-medicamentos-negativo icono-4x text-gray" data-clipboard-text="icono-arg-medicamentos-negativo"></i>
                        <br>
                        <p class="name m-b-0">medicamentos-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medicina" class="icono-arg-medicina icono-4x text-gray" data-clipboard-text="icono-arg-medicina"></i>
                        <br>
                        <p class="name m-b-0">medicina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medicina-2" class="icono-arg-medicina-2 icono-4x text-gray" data-clipboard-text="icono-arg-medicina-2"></i>
                        <br>
                        <p class="name m-b-0">medicina-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medico" class="icono-arg-medico icono-4x text-gray" data-clipboard-text="icono-arg-medico"></i>
                        <br>
                        <p class="name m-b-0">medico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-medula-osea" class="icono-arg-medula-osea icono-4x text-gray" data-clipboard-text="icono-arg-medula-osea"></i>
                        <br>
                        <p class="name m-b-0">medula-osea</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mi-argentina" class="icono-arg-mi-argentina icono-4x text-gray" data-clipboard-text="icono-arg-mi-argentina"></i>
                        <br>
                        <p class="name m-b-0">mi-argentina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-micro" class="icono-arg-micro icono-4x text-gray" data-clipboard-text="icono-arg-micro"></i>
                        <br>
                        <p class="name m-b-0">micro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-microscopio" class="icono-arg-microscopio icono-4x text-gray" data-clipboard-text="icono-arg-microscopio"></i>
                        <br>
                        <p class="name m-b-0">microscopio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-molino" class="icono-arg-molino icono-4x text-gray" data-clipboard-text="icono-arg-molino"></i>
                        <br>
                        <p class="name m-b-0">molino</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-monedas" class="icono-arg-monedas icono-4x text-gray" data-clipboard-text="icono-arg-monedas"></i>
                        <br>
                        <p class="name m-b-0">monedas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mosquito" class="icono-arg-mosquito icono-4x text-gray" data-clipboard-text="icono-arg-mosquito"></i>
                        <br>
                        <p class="name m-b-0">mosquito</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-moticicleta-lineal" class="icono-arg-moticicleta-lineal icono-4x text-gray" data-clipboard-text="icono-arg-moticicleta-lineal"></i>
                        <br>
                        <p class="name m-b-0">moticicleta-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-motocicleta-negativo" class="icono-arg-motocicleta-negativo icono-4x text-gray" data-clipboard-text="icono-arg-motocicleta-negativo"></i>
                        <br>
                        <p class="name m-b-0">motocicleta-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mujer" class="icono-arg-mujer icono-4x text-gray" data-clipboard-text="icono-arg-mujer"></i>
                        <br>
                        <p class="name m-b-0">mujer</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-mundo" class="icono-arg-mundo icono-4x text-gray" data-clipboard-text="icono-arg-mundo"></i>
                        <br>
                        <p class="name m-b-0">mundo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-museo" class="icono-arg-museo icono-4x text-gray" data-clipboard-text="icono-arg-museo"></i>
                        <br>
                        <p class="name m-b-0">museo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-natacion" class="icono-arg-natacion icono-4x text-gray" data-clipboard-text="icono-arg-natacion"></i>
                        <br>
                        <p class="name m-b-0">natacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nevadas" class="icono-arg-nevadas icono-4x text-gray" data-clipboard-text="icono-arg-nevadas"></i>
                        <br>
                        <p class="name m-b-0">nevadas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nido" class="icono-arg-nido icono-4x text-gray" data-clipboard-text="icono-arg-nido"></i>
                        <br>
                        <p class="name m-b-0">nido</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-niebla" class="icono-arg-niebla icono-4x text-gray" data-clipboard-text="icono-arg-niebla"></i>
                        <br>
                        <p class="name m-b-0">niebla</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-notebook" class="icono-arg-notebook icono-4x text-gray" data-clipboard-text="icono-arg-notebook"></i>
                        <br>
                        <p class="name m-b-0">notebook</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-notificaciones" class="icono-arg-notificaciones icono-4x text-gray" data-clipboard-text="icono-arg-notificaciones"></i>
                        <br>
                        <p class="name m-b-0">notificaciones</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nube" class="icono-arg-nube icono-4x text-gray" data-clipboard-text="icono-arg-nube"></i>
                        <br>
                        <p class="name m-b-0">nube</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nube-bajar-datos" class="icono-arg-nube-bajar-datos icono-4x text-gray" data-clipboard-text="icono-arg-nube-bajar-datos"></i>
                        <br>
                        <p class="name m-b-0">nube-bajar-datos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nube-seguridad" class="icono-arg-nube-seguridad icono-4x text-gray" data-clipboard-text="icono-arg-nube-seguridad"></i>
                        <br>
                        <p class="name m-b-0">nube-seguridad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nube-subir-datos" class="icono-arg-nube-subir-datos icono-4x text-gray" data-clipboard-text="icono-arg-nube-subir-datos"></i>
                        <br>
                        <p class="name m-b-0">nube-subir-datos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-nuclear" class="icono-arg-nuclear icono-4x text-gray" data-clipboard-text="icono-arg-nuclear"></i>
                        <br>
                        <p class="name m-b-0">nuclear</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ola-de-calor" class="icono-arg-ola-de-calor icono-4x text-gray" data-clipboard-text="icono-arg-ola-de-calor"></i>
                        <br>
                        <p class="name m-b-0">ola-de-calor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ola-polar" class="icono-arg-ola-polar icono-4x text-gray" data-clipboard-text="icono-arg-ola-polar"></i>
                        <br>
                        <p class="name m-b-0">ola-polar</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-olas" class="icono-arg-olas icono-4x text-gray" data-clipboard-text="icono-arg-olas"></i>
                        <br>
                        <p class="name m-b-0">olas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-olla" class="icono-arg-olla icono-4x text-gray" data-clipboard-text="icono-arg-olla"></i>
                        <br>
                        <p class="name m-b-0">olla</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-olla-fuego" class="icono-arg-olla-fuego icono-4x text-gray" data-clipboard-text="icono-arg-olla-fuego"></i>
                        <br>
                        <p class="name m-b-0">olla-fuego</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-olla-fuego-negativo" class="icono-arg-olla-fuego-negativo icono-4x text-gray" data-clipboard-text="icono-arg-olla-fuego-negativo"></i>
                        <br>
                        <p class="name m-b-0">olla-fuego-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ombu" class="icono-arg-ombu icono-4x text-gray" data-clipboard-text="icono-arg-ombu"></i>
                        <br>
                        <p class="name m-b-0">ombu</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-open-id" class="icono-arg-open-id icono-4x text-gray" data-clipboard-text="icono-arg-open-id"></i>
                        <br>
                        <p class="name m-b-0">open-id</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-operativo-sanitario-1" class="icono-arg-operativo-sanitario-1 icono-4x text-gray" data-clipboard-text="icono-arg-operativo-sanitario-1"></i>
                        <br>
                        <p class="name m-b-0">operativo-sanitario-1</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-operativo-sanitario-2" class="icono-arg-operativo-sanitario-2 icono-4x text-gray" data-clipboard-text="icono-arg-operativo-sanitario-2"></i>
                        <br>
                        <p class="name m-b-0">operativo-sanitario-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pago" class="icono-arg-pago icono-4x text-gray" data-clipboard-text="icono-arg-pago"></i>
                        <br>
                        <p class="name m-b-0">pago</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paisaje-hidrico" class="icono-arg-paisaje-hidrico icono-4x text-gray" data-clipboard-text="icono-arg-paisaje-hidrico"></i>
                        <br>
                        <p class="name m-b-0">paisaje-hidrico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paisaje-hidrico-2" class="icono-arg-paisaje-hidrico-2 icono-4x text-gray" data-clipboard-text="icono-arg-paisaje-hidrico-2"></i>
                        <br>
                        <p class="name m-b-0">paisaje-hidrico-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paisaje-urbano-2" class="icono-arg-paisaje-urbano-2 icono-4x text-gray" data-clipboard-text="icono-arg-paisaje-urbano-2"></i>
                        <br>
                        <p class="name m-b-0">paisaje-urbano-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paisaje-urbano-3" class="icono-arg-paisaje-urbano-3 icono-4x text-gray" data-clipboard-text="icono-arg-paisaje-urbano-3"></i>
                        <br>
                        <p class="name m-b-0">paisaje-urbano-3</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paisaje-urbano-4" class="icono-arg-paisaje-urbano-4 icono-4x text-gray" data-clipboard-text="icono-arg-paisaje-urbano-4"></i>
                        <br>
                        <p class="name m-b-0">paisaje-urbano-4</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paloma" class="icono-arg-paloma icono-4x text-gray" data-clipboard-text="icono-arg-paloma"></i>
                        <br>
                        <p class="name m-b-0">paloma</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pareja" class="icono-arg-pareja icono-4x text-gray" data-clipboard-text="icono-arg-pareja"></i>
                        <br>
                        <p class="name m-b-0">pareja</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-participacion" class="icono-arg-participacion icono-4x text-gray" data-clipboard-text="icono-arg-participacion"></i>
                        <br>
                        <p class="name m-b-0">participacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-participacion-ciudadana" class="icono-arg-participacion-ciudadana icono-4x text-gray" data-clipboard-text="icono-arg-participacion-ciudadana"></i>
                        <br>
                        <p class="name m-b-0">participacion-ciudadana</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pasaje-avion" class="icono-arg-pasaje-avion icono-4x text-gray" data-clipboard-text="icono-arg-pasaje-avion"></i>
                        <br>
                        <p class="name m-b-0">pasaje-avion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pasaporte" class="icono-arg-pasaporte icono-4x text-gray" data-clipboard-text="icono-arg-pasaporte"></i>
                        <br>
                        <p class="name m-b-0">pasaporte</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-paso-a-nivel" class="icono-arg-paso-a-nivel icono-4x text-gray" data-clipboard-text="icono-arg-paso-a-nivel"></i>
                        <br>
                        <p class="name m-b-0">paso-a-nivel</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pastizales" class="icono-arg-pastizales icono-4x text-gray" data-clipboard-text="icono-arg-pastizales"></i>
                        <br>
                        <p class="name m-b-0">pastizales</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pd-punto-digital" class="icono-arg-pd-punto-digital icono-4x text-gray" data-clipboard-text="icono-arg-pd-punto-digital"></i>
                        <br>
                        <p class="name m-b-0">pd-punto-digital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pd-wifi" class="icono-arg-pd-wifi icono-4x text-gray" data-clipboard-text="icono-arg-pd-wifi"></i>
                        <br>
                        <p class="name m-b-0">pd-wifi</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pera" class="icono-arg-pera icono-4x text-gray" data-clipboard-text="icono-arg-pera"></i>
                        <br>
                        <p class="name m-b-0">pera</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pesca-con-red" class="icono-arg-pesca-con-red icono-4x text-gray" data-clipboard-text="icono-arg-pesca-con-red"></i>
                        <br>
                        <p class="name m-b-0">pesca-con-red</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pescado" class="icono-arg-pescado icono-4x text-gray" data-clipboard-text="icono-arg-pescado"></i>
                        <br>
                        <p class="name m-b-0">pescado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pescador" class="icono-arg-pescador icono-4x text-gray" data-clipboard-text="icono-arg-pescador"></i>
                        <br>
                        <p class="name m-b-0">pescador</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pesos" class="icono-arg-pesos icono-4x text-gray" data-clipboard-text="icono-arg-pesos"></i>
                        <br>
                        <p class="name m-b-0">pesos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-petroleo" class="icono-arg-petroleo icono-4x text-gray" data-clipboard-text="icono-arg-petroleo"></i>
                        <br>
                        <p class="name m-b-0">petroleo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-piloto" class="icono-arg-piloto icono-4x text-gray" data-clipboard-text="icono-arg-piloto"></i>
                        <br>
                        <p class="name m-b-0">piloto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-plancha" class="icono-arg-plancha icono-4x text-gray" data-clipboard-text="icono-arg-plancha"></i>
                        <br>
                        <p class="name m-b-0">plancha</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-planta-exotica" class="icono-arg-planta-exotica icono-4x text-gray" data-clipboard-text="icono-arg-planta-exotica"></i>
                        <br>
                        <p class="name m-b-0">planta-exotica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-planta-exotica-2" class="icono-arg-planta-exotica-2 icono-4x text-gray" data-clipboard-text="icono-arg-planta-exotica-2"></i>
                        <br>
                        <p class="name m-b-0">planta-exotica-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-plataforma-offshore" class="icono-arg-plataforma-offshore icono-4x text-gray" data-clipboard-text="icono-arg-plataforma-offshore"></i>
                        <br>
                        <p class="name m-b-0">plataforma-offshore</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-plataforma-offshore-2" class="icono-arg-plataforma-offshore-2 icono-4x text-gray" data-clipboard-text="icono-arg-plataforma-offshore-2"></i>
                        <br>
                        <p class="name m-b-0">plataforma-offshore-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-poligono-de-tiro" class="icono-arg-poligono-de-tiro icono-4x text-gray" data-clipboard-text="icono-arg-poligono-de-tiro"></i>
                        <br>
                        <p class="name m-b-0">poligono-de-tiro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-portal-educativo" class="icono-arg-portal-educativo icono-4x text-gray" data-clipboard-text="icono-arg-portal-educativo"></i>
                        <br>
                        <p class="name m-b-0">portal-educativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-preservativo" class="icono-arg-preservativo icono-4x text-gray" data-clipboard-text="icono-arg-preservativo"></i>
                        <br>
                        <p class="name m-b-0">preservativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-presupuesto" class="icono-arg-presupuesto icono-4x text-gray" data-clipboard-text="icono-arg-presupuesto"></i>
                        <br>
                        <p class="name m-b-0">presupuesto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-probeta" class="icono-arg-probeta icono-4x text-gray" data-clipboard-text="icono-arg-probeta"></i>
                        <br>
                        <p class="name m-b-0">probeta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-probeta-hoja" class="icono-arg-probeta-hoja icono-4x text-gray" data-clipboard-text="icono-arg-probeta-hoja"></i>
                        <br>
                        <p class="name m-b-0">probeta-hoja</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-profesional-nuclear" class="icono-arg-profesional-nuclear icono-4x text-gray" data-clipboard-text="icono-arg-profesional-nuclear"></i>
                        <br>
                        <p class="name m-b-0">profesional-nuclear</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-profesionales-y-empresas" class="icono-arg-profesionales-y-empresas icono-4x text-gray" data-clipboard-text="icono-arg-profesionales-y-empresas"></i>
                        <br>
                        <p class="name m-b-0">profesionales-y-empresas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-prohibido-celular" class="icono-arg-prohibido-celular icono-4x text-gray" data-clipboard-text="icono-arg-prohibido-celular"></i>
                        <br>
                        <p class="name m-b-0">prohibido-celular</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-promocion-comercial" class="icono-arg-promocion-comercial icono-4x text-gray" data-clipboard-text="icono-arg-promocion-comercial"></i>
                        <br>
                        <p class="name m-b-0">promocion-comercial</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-prueba" class="icono-arg-prueba icono-4x text-gray" data-clipboard-text="icono-arg-prueba"></i>
                        <br>
                        <p class="name m-b-0">prueba</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-pueblos-originarios" class="icono-arg-pueblos-originarios icono-4x text-gray" data-clipboard-text="icono-arg-pueblos-originarios"></i>
                        <br>
                        <p class="name m-b-0">pueblos-originarios</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-radar-meteorologico" class="icono-arg-radar-meteorologico icono-4x text-gray" data-clipboard-text="icono-arg-radar-meteorologico"></i>
                        <br>
                        <p class="name m-b-0">radar-meteorologico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reactor-ra6" class="icono-arg-reactor-ra6 icono-4x text-gray" data-clipboard-text="icono-arg-reactor-ra6"></i>
                        <br>
                        <p class="name m-b-0">reactor-ra6</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reciclaje" class="icono-arg-reciclaje icono-4x text-gray" data-clipboard-text="icono-arg-reciclaje"></i>
                        <br>
                        <p class="name m-b-0">reciclaje</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reciclaje-basura" class="icono-arg-reciclaje-basura icono-4x text-gray" data-clipboard-text="icono-arg-reciclaje-basura"></i>
                        <br>
                        <p class="name m-b-0">reciclaje-basura</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reclamo-consulta" class="icono-arg-reclamo-consulta icono-4x text-gray" data-clipboard-text="icono-arg-reclamo-consulta"></i>
                        <br>
                        <p class="name m-b-0">reclamo-consulta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reclamo-demora" class="icono-arg-reclamo-demora icono-4x text-gray" data-clipboard-text="icono-arg-reclamo-demora"></i>
                        <br>
                        <p class="name m-b-0">reclamo-demora</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-recursos-origen" class="icono-arg-recursos-origen icono-4x text-gray" data-clipboard-text="icono-arg-recursos-origen"></i>
                        <br>
                        <p class="name m-b-0">recursos-origen</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-registro-donantes-medula-osea" class="icono-arg-registro-donantes-medula-osea icono-4x text-gray" data-clipboard-text="icono-arg-registro-donantes-medula-osea"></i>
                        <br>
                        <p class="name m-b-0">registro-donantes-medula-osea</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-reloj" class="icono-arg-reloj icono-4x text-gray" data-clipboard-text="icono-arg-reloj"></i>
                        <br>
                        <p class="name m-b-0">reloj</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-represa-hidroelectrica" class="icono-arg-represa-hidroelectrica icono-4x text-gray" data-clipboard-text="icono-arg-represa-hidroelectrica"></i>
                        <br>
                        <p class="name m-b-0">represa-hidroelectrica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-residuos-peligrosos-transporte" class="icono-arg-residuos-peligrosos-transporte icono-4x text-gray" data-clipboard-text="icono-arg-residuos-peligrosos-transporte"></i>
                        <br>
                        <p class="name m-b-0">residuos-peligrosos-transporte</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-riego-produccion" class="icono-arg-riego-produccion icono-4x text-gray" data-clipboard-text="icono-arg-riego-produccion"></i>
                        <br>
                        <p class="name m-b-0">riego-produccion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-rocas" class="icono-arg-rocas icono-4x text-gray" data-clipboard-text="icono-arg-rocas"></i>
                        <br>
                        <p class="name m-b-0">rocas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-ruta" class="icono-arg-ruta icono-4x text-gray" data-clipboard-text="icono-arg-ruta"></i>
                        <br>
                        <p class="name m-b-0">ruta</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-salero" class="icono-arg-salero icono-4x text-gray" data-clipboard-text="icono-arg-salero"></i>
                        <br>
                        <p class="name m-b-0">salero</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-salero-prohibido" class="icono-arg-salero-prohibido icono-4x text-gray" data-clipboard-text="icono-arg-salero-prohibido"></i>
                        <br>
                        <p class="name m-b-0">salero-prohibido</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-salud-escudo" class="icono-arg-salud-escudo icono-4x text-gray" data-clipboard-text="icono-arg-salud-escudo"></i>
                        <br>
                        <p class="name m-b-0">salud-escudo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-salud-reproductiva-y-sexual" class="icono-arg-salud-reproductiva-y-sexual icono-4x text-gray" data-clipboard-text="icono-arg-salud-reproductiva-y-sexual"></i>
                        <br>
                        <p class="name m-b-0">salud-reproductiva-y-sexual</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sanidad-animal" class="icono-arg-sanidad-animal icono-4x text-gray" data-clipboard-text="icono-arg-sanidad-animal"></i>
                        <br>
                        <p class="name m-b-0">sanidad-animal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-satelital" class="icono-arg-satelital icono-4x text-gray" data-clipboard-text="icono-arg-satelital"></i>
                        <br>
                        <p class="name m-b-0">satelital</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-satelite" class="icono-arg-satelite icono-4x text-gray" data-clipboard-text="icono-arg-satelite"></i>
                        <br>
                        <p class="name m-b-0">satelite</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad" class="icono-arg-seguridad icono-4x text-gray" data-clipboard-text="icono-arg-seguridad"></i>
                        <br>
                        <p class="name m-b-0">seguridad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-bebe-lineal" class="icono-arg-seguridad-bebe-lineal icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-bebe-lineal"></i>
                        <br>
                        <p class="name m-b-0">seguridad-bebe-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-bebe-negativo" class="icono-arg-seguridad-bebe-negativo icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-bebe-negativo"></i>
                        <br>
                        <p class="name m-b-0">seguridad-bebe-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-datos" class="icono-arg-seguridad-datos icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-datos"></i>
                        <br>
                        <p class="name m-b-0">seguridad-datos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-datos-2" class="icono-arg-seguridad-datos-2 icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-datos-2"></i>
                        <br>
                        <p class="name m-b-0">seguridad-datos-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-documento" class="icono-arg-seguridad-documento icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-documento"></i>
                        <br>
                        <p class="name m-b-0">seguridad-documento</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-seguridad-red" class="icono-arg-seguridad-red icono-4x text-gray" data-clipboard-text="icono-arg-seguridad-red"></i>
                        <br>
                        <p class="name m-b-0">seguridad-red</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-semaforo" class="icono-arg-semaforo icono-4x text-gray" data-clipboard-text="icono-arg-semaforo"></i>
                        <br>
                        <p class="name m-b-0">semaforo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-serpiente" class="icono-arg-serpiente icono-4x text-gray" data-clipboard-text="icono-arg-serpiente"></i>
                        <br>
                        <p class="name m-b-0">serpiente</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-servicios" class="icono-arg-servicios icono-4x text-gray" data-clipboard-text="icono-arg-servicios"></i>
                        <br>
                        <p class="name m-b-0">servicios</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-servidor" class="icono-arg-servidor icono-4x text-gray" data-clipboard-text="icono-arg-servidor"></i>
                        <br>
                        <p class="name m-b-0">servidor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-silla-seguridad-auto" class="icono-arg-silla-seguridad-auto icono-4x text-gray" data-clipboard-text="icono-arg-silla-seguridad-auto"></i>
                        <br>
                        <p class="name m-b-0">silla-seguridad-auto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-silla-seguridad-auto-negativo" class="icono-arg-silla-seguridad-auto-negativo icono-4x text-gray" data-clipboard-text="icono-arg-silla-seguridad-auto-negativo"></i>
                        <br>
                        <p class="name m-b-0">silla-seguridad-auto-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sin-mosquito" class="icono-arg-sin-mosquito icono-4x text-gray" data-clipboard-text="icono-arg-sin-mosquito"></i>
                        <br>
                        <p class="name m-b-0">sin-mosquito</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-social" class="icono-arg-social icono-4x text-gray" data-clipboard-text="icono-arg-social"></i>
                        <br>
                        <p class="name m-b-0">social</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sol" class="icono-arg-sol icono-4x text-gray" data-clipboard-text="icono-arg-sol"></i>
                        <br>
                        <p class="name m-b-0">sol</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sol-patrio" class="icono-arg-sol-patrio icono-4x text-gray" data-clipboard-text="icono-arg-sol-patrio"></i>
                        <br>
                        <p class="name m-b-0">sol-patrio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-solidaridad" class="icono-arg-solidaridad icono-4x text-gray" data-clipboard-text="icono-arg-solidaridad"></i>
                        <br>
                        <p class="name m-b-0">solidaridad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-soluciones-habitacionales" class="icono-arg-soluciones-habitacionales icono-4x text-gray" data-clipboard-text="icono-arg-soluciones-habitacionales"></i>
                        <br>
                        <p class="name m-b-0">soluciones-habitacionales</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sube" class="icono-arg-sube icono-4x text-gray" data-clipboard-text="icono-arg-sube"></i>
                        <br>
                        <p class="name m-b-0">sube</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-subir-escaleras" class="icono-arg-subir-escaleras icono-4x text-gray" data-clipboard-text="icono-arg-subir-escaleras"></i>
                        <br>
                        <p class="name m-b-0">subir-escaleras</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-surtidor" class="icono-arg-surtidor icono-4x text-gray" data-clipboard-text="icono-arg-surtidor"></i>
                        <br>
                        <p class="name m-b-0">surtidor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-sustancias-ok" class="icono-arg-sustancias-ok icono-4x text-gray" data-clipboard-text="icono-arg-sustancias-ok"></i>
                        <br>
                        <p class="name m-b-0">sustancias-ok</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tabla-de-datos" class="icono-arg-tabla-de-datos icono-4x text-gray" data-clipboard-text="icono-arg-tabla-de-datos"></i>
                        <br>
                        <p class="name m-b-0">tabla-de-datos</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tablero-gestion" class="icono-arg-tablero-gestion icono-4x text-gray" data-clipboard-text="icono-arg-tablero-gestion"></i>
                        <br>
                        <p class="name m-b-0">tablero-gestion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tambo" class="icono-arg-tambo icono-4x text-gray" data-clipboard-text="icono-arg-tambo"></i>
                        <br>
                        <p class="name m-b-0">tambo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-teatro" class="icono-arg-teatro icono-4x text-gray" data-clipboard-text="icono-arg-teatro"></i>
                        <br>
                        <p class="name m-b-0">teatro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-teatro-archivo" class="icono-arg-teatro-archivo icono-4x text-gray" data-clipboard-text="icono-arg-teatro-archivo"></i>
                        <br>
                        <p class="name m-b-0">teatro-archivo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tecnovigilancia" class="icono-arg-tecnovigilancia icono-4x text-gray" data-clipboard-text="icono-arg-tecnovigilancia"></i>
                        <br>
                        <p class="name m-b-0">tecnovigilancia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-telefono-lineal" class="icono-arg-telefono-lineal icono-4x text-gray" data-clipboard-text="icono-arg-telefono-lineal"></i>
                        <br>
                        <p class="name m-b-0">telefono-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-telefono-neg" class="icono-arg-telefono-neg icono-4x text-gray" data-clipboard-text="icono-arg-telefono-neg"></i>
                        <br>
                        <p class="name m-b-0">telefono-neg</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-televisor" class="icono-arg-televisor icono-4x text-gray" data-clipboard-text="icono-arg-televisor"></i>
                        <br>
                        <p class="name m-b-0">televisor</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-temporal-frio" class="icono-arg-temporal-frio icono-4x text-gray" data-clipboard-text="icono-arg-temporal-frio"></i>
                        <br>
                        <p class="name m-b-0">temporal-frio</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tensiometro" class="icono-arg-tensiometro icono-4x text-gray" data-clipboard-text="icono-arg-tensiometro"></i>
                        <br>
                        <p class="name m-b-0">tensiometro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-termometro" class="icono-arg-termometro icono-4x text-gray" data-clipboard-text="icono-arg-termometro"></i>
                        <br>
                        <p class="name m-b-0">termometro</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-termotanque" class="icono-arg-termotanque icono-4x text-gray" data-clipboard-text="icono-arg-termotanque"></i>
                        <br>
                        <p class="name m-b-0">termotanque</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-terremoto" class="icono-arg-terremoto icono-4x text-gray" data-clipboard-text="icono-arg-terremoto"></i>
                        <br>
                        <p class="name m-b-0">terremoto</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tomate" class="icono-arg-tomate icono-4x text-gray" data-clipboard-text="icono-arg-tomate"></i>
                        <br>
                        <p class="name m-b-0">tomate</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tormentas-electricas" class="icono-arg-tormentas-electricas icono-4x text-gray" data-clipboard-text="icono-arg-tormentas-electricas"></i>
                        <br>
                        <p class="name m-b-0">tormentas-electricas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tornado" class="icono-arg-tornado icono-4x text-gray" data-clipboard-text="icono-arg-tornado"></i>
                        <br>
                        <p class="name m-b-0">tornado</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tornados-y-tormentas-electricas" class="icono-arg-tornados-y-tormentas-electricas icono-4x text-gray" data-clipboard-text="icono-arg-tornados-y-tormentas-electricas"></i>
                        <br>
                        <p class="name m-b-0">tornados-y-tormentas-electricas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-torre-electrica" class="icono-arg-torre-electrica icono-4x text-gray" data-clipboard-text="icono-arg-torre-electrica"></i>
                        <br>
                        <p class="name m-b-0">torre-electrica</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-torre-petroleo" class="icono-arg-torre-petroleo icono-4x text-gray" data-clipboard-text="icono-arg-torre-petroleo"></i>
                        <br>
                        <p class="name m-b-0">torre-petroleo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-trabajador" class="icono-arg-trabajador icono-4x text-gray" data-clipboard-text="icono-arg-trabajador"></i>
                        <br>
                        <p class="name m-b-0">trabajador</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-trabajador-temporal" class="icono-arg-trabajador-temporal icono-4x text-gray" data-clipboard-text="icono-arg-trabajador-temporal"></i>
                        <br>
                        <p class="name m-b-0">trabajador-temporal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tramite" class="icono-arg-tramite icono-4x text-gray" data-clipboard-text="icono-arg-tramite"></i>
                        <br>
                        <p class="name m-b-0">tramite</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tramite-electronico" class="icono-arg-tramite-electronico icono-4x text-gray" data-clipboard-text="icono-arg-tramite-electronico"></i>
                        <br>
                        <p class="name m-b-0">tramite-electronico</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tramite-jubilacion" class="icono-arg-tramite-jubilacion icono-4x text-gray" data-clipboard-text="icono-arg-tramite-jubilacion"></i>
                        <br>
                        <p class="name m-b-0">tramite-jubilacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tramite-negativo" class="icono-arg-tramite-negativo icono-4x text-gray" data-clipboard-text="icono-arg-tramite-negativo"></i>
                        <br>
                        <p class="name m-b-0">tramite-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-transplante-lineal" class="icono-arg-transplante-lineal icono-4x text-gray" data-clipboard-text="icono-arg-transplante-lineal"></i>
                        <br>
                        <p class="name m-b-0">transplante-lineal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-transplante-negativo" class="icono-arg-transplante-negativo icono-4x text-gray" data-clipboard-text="icono-arg-transplante-negativo"></i>
                        <br>
                        <p class="name m-b-0">transplante-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-transporte-licencia" class="icono-arg-transporte-licencia icono-4x text-gray" data-clipboard-text="icono-arg-transporte-licencia"></i>
                        <br>
                        <p class="name m-b-0">transporte-licencia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-transporte-publico-licencia" class="icono-arg-transporte-publico-licencia icono-4x text-gray" data-clipboard-text="icono-arg-transporte-publico-licencia"></i>
                        <br>
                        <p class="name m-b-0">transporte-publico-licencia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-trigo" class="icono-arg-trigo icono-4x text-gray" data-clipboard-text="icono-arg-trigo"></i>
                        <br>
                        <p class="name m-b-0">trigo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-trigo-2" class="icono-arg-trigo-2 icono-4x text-gray" data-clipboard-text="icono-arg-trigo-2"></i>
                        <br>
                        <p class="name m-b-0">trigo-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tristeza" class="icono-arg-tristeza icono-4x text-gray" data-clipboard-text="icono-arg-tristeza"></i>
                        <br>
                        <p class="name m-b-0">tristeza</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tromba-marina" class="icono-arg-tromba-marina icono-4x text-gray" data-clipboard-text="icono-arg-tromba-marina"></i>
                        <br>
                        <p class="name m-b-0">tromba-marina</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tuberias" class="icono-arg-tuberias icono-4x text-gray" data-clipboard-text="icono-arg-tuberias"></i>
                        <br>
                        <p class="name m-b-0">tuberias</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-tumba" class="icono-arg-tumba icono-4x text-gray" data-clipboard-text="icono-arg-tumba"></i>
                        <br>
                        <p class="name m-b-0">tumba</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-turno" class="icono-arg-turno icono-4x text-gray" data-clipboard-text="icono-arg-turno"></i>
                        <br>
                        <p class="name m-b-0">turno</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-turno-negativo" class="icono-arg-turno-negativo icono-4x text-gray" data-clipboard-text="icono-arg-turno-negativo"></i>
                        <br>
                        <p class="name m-b-0">turno-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-urbanizacion" class="icono-arg-urbanizacion icono-4x text-gray" data-clipboard-text="icono-arg-urbanizacion"></i>
                        <br>
                        <p class="name m-b-0">urbanizacion</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-usuarios" class="icono-arg-usuarios icono-4x text-gray" data-clipboard-text="icono-arg-usuarios"></i>
                        <br>
                        <p class="name m-b-0">usuarios</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-uvas" class="icono-arg-uvas icono-4x text-gray" data-clipboard-text="icono-arg-uvas"></i>
                        <br>
                        <p class="name m-b-0">uvas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vacuna" class="icono-arg-vacuna icono-4x text-gray" data-clipboard-text="icono-arg-vacuna"></i>
                        <br>
                        <p class="name m-b-0">vacuna</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vacunas" class="icono-arg-vacunas icono-4x text-gray" data-clipboard-text="icono-arg-vacunas"></i>
                        <br>
                        <p class="name m-b-0">vacunas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vademecum" class="icono-arg-vademecum icono-4x text-gray" data-clipboard-text="icono-arg-vademecum"></i>
                        <br>
                        <p class="name m-b-0">vademecum</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vademecum-2" class="icono-arg-vademecum-2 icono-4x text-gray" data-clipboard-text="icono-arg-vademecum-2"></i>
                        <br>
                        <p class="name m-b-0">vademecum-2</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-validacion-identidad" class="icono-arg-validacion-identidad icono-4x text-gray" data-clipboard-text="icono-arg-validacion-identidad"></i>
                        <br>
                        <p class="name m-b-0">validacion-identidad</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-validez-legal" class="icono-arg-validez-legal icono-4x text-gray" data-clipboard-text="icono-arg-validez-legal"></i>
                        <br>
                        <p class="name m-b-0">validez-legal</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-velero-con-olas" class="icono-arg-velero-con-olas icono-4x text-gray" data-clipboard-text="icono-arg-velero-con-olas"></i>
                        <br>
                        <p class="name m-b-0">velero-con-olas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-velero-sin-olas" class="icono-arg-velero-sin-olas icono-4x text-gray" data-clipboard-text="icono-arg-velero-sin-olas"></i>
                        <br>
                        <p class="name m-b-0">velero-sin-olas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vencimiento" class="icono-arg-vencimiento icono-4x text-gray" data-clipboard-text="icono-arg-vencimiento"></i>
                        <br>
                        <p class="name m-b-0">vencimiento</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vencimiento-negativo" class="icono-arg-vencimiento-negativo icono-4x text-gray" data-clipboard-text="icono-arg-vencimiento-negativo"></i>
                        <br>
                        <p class="name m-b-0">vencimiento-negativo</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-viajero" class="icono-arg-viajero icono-4x text-gray" data-clipboard-text="icono-arg-viajero"></i>
                        <br>
                        <p class="name m-b-0">viajero</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vias" class="icono-arg-vias icono-4x text-gray" data-clipboard-text="icono-arg-vias"></i>
                        <br>
                        <p class="name m-b-0">vias</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-viento-zonda" class="icono-arg-viento-zonda icono-4x text-gray" data-clipboard-text="icono-arg-viento-zonda"></i>
                        <br>
                        <p class="name m-b-0">viento-zonda</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vigilancia-alimentaria" class="icono-arg-vigilancia-alimentaria icono-4x text-gray" data-clipboard-text="icono-arg-vigilancia-alimentaria"></i>
                        <br>
                        <p class="name m-b-0">vigilancia-alimentaria</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-visa-negocios" class="icono-arg-visa-negocios icono-4x text-gray" data-clipboard-text="icono-arg-visa-negocios"></i>
                        <br>
                        <p class="name m-b-0">visa-negocios</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vivienda" class="icono-arg-vivienda icono-4x text-gray" data-clipboard-text="icono-arg-vivienda"></i>
                        <br>
                        <p class="name m-b-0">vivienda</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vivienda-credito" class="icono-arg-vivienda-credito icono-4x text-gray" data-clipboard-text="icono-arg-vivienda-credito"></i>
                        <br>
                        <p class="name m-b-0">vivienda-credito</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vivienda-familia" class="icono-arg-vivienda-familia icono-4x text-gray" data-clipboard-text="icono-arg-vivienda-familia"></i>
                        <br>
                        <p class="name m-b-0">vivienda-familia</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-vivienda-mas" class="icono-arg-vivienda-mas icono-4x text-gray" data-clipboard-text="icono-arg-vivienda-mas"></i>
                        <br>
                        <p class="name m-b-0">vivienda-mas</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-zanahorias" class="icono-arg-zanahorias icono-4x text-gray" data-clipboard-text="icono-arg-zanahorias"></i>
                        <br>
                        <p class="name m-b-0">zanahorias</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center margin-40">
                        <i id="icono-arg-zapallo" class="icono-arg-zapallo icono-4x text-gray" data-clipboard-text="icono-arg-zapallo"></i>
                        <br>
                        <p class="name m-b-0">zapallo</p>
                    </div>
            </section>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    	jQuery(document).ready(function() { 
    		jQuery(window).scroll(function() {
    			onScroll();     
    		});
    	});
    	function onScroll(event){
    		var scrollPos = jQuery(document).scrollTop();
    		if(scrollPos > 424){
    			jQuery('.sticky').css('top','15px')	
    		}else{
    			jQuery('.sticky').css('top','')	
    		}
    		jQuery('ul.sticky li a[href*=#]:not([href=#])').each(function () {
    			var currLink = jQuery(this);
    			var refElement = jQuery(currLink.attr("href"));
    			if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
	        jQuery('ul.sticky li').removeClass("active"); //added to remove active class from all a elements
	        currLink.parent().addClass("active");
	    }
	    else{
	    	currLink.parent().removeClass("active");
	    }
	});
    	}
    </script>
    <?php get_footer() ?>