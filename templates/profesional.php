<?php /* Template Name: Profesional */ ?>
<?php get_header() ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1><?php the_title() ?></h1>
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-institucion"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-institucion"></i>&nbsp; </span>Fuero penal colegiado tribunales y juzgados</h3>
					<div class="text-muted"><p>Consultar audiencias</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-educacion"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-educacion"></i>&nbsp; </span>Codigo Procesal y Civil y Comercial</h3>
					<div class="text-muted"><p>Denucia hechos delictivos</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-mujer"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-mujer"></i>&nbsp; </span>Dirección de la mujer, genero y diversidad</h3>
					<div class="text-muted"><p>Consultas e información</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-justicia-negativo"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-justicia-negativo"></i>&nbsp; </span>IURIX Online</h3>
					<div class="text-muted"><p>Acceso</p></div>
				</div>
			</a>	
		</div>
	</div>
	<!-- Segunda fila -->
	<div class="row">
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-familia-2"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-familia-2"></i>&nbsp; </span>Dirección de Derechos Humanos</h3>
					<div class="text-muted"><p>Denunciar hechos delictivos</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-datos-abiertos-2"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-datos-abiertos-2"></i>&nbsp; </span>Consulta movimientos de expedientes</h3>
					<div class="text-muted"><p>Consultas</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-marcador-ubicacion-2"></i></div>
				<div class="panel-body">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-marcador-ubicacion-2"></i>&nbsp; </span>Ubicación de oficinas en Tribunales</h3>
					<div class="text-muted"><p>Consultas e información</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-notificaciones"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-notificaciones"></i>&nbsp; </span>Notificaciones Electrónicas</h3>
					<div class="text-muted"><p>Consultas</p></div>
				</div>
			</a>	
		</div>
	</div>
	<!-- Tercer fila -->
	<div class="row">
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-comunidad"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-comunidad"></i>&nbsp; </span>Oficina de profesionales</h3>
					<div class="text-muted"><p>Acceso</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-seguridad"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-seguridad"></i>&nbsp; </span>Registro de detenidos</h3>
					<div class="text-muted"><p>Consultas</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-reloj"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-reloj"></i>&nbsp; </span>Cálculo de penas</h3>
					<div class="text-muted"><p>Consultas y calculo</p></div>
				</div>
			</a>	
		</div>
		<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
			<a href="#" class="panel panel-default panel-icon panel-primary">
				<div class="panel-heading hidden-xs"><i class="fa icono-arg-documento"></i></div>
				<div class="panel-body mh-136">
					<h3><span class="visible-xs-inline"><i class="fa icono-arg-documento"></i>&nbsp; </span>Descarga de formularios y escritos varios</h3>
					<div class="text-muted"><p>Consultas y descargas</p></div>
				</div>
			</a>	
		</div>
	</div>
</div>
<?php get_footer() ?>