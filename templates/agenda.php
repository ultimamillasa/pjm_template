<?php /* Template Name: Agenda */ ?>
<?php get_header() ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<h1>Agenda <?php echo get_the_time('F j, Y') ?></h1>
			<hr>
			<a href="/salud/calidadatencionmedica/2jornadanacional" class="panel panel-default">
				<div class="panel-body">
					<div class="media">
						<div class="media-body">
							<h3><span class="label label-primary"><?php echo get_the_time('g:i a') ?></span> Ciclo de capacitación a través del análisis de sentencias con perspectiva de género</h3>
						</div>
					</div>
				</div>
			</a>
			<a href="/salud/calidadatencionmedica/2jornadanacional" class="panel panel-default">
				<div class="panel-body">
					<div class="media">
						<div class="media-body">
							<h3><span class="label label-primary"><?php echo get_the_time('g:i a') ?></span> Jornadas de capacitación: Fuero de niñez, adolescencia y familia.</h3>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
</div>
<?php get_footer() ?>